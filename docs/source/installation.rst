============
Installation
============

The package is hosted in a public Git repository. The preferred way to obtain it and set up is:

::

   git clone https://bitbucket.org/jairhul/pyflusix
   pip install -e pyflusix


An alternative, but strongly discouraged way to use pyflusix is to add the repository to the ``PYTHONPATH`` environmental variable directly.
:code:`export PYTHONPATH=/path/to/pyflusix/repository:$PYTHONPATH`
