.. pyflusix documentation master file, created by
   sphinx-quickstart on Mon Aug 20 13:08:48 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyflusix's documentation!
====================================

pyflusix is a Python package for analysis of results from simulations performed using the SixTrack-Fluka active coupling framework. This package is intended as a layer between the user and the raw output from the simulation which allows for quick and scalable processing.

For details on how to set up and use the SixTrack-Fluka coupling framework please refer to the project repoistory and the project `Twiki page`_.

.. _Twiki page: https://twiki.cern.ch/twiki/bin/view/FlukaTeam/CouplingSVNRepositories

.. toctree::
   :maxdepth: 3
   :titlesonly:

   installation
   authorship
   overview
   quickstart
   poweruser
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
