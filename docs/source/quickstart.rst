==========
Quickstart
==========

This section outlines the basic steps to process the data and produce plots.

.. _data_compression:

Data compression
----------------

The simulation output must be present in a directory like described in :ref:`data_structure` and a valid Twiss file ``<name>.tfs`` and collimator info file ``collgaps.dat`` must be placed in the ``input`` directory of the study. The processing can be initiated using::

  python <path_to_pyflusix>/bin/compress_originals.py --dir <study_dir>

or, provided that python environment is appropriately set up for python, simply::

  <path_to_pyflusix>/bin/compress_originals.py --dir <study_dir>

The script collects the information from the auxiliary files and processes the data from the simulation. A directory ``output`` is created with the processed data.

If only the study directory is supplied the default procedure is to extract the starting element name from the ``fort.2`` file in ``clean_input``, the collimator parameters from ``collgaps.dat`` and the machine length and starting S position from the TFS file. The machine type is also needed in order to distinguish between cold and warm losses.

.. note::
   If machines that are not present in the current list or different optics version
   where the cold and warm region boundaries change are simulated, it is down to the user to ensure
   that the aperture temperature information is added to the existing ones and accessed appropriately.

As no files in the whole simulation workflow file carry rigorous and reliable information about the type of machine being simulated, a set of heuristic conditions is used for guessing the machine type:

+------------------------+---------------------------+
| Condition              | Machine guess             |
+========================+===========================+
| Circumference > 30 km  | FCC                       |
+------------------------+---------------------------+
| Circumference between  | HL-LHC                    |
| 20 km and 30 km and    |                           |
| TCLD element in        |                           |
| lattice                |                           |
+------------------------+---------------------------+
| Circumference  between | LHC                       |
| 20 km and 30 km        |                           |
+------------------------+---------------------------+
| Circumference between  | SPS                       |
| 6 km and 20 km         |                           |
+------------------------+---------------------------+
| Circumference between  | PS                        |
| 500 m and 6 km         |                           |
+------------------------+---------------------------+

.. warning::
   The default machine type guess is simplistic and may be unreliable. Always check that
   the machine has been identified correctly or explicitly set the machine using the
   --machine flag.

The ``compress_orignals.py`` script is controlled using the following executable options:
  -h, --help      show this help message and exit
  --dir=STUDYDIR  Path to the directory with run_xxxxx files
  --imax=IMAX     Maximum number of runs to read
  -i, --impacts   Include detailed information about aperture losses
  -p, --isotopes  Include merging of isotopes and unique id processing for
                  particle ids
  -q, --quiet     Silence all printouts


.. _plot_lossmap:

Plotting a Loss Map
-------------------

Once the data compression is complete, a lossmap can be plotted directly using the ``lossmap.py`` script::

  python <path_to_pyflusix>/bin/lossmap.py --dir <study_dir>

The default range of the lossmap is the entire machine, but a section can be plotted using the ``--smin`` and ``--smax`` arguments. For example, plotting only IR7 of the LHC::

  python <path_to_pyflusix>/bin/lossmap.py --dir mylhcstudy --smin 19600 --smax 22000

If the data compression is perfomed as descibed above, all of the machine parameters needed are
stored in the ``output/machine_info.txt`` file and it is not neccessary to specify them.

All the available arguments are:
  -h, --help         show this help message and exit
  --dir=STUDYDIR     Path to the directory with run_xxxxx files
  --machine=MACHINE  Machine - lhc, hllhc, fcc. (Optional)
  --startS=STARTS    Offset of SixTrack S coordinate from the TFS - value in
                     meters or path to fort.2 file. (Optional)
  --smin=SMIN        Lower S bound for plot (Optional)
  --smax=SMAX        Upper S bound for plot (Optional)
  --quench=QUENCH    Quench limit in 1/m (Optional)
  --outfile=OUTFILE  File to save the plot under
  -q, --quiet        Silence all printouts (Optional)
  -x, --debug        Enable detailed printouts for debugging (Optional)


Plotting Aperure Losses
-----------------------

In addition to the lossmap plotting, tools exist to plot various information about aperture losses. The ``plot_impacts.py`` script allows plotting of transverse and longitudinal distribution and particle type for aperture losses in a given region of the machine. 

The default range is again the entire machine, but a section can be plotted using the ``--smin`` and ``--smax`` arguments. For example, plotting transverse and longitudinal distirbution of aperture losses in IR7 of the LHC::

  python <path_to_pyflusix>/bin/plot_impacts.py --filename mylhcstudy/output/fort.impacts
    -smin 19600 --smax 22000 -sp

If a SixTrack aperture dump is specified using the ``--aperture`` argument, the aperture profile in the corresponding plane will be plotted on top of the longitudinal distribution plot.

All the available arguments are:
  -h, --help            show this help message and exit
  --filename=FILENAME   Path to file containing impacts. Default is
                        fort.impacts.
  --plane=PLANE         Plane to make the plots in: horizontal or vertical.
                        Default is horizontal
  --smin=SMIN           Starting S position of region of interest.
  --smax=SMAX           Final S position of region of interest.
  --sstart=SSTART       Offset to be applied to all S position. Offset is
                        applied before region selection.
  --stot=STOT           Total S (length) of the machine. Must specify together
                        with the --sstart option.
  --ymin=YMIN           Minimum y-axis extent of plot.
  --ymax=YMAX           Maximum y-axis extent of plot.
  --aperture=APERFILENAME
                        Name of aperture dump from SixTrack (Optional).
  --output=OUTFILE      Filepath to save the plot under.
  -p, --phplot          Make a projected phase space plot of the losses.
  -z, --azplot          Make a 2D histrogram of A and Z of the losses.
  -a, --waplot          Make an energy-weighted histogram of A of the lossses.
  -s, --splot           Make a plot of the loss coordinates against S for the
                        selected plane.
  -v, --verbose         Print detailed information
