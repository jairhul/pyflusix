==========
Authorship
==========

The following authors have contributed to pyflusix:

* Andrey Abramov
* Nuria Fuster-Martinez
* Pascal Hermes
* Alessio Mereghetti
* Laurie Nevay
