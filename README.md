# pyflusix

The purpose of this repository is to enable quick and straightforward analysis of results from simulations performed in the SixTrack-Fluka active coupling framework. The tools found are intended to work alongside existing post-processing and analysis tools. The analysis makes a few assumptions about how the results are organised and what prior pre-processing has been done on the data.

The project is hosted in a public git repository: <https://bitbucket.org/jairhul/pyflusix/>
The HTML documentation can be found at: <https://andreyabramov.bitbucket.io/>

pyflusix is intended to be used with:
   * SixTrack: <http://sixtrack.web.cern.ch/SixTrack/>
   * FLUKA: <http://www.fluka.org/fluka.php>
   * fluka\_coupling: <https://gitlab.cern.ch/bmi/fedb/coupling/fluka_coupling/>
   * MADX: <https://mad.web.cern.ch/mad/>
 
pyflusix also uses on several open source projects:
   * [Pandas] - Genral and powerful Python data analysis library
   * [Pymadx] - Package for loading and manipulating MADX input and output
  
### Requirements
pyflusix is exclusively developed for python2.7 and requires the following packages:
- numpy
- pandas
- matplotlib
- pymadx [optional]

### Installation

pyflusix can be obtained from the public git repository and installed like,

```sh
$ cd /path/to/desired/package/desitnation
$ git clone https://bitbucket.org/jairhul/pyflusix.git
$ cd pyflusix
$ make install
```

### Runing
Executable scripts performing the most common tasks can be found in `bin` directory. Each of the scripts has documentation that can be invoked with the `-h` flag and additional infomation is available in the HTML manual.


### Todos

 - Write Tests
 - Provide examples

[Pandas]: <https://pandas.pydata.org/>
[pymadx]: <http://www.pp.rhul.ac.uk/bdsim/pymadx/index.html>
