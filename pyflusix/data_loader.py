import os as _os
import sys as _sys
import gzip as _gzip
import numpy as _np
import pandas as _pd


def string_isnumber(string):
    """
    Check whether a string represents a  number.
    """
    try:
        float(string)
        isnumber = True
    except ValueError:
        isnumber = False

    return isnumber


def _eval_string_operator(operator, lhs, rhs):
    if operator == "<":
        result = lhs < rhs
    elif operator == ">":
        result = lhs > rhs
    elif operator == "=":
        result = lhs == rhs
    else:
        raise ValueError("Operator not supported {}".format(operator))
    return result


class ColumnData(object):
    """
    General purpose data loader that loads  whitespace- and tab-separated
    data files into numpy array with corresponding columns.
    Access to the data is possible by both column names and column indices
    and methods are provided to filter the data and iterate over data rows.
    When loading a file an attempt is made to find a header row and assign
    data column names, but if this fails it is possible to assign names later.
    """
    def __init__(self, filename=None, skip_header=0, verbose=True):

        self.fromfile = False
        if filename is not None:
            self.fromfile = True

        if self.fromfile:
            self.filename = _os.path.abspath(filename)
            if not _os.path.isfile(self.filename):
                raise IOError("File doesn't exist, {}".format(self.filename))

        self.skip_header = skip_header
        self.verbose = verbose

        # Keep the data in an array to preserve order
        # Access via name provided by an index map
        self.data = _np.array([])
        self.names = []
        self._index_map = {}

        if self.fromfile:
            self.load()
            self.ncolumns = len(self.data)
            self.nrows = len(self.data[0])

    def __getitem__(self, key):
        index = self._get_index(key)
        return self.data[index]

    def __setitem__(self, key, item):
        raise ValueError("Setting items, not implemented yet")

    def _get_index(self, key):
        if isinstance(key, str):
            self._make_index_map()  # refresh in case column names have changed

            if key in list(self._index_map.keys()):
                index = self._index_map[key]

            elif string_isnumber(key):
                key_int = int(float(key))

                if abs(float(key) - int(float(key))) > 2*_sys.float_info.epsilon:
                    raise ValueError("Bad column index : {}".format(key))

                index = self._get_index(key_int)

            else:
                msg = "Column not found {}, available columns:\n{}"
                raise ValueError(msg.format(key, ", ".join(list(self._index_map.keys()))))

        elif isinstance(key, int):
            if key < -1 or key > (len(self.data) - 1):
                msg = ("Index out of range. "
                        "Possible indices are between 0 and {}".format(len(self.data)-1))
                raise ValueError(msg)
            else:
                index = key

        else:
            raise ValueError("Invalid index format {}, use str or int".format(type(key)))

        return index

    def iterrows(self):  # generator for row reading
        """
        Generator for iteration  over the data row by row. Each row returned is
        itself a ColumnData instance and values inside it can be accessed by name
        and index as normal

        Yields: a single data row
        """
        data_rows = _np.column_stack(self.data)
        for data_row in data_rows:
            row = ColumnData()
            row.data = data_row
            row.names = self.names
            yield row

    def _make_index_map(self):
        if not self.data.size:
            raise SystemExit("No data found.")

        if len(self.names) != len(self.data):
            msg = "Invalid number of column names: {} for data containing {} columns"
            raise ValueError(msg.format(len(self.names), len(self.data)))

        for i in range(len(self.names)):
            self._index_map[self.names[i]] = i

    def _get_header(self):
        """
        This method tries to find a header line and extract the column
        names from it.  The header line must begin with #.
        Note that bracketed item in the header like [foo] or (foo) are taken
        as a unit of the previous item. This may sometimes cause mistmatches
        between the data and the header, but this is protected against.
        """
        # Allow reading of headers from gzipped files
        if self.filename.endswith(".gz"):
            myopen = _gzip.open
        else:
            myopen = open

        with myopen(self.filename, "r") as filein:
            line = filein.readline()

            for i in range(int(self.skip_header)):
                line = filein.readline()

            while len(line) <= 1:  # Skip empty lines if any
                line = filein.readline()

            comment = "#"
            if line[0] == comment:
                sline = line.split()
                sline[0] = sline[0].strip().replace("#", "")
                sline = [val for val in sline if val]

                # Try to match values and units that may have been split off
                header = []
                i = 0
                while i < len(sline):
                    value = sline[i]

                    if i < len(sline) - 1:
                        maybe_unit = sline[i+1]
                    else:
                        maybe_unit = ""

                    if maybe_unit.startswith("[") or maybe_unit.startswith("("):
                        # No column should start with a bracket - this is a unit
                        column = value + maybe_unit
                        i += 2  # Skip next element

                    else:
                        column = value
                        i += 1

                    header.append(column.replace(",", ""))

                if self.verbose:
                    print(("Header labels found: {}".format(", ".join(header))))

            else:
                header = None

            return header

    def load(self):
        """
        Load a data file into an array. The data array has elements corresponding
        to the columns in the data file.
        """
        if self.verbose:
            print(("Loading file {}".format(self.filename)))
        if self.skip_header:
            if self.verbose:
                print(("Skipping {} lines from the head of the file".format(self.skip_header)))

        # If there is a header take columns names from it
        header = self._get_header()

        # Use pandas to load file to make use of type comprehension
        raw_data = _pd.read_csv(self.filename, delim_whitespace=True, comment="#", header=None,
                                skiprows=self.skip_header, names=header)
        raw_data = raw_data.values.T  # Get a numpy array from the pandas data frame

        if self.verbose:
            print(("Data columns loaded: {}".format(len(raw_data))))

        if not header or len(header) != len(raw_data):
            if self.verbose:
                print("No header found or header/data mistmatch, use column numbers for access")
            header = ["{:2d}".format(val) for val in range(len(raw_data))]

        self.names = header
        self.data = raw_data

    def filter_data(self, filterstring):
        """
        Use a filter string to define a range of the data.
        The returned data set has only the rows that have values
        in the target columns that match the condition.

        Use a semicolon-separated list of conditions of the form:

        @columnIndex <,>,= value

        eg.

        filterstr = '@1<10; @2>3; @5=1'

        or

        filterstr = '@COLNAME1 < 10; @COLNAME2 >3;'
        """

        if not filterstring or not self.data.size:
            return

        # Unpack into individual conditions and strip spaces
        conditions = filterstring.strip().split(";")
        conditions = [val.strip() for val in conditions if val]

        # Check always 1 and only 1 operator in every condition
        operators = frozenset(["<", ">", "="])
        operators_found = []
        for cnd in conditions:
            found = set(cnd) & operators
            operators_found.append(found)

        single_and_unique = [False]*len(conditions)
        for i, oper in enumerate(operators_found):
            cnd = conditions[i]

            if len(oper) == 1:
                if cnd.count(list(oper)[0]) == 1:
                    single_and_unique[i] = True

        if not all(single_and_unique):
            raise ValueError("Invalid filter operator configuration {}".format(filterstring))

        data_reduced = _np.column_stack(self.data)

        for cnd in conditions:
            operator = list(operators_found[conditions.index(cnd)])[0]
            target, value = cnd.split(operator)

            if target[0] == "@":
                target_stripped = target[1:]
            else:
                raise ValueError("Invalid index formatting {}, use @{}".format(target, "index"))

            try:
                value_rendered = float(value)
            except ValueError:
                raise ValueError("Invalid fliter value: {}".format(value))

            # If there is problems with index access this will fail gracefully
            index = self._get_index(target_stripped)

            expression = "{} {} {}".format(index, operator, value_rendered)
            if self.verbose:
                print(("Applying filer condition: values in Column {}".format(expression)))
                print(("Rows before {}".format(len(data_reduced))))

            data_reduced = [row for row in data_reduced if
                            _eval_string_operator(operator, float(row[index]), value_rendered)]
            if self.verbose:
                print(("Rows after {}".format(len(data_reduced))))

        self.data = _np.array(list(zip(*data_reduced)))

        if not self.data.size:
            raise SystemExit("No data left after filtering. Stop.")

    def sort_data(self, columnid, reverse=False):
        """
        Sort the rows of data based on the values in a column.
        The columnid can be an integer index or a column name.

        By default the sort is done in ascending order, but a flag
        is provided if descending order is needed.
        """
        index = self._get_index(columnid)

        data_rows = _np.column_stack(self.data)

        order = "descending" if reverse else "ascending"
        if self.verbose:
            print(("Sorting data in {} on column {}".format(order, index)))

        self.data = list(zip(*sorted(data_rows, key=lambda tup: tup[index], reverse=reverse)))
