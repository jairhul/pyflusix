import os as _os
import warnings as _warnings
from datetime import datetime

import pandas as _pd


class MachineParameters(object):
    def __init__(self, verbose=True):
        self.length = 0
        self.s_start = 0
        self.machine = None
        self.start_name = ""
        self.verbose = verbose

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __bool__(self):
        nonzero = False
        if self.length and self.s_start:
            nonzero = True
        return nonzero

    def load_from_tfs_fort2(self, tfsfile, fort2file, machine=None):
        if not _os.path.isfile(tfsfile):
            raise IOError("TFS file {} doesn't exist".format(tfsfile))

        if not _os.path.isfile(fort2file):
            raise IOError("fort.2 file {} doesn't exist".format(fort2file))

        self.start_name = find_fort2_start(fort2file)

        header, header_line_no = get_twiss_header(tfsfile)

        if self.verbose:
            # Skip header_line_no + 1 because the line after is field formats
            print(("Skipping {} lines from the TFS".format(header_line_no + 1)))

        # twiss = _pd.read_csv(tfsfile, delim_whitespace=True, names=header, skiprows=47)
        twiss = _pd.read_csv(tfsfile, delim_whitespace=True, skiprows=header_line_no + 1)

        twiss.columns = header

        s_start = None
        for idx, row in twiss.iterrows():
            # print row
            if self.start_name.lower() in row["NAME"].lower():
                s_start = row["S"]

        if s_start is not None:
            if self.verbose:
                msg = "Start element identified as {} at S = {} m in the Twiss"
                print((msg.format(self.start_name, s_start)))
            self.s_start = float(s_start)
        else:
            raise SystemExit("Could not find a valid starting S positon from the fort.2\n"
                             "Please provide an explicit nurmerical value.")

        self.length = max(twiss["S"])

        if self.verbose:
            print(("Machine length is {} m".format(self.length)))

        # Try to guess the type of machine based on the length
        # and the presense of a dispersion suppressor collimator

        if machine is None:
            tcld_in = False
            for name in twiss["NAME"]:
                if "TCLD" in name.upper():
                    tcld_in = True
                    break

            if self.length > 3E4:  # FCC-scale machine
                self.machine = "fcc"

            elif self.length > 2E4:  # LHC-scale machine
                if tcld_in:
                    self.machine = "hllhc"  # The LHC doesn't have sa TCLD
                else:
                    self.machine = "lhc"

            elif self.length > 6E3:
                self.machine = "sps"

            elif self.length > 500:
                self.machine = "ps"

            else:
                _warnings.warn("Could not determine machine type. Setting to None")
                self.machine = None

        else:
            supported_machines = ['lhc', 'hllhc', 'fcc', 'sps', 'ps']
            if machine not in supported_machines:
                msg = 'Unknown machine, {}\n Supported machines: {}'
                raise SystemExit(msg.format(machine, ', '.join(supported_machines)))

            self.machine = machine

        if self.verbose:
            print(("Machine identified as: {}".format(self.machine)))

    def shift_S(self, S):
        if self.s_start and self.length:
            S_shifted = (float(S) + self.s_start) % self.length
        else:
            S_shifted = float(S)

        return S_shifted

    def write_machine_info(self, outfile):
        with open(outfile, 'w') as fileout:
            today_formatted = datetime.today().strftime("%d-%m-%Y")
            fileout.write("# Machine info from compression run on {}\n".format(today_formatted))
            fileout.write("Machine {}\n".format(self.machine))
            fileout.write("Length  {}\n".format(self.length))
            fileout.write("Start   {}\n".format(self.start_name))
            fileout.write("startS  {}\n".format(self.s_start))

    def load_from_infofile(self, filepath):
        if not _os.path.isfile(filepath):
            raise IOError("Machine parameters file {} doesn't exist".format(filepath))

        with open(filepath, "r") as filein:
            for line in filein:
                sline = line.split()
                if "Machine" in sline[0]:
                    self.machine = sline[1]
                elif "Length" in sline[0]:
                    self.length = float(sline[1])
                elif "Start" in sline[0]:
                    self.start_name = sline[1]
                elif "startS" in sline[0]:
                    self.s_start = float(sline[1])

    def user_defined_S(self, s_start, s_total):
        if s_start is not None and s_total is not None:
            try:  # Check the inputs are numbers
                self.s_start = float(s_start)
                self.length = float(s_total)

            except ValueError:  # They are maybe filenames
                if _os.path.isfile(s_start) and _os.path.isfile(s_total):
                    if s_start.endswith("fort.2") and s_total.endswith(".tfs"):
                        self.load_from_tfs_fort2(s_total, s_start)
                    else:
                        raise ValueError("Incorrect files found, please provide a fort.2"
                                         "file and a TFS file in this order.")
                else:
                    raise ValueError("Invalid input, provide numerical values or valid"
                                     "paths to a fort.2 and a TFS file")


def get_twiss_header(filepath):
    header = None
    line_no = 0
    with open(filepath, "r") as filein:
        for idx, line in enumerate(filein):
            if line.startswith("*"):  # This is the header line, preable lines start with @
                header = line.replace("*", "").strip().split()  # Strip the comment char
                line_no = idx + 1  # As zero counterd
                break  # End the loop early - information found

    return header, line_no


def find_fort2_start(filepath):
    """
    Finds the GO directive if present in the fort.2 and returns the name of the element.
    """
    first_element = None
    start_element = None
    on_next_line = False
    line_before = []
    with open(filepath, "r+") as filein:
        for line in filein:
            line_split = line.strip().split()
            if "GO" in line_split:  # Find the line with the GO directive
                go_index = line_split.index("GO")
                if go_index < len(line_split) - 1:
                    start_element = line_split[go_index + 1]  # Need the element after the GO
                    break  # Stop search if element is found
                else:
                    on_next_line = True

            elif on_next_line:
                start_element = line_split[0]  # If GO is last, look on next line
                break

            if "STRUCTURE" in line_before:  # Find and store the very first element
                first_element = line_split[0]

            line_before = line

    if start_element is None:  # If no GO is found, the start is the first element
        start_element = first_element

    return start_element
