import os as _os
import glob as _glob
import numpy as _np


class FlukaTouches(object):
    def __init__(self, study_directory, imax=-1, outputdir=-1, debug=False):
        """
        Assumes a structure like:

        study_directory
          |
          --> run***
          |    |
          |    --> *_first.dat
          |
          --> run***
          |    |
          |    --> *_first.dat
         ...

        This script merges multiple first touches file into one file
        """

        self.studydir = _os.path.abspath(study_directory)
        if imax < 0:
            self.imax = None
        else:
            self.imax = int(imax)

        if str(outputdir) == "-1":
            self.outputdir = self.studydir
        else:
            self.outputdir = _os.path.abspath(outputdir)

        self.data_merged = []
        self.outputfilename = ""
        self.debug = debug
        print(("Study directory: ", self.studydir))
        print(("Output directory: ", self.outputdir))

        self.load()
        self.write()

    def load(self):
        print("Loading data...")
        rundirs = [_os.path.join(self.studydir, each) for each in _os.listdir(self.studydir)
                   if each.startswith("run_")][:self.imax]

        # If no first touches maybe nothing hit the collimators
        first_touches = [_glob.glob(_os.path.join(path, '*first.dat.gz'))[0] for path
                         in rundirs if _glob.glob(_os.path.join(path, '*first.dat.gz'))]

        # Obtain the outputfilename dynamically
        self.outputfilename = first_touches[0].replace("_first.dat.gz", "")

        data = []
        for filename in first_touches:
            try:
                data.append(_np.genfromtxt(filename))
            except ValueError:
                print(("Corrupted file {}".format(filename)))

        # Merge the data from all files, sort on collimator id and unpack into columns
        self.data_merged = list(zip(*sorted(_np.vstack(data), key=lambda tup: tup[0])))

    def write(self):
        name_stub = "merged_{}_first.dat"
        outputfilename = _os.path.join(self.outputdir,
                                       name_stub.format(_os.path.basename(self.outputfilename)))

        # TODO: This header is rather ugly, use format to make it better
        header = (" IPNT      Ekin [GeV]            S[m]           X[mm]        XP[mrad]"
                  "           Y[mm]        YP[mrad]    IDP IDPGEN  ITURN WEIGHT")

        fltfmt = ["% 2d", "% .8E", "% .8E", "% .8E", "% .8E", "% .8E",
                  "% .8E", "% 4d", "% 4d", "% 4d", "% .8E"]

        _np.savetxt(outputfilename, _np.column_stack(self.data_merged), delimiter="\t",
                    fmt=fltfmt, header=header, comments="#")

        print(("File written: ", outputfilename))
