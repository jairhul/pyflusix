#!/bin/bash

# Copy files needed by the simulation - initial.dat is copied selectively
shopt -s extglob  # to enable extglob
cp ${__WORKDIR__}/clean_input/!(initial.dat*) ./
shopt -u extglob

# replace the Random seed by the "cycle" number (${__SEED__})
sed -i "s/^RANDOMIZ.*/RANDOMIZ         1.0       ${__SEED_ASFLOAT__}/g" ${__INPFILE__}

# beam distribution from existing file
head -n${__NLAST__} ${__WORKDIR__}/clean_input/initial.dat | tail -n${__NPART__} > initial.dat
