#!/bin/bash

# Copy files needed by the simulation
cp ${__WORKDIR__}/clean_input/* .

# replace the Random seed by the "cycle" number (${__SEED__})
sed -i "s/^RANDOMIZ.*/RANDOMIZ         1.0${__SEED_ASFLOAT__}/g" ${__INPFILE__}

# We create the beam distribution runtime
sed -i "s/^OUTPUT.*/OUTPUT  ${__NPART__}  ${__SEED__}  initial.dat/g" ${__BEAMFILE__}

# beam distribution created runtime
if [[ ! -f ${__COUPLING_PATH__}/tools/partdist/gpdist.exe ]] ; then
    cd ${__COUPLING_PATH__}/tools/partdist
    make clean
    make all
    cd - >> /dev/null
fi
${__COUPLING_PATH__}/tools/partdist/gpdist.exe ${__BEAMFILE__}
