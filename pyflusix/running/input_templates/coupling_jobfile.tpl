#!/bin/bash
#PBS -q ${__QUEUE__}
#PBS -N ${__WORKDIR__}.${__RUN_NUM__}

export FLUKA=${__FLUKAPRO__}
export FLUPRO=${__FLUKAPRO__}

RUNPATH=$$PWD
HOST=$$(hostname)
USERNAME=$$(whoami)

source /cvmfs/sft.cern.ch/lcg/contrib/gcc/8.1.0/x86_64-slc6/setup.sh

echo " ** Host: $$HOST" > $$RUNPATH/server_${__RUN_NUM__}.log
echo " ** Original directory: $$RUNPATH/run_${__RUN_NUM__}" >> $$RUNPATH/server_${__RUN_NUM__}.log

echo " $$HOST" > network.nfo

# Check if the executable is newer than the library
newer_file=0
if [ -e ${__SERVER_PATH__} ] ; then
    if [ ${__SERVER_PATH__} -ot ${__FLUKAHP_PATH__} ] ; then
        let newer_file=$${newer_file}+1
    fi
else
    let newer_file=$${newer_file}+1
fi
if [ $${newer_file} -gt 0 ]; then
    echo " ** flukahp newer than the executable flukaserver..." >> $$RUNPATH/server_${__RUN_NUM__}.log
    ls -lhr  $${FLUPRO}/flukahp >> $$RUNPATH/server_${__RUN_NUM__}.log
    echo " ** Recompiling FLUKA executable..." >> $$RUNPATH/server_${__RUN_NUM__}.log
    cd ${__COUPLING_PATH__}/fluka
    make all
    cd -
fi
# Run the server in background
echo " - Running server in background..." >> $$RUNPATH/server_${__RUN_NUM__}.log
$$FLUPRO/bin/rfluka ${__NAME__} -e ${__SERVER_PATH__} -M 1 >> $$RUNPATH/server_${__RUN_NUM__}.log 2>&1 &

echo " ** Host: $$HOST" > $$RUNPATH/tracker_${__RUN_NUM__}.log

# Wait until server is initialized
while [ ! -f network.nfo ]; do
  echo "server still not running, waiting..." >> $$RUNPATH/tracker_${__RUN_NUM__}.log
  sleep 10
done

# wait until network.nfo declares both host and port
while [ `\wc -l network.nfo | \awk '{print ($$1)}'` -ne 2 ] ; do
  echo "host/port not fully declared, waiting..." >> $$RUNPATH/tracker_${__RUN_NUM__}.log
  sleep 10
  # Check FLUKA is still running on the background. If not, close the job.
  if [ ` \ps -u $$USERNAME | \grep rfluka | \wc -l ` -eq 0 ]; then
      echo "Something went wrong: FLUKA exited before opening the port!"  >> $$RUNPATH/tracker_${__RUN_NUM__}.log
      gzip -c tracker_*.log > crashed_six.log.gz
      gzip -c fluka*/*log > crashed_fluka.log.gz
      gzip -c fluka*/*err > crashed_fluka.err.gz
      gzip -c fluka*/*out > crashed_fluka.out.gz
      ls -lhR

      ${__POSTPROC__}
      exit
  fi
done

# Run tracker in foreground
echo " - Running the Tracker..." >> $$RUNPATH/tracker_${__RUN_NUM__}.log
${__TRACKER_PATH__} >> $$RUNPATH/tracker_${__RUN_NUM__}.log 2>&1
 
if [ $$? -ne 0 ]; then
   gzip -c tracker_*.log > crashed_six.log.gz 
   gzip -c fluka*/*log > crashed_fluka.log.gz 
   gzip -c fluka*/*err > crashed_fluka.err.gz 
   gzip -c fluka*/*out > crashed_fluka.out.gz 
   ls -lhR 
fi

echo " - Tracker done" >> $$RUNPATH/tracker_${__RUN_NUM__}.log

# Wait until server ends
wait
echo " - Server done" >> $$RUNPATH/server_${__RUN_NUM__}.log


# remove empty units:
file_list=`\ls .`
if [ -e empties.lis ] ; then
   \rm empties.lis
fi
for file in $${file_list} ; do
    if [ ! -s $${file} ] ; then
       \ls -ltrh $${file} >> empties.lis
       \rm $${file}
    fi
done

${__POSTPROC__}
