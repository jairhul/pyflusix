***************************************************************************
**${TEMPLATE_HEADER}
***************************************************************************
* INPUT GENERATION FOR HISIX [2018]
***************************************************************************

**** OPTICS PARAMETERS ****************************************************
***************************************************************************
****    Optics/Beam parameters    ****************************************
*
*  WHAT(1) : index of the plane
*           1 -> horizonal;  2 -> vertical;
*    WHAT(2) : Beta function [m]  
*    WHAT(3) : derivative of Beta function []
*    WHAT(4) : Dispersion function [m]
*    WHAT(5) : derivative of Dispersion function []
*    WHAT(6) : Normalized emittance [m]
*           3 -> longitudinal;
*    WHAT(2) : particle mass [GeV/c2]  
*    WHAT(3) : momentum of synchronous particle [GeV/c]
*    WHAT(4) : sigma delta_p/p []
*    WHAT(5) : bunch length (actually 4*sigma_l) [s]
*    WHAT(6) : not used
*
***************************************************************************
** ${BEAM_DESCRIPTION}
*
PARAM     1        ${BETX}  ${ALFX}   ${DX}  ${DPX}   ${ENX}
PARAM     2        ${BETY}  ${ALFY}   ${DY}  ${DPY}   ${ENY}
PARAM     3        ${MASS}  ${PC}   ${DELTAP}   ${BLEN}
${IONBEAM}PARAM     4        ${A} ${Z} ${Q}
${HASPDGID}PARAM     5        ${PDGID}  ${Q}
*
***************************************************************************

**** SAMPLING DISTRIBUTIONS ***********************************************
*************************************************************************** 
*
DTYPE     1         ${DISTX}     ${SIG_INNER_X}    ${SIG_OUTER_X}
DTYPE     2         ${DISTY}     ${SIG_INNER_Y}    ${SIG_OUTER_Y}
DTYPE     3         ${DISTL}     ${SIG_INNER_L}    ${SIG_OUTER_L}
*
***************************************************************************

**** OFFSETS **************************************************************
***************************************************************************
*
OFFSET      ${XOFFS}  ${XPOFFS}  ${YOFFS}  ${YPOFFS}  ${TDELAY}  ${PCOFFS}
*
***************************************************************************

****    Geometrical Acceptance ********************************************
*
*  WHAT(1) : 1: x; 2: xp;
*          : 3: y; 4: yp;
*          : 5: t; 6: pc;
*  WHAT(2) : cut expressed in normalised (N) or real (R) units
*            - N:  sig;
*            - R:  x/y:   m
*                  xp/yp: rad
*                  t:     s
*                  pc:    GeV
*  WHAT(3) : min value
*  WHAT(4) : max value
*  WHAT(5) : (optional) check on absolute value (A)
*  WHAT(6) : (optional) check on absolute value performed after adding
*            offsets (A)
*
***************************************************************************
*
GEOACC      ${PLANE}     R     ${GEOACC_LOWER}     ${GEOACC_UPPER}   A
*
***************************************************************************

**** S SHIFT **************************************************************
*************************************************************************** 
*  WHAT(1) : length of drift;
*          : > 0 the final point is downstream of where the sampling is done 
*          : < 0 the final point is upstream of where the sampling is done
*  WHAT(2) : (optional, no default) name of file where to dump the orginal
*            distribution before being shifted
*  WHAT(3) : (opional, default 0) type of tracking along the drift
*            0: approximated Hamiltonian, corresponding to FAST compilation
*                flag of SixTrack
*            1: exact Hamiltonian
*            2: approximated Hamiltonian, corresponding the non -=FAST=
*                compilation flag of SixTrack
***************************************************************************
*
SSHIFT     ${SSHIFT}    unshifted_initial.dat
*
***************************************************************************

**** SAMPLE PARAMETERS ****************************************************
***************************************************************************
*
OUTPUT  100  RNGSEED  initial.dat header.dat 0 1
*
***************************************************************************
