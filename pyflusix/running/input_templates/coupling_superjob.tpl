#!/bin/bash
#PBS -q ${__QUEUE__}
#PBS -N coupling_${__RFIRST__}_${__RLAST__}
CURDIR=$$PWD

mkdir $$CURDIR/${__RUNDIR__}
cd $$CURDIR/${__RUNDIR__}
cp -rf ${__WORKDIR__}/${__RUNDIR__}/* .
./preproc.job
./${__NAME__}.job
cd -
date
