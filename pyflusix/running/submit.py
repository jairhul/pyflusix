import os as _os
import shutil as _shutil

import multiprocessing as _multiprocessing

from os import path as _path
from subprocess import call as _call
from subprocess import Popen as _Popen
from string import Template as _Template
from functools import partial as _partial
from datetime import datetime as _datetime


def count_lines(path):  # like wc -l
    with open(path, "r") as f:
        nlines = sum(1 for line in f)
    return nlines


def make_executable(path):
    mode = _os.stat(path).st_mode
    mode |= (mode & 0o444) >> 2
    _os.chmod(path, mode)


def get_npart_fort3(fort3file):
    with open(fort3file, "r") as f:
        npairs = 0
        perpair = 0
        trac_plusone = False
        init_plusone = False
        for line in f:
            if line.startswith("/"):
                continue
            elif trac_plusone and not npairs:
                npairs = int(line.split()[2])
            elif init_plusone and not perpair:
                perpair = int(line.split()[0])
                break  # We need nothing more
            elif "TRAC" in line:
                trac_plusone = True  # Definition on line after identified
            elif "INIT" in line:
                init_plusone = True
    npart = npairs * perpair
    return npart


def load_template(templatefile):
    with open(templatefile, "r") as f:
        template_contents = f.read()
    template = _Template(template_contents)

    return template


def load_all_templates(templatedir=_path.join(_path.dirname(__file__), "./input_templates")):
    abs_templatedir = _path.abspath(templatedir)
    template_names = [
        "coupling_jobfile.tpl",
        "coupling_preproc_gpdist.tpl",
        "coupling_preproc_initial.tpl",
        "coupling_superjob.tpl"
    ]

    template_dict = {}
    for name in template_names:
        template = load_template(_path.join(abs_templatedir, name))
        template_dict[name.replace(".tpl", "")] = template

    return template_dict


# Note that the function loadTemplate is only evaluated only once when
# the makeSingleRun is declared, not at every call
def make_single_run(run_num, var_dict={}, template_dict=load_all_templates()):

    if var_dict["beamfile"]:
        preproc_template = template_dict["coupling_preproc_gpdist"]
    else:
        preproc_template = template_dict["coupling_preproc_initial"]

    job_template = template_dict["coupling_jobfile"]

    workdir = var_dict["workdir"]
    study_name = var_dict["studyname"]

    run_dir = _path.join(workdir, "run_{:05d}".format(run_num))
    job_file = _os.path.join(run_dir, "{}.job".format(study_name))
    prep_file = _os.path.join(run_dir, "preproc.job")

    _os.makedirs(run_dir)

    # Here look at the postprocessing script and dynamically construct
    # a dict with variables that are compatible with the regular
    # submit.sh script
    postproc = var_dict["postproc"]

    postproc_resolved = ""
    if postproc:
        postproc_sub_dict = {"NAMENUMDIR": _path.basename(run_dir),
                             "RUNDIR": workdir}

        postproc_template = _Template(postproc)
        # This is user-defined and can be all kinds of wrong, but it is
        # taken as is. Because of that use safe_substitute as
        # this always returns a string, properly formed or not.
        postproc_resolved = postproc_template.safe_substitute(postproc_sub_dict)

    # Resolve the inputs into a dicts with the correct identifiers
    prep_sub_dict = {
        "__SEED__": run_num,
        "__NPART__": var_dict["npart"],
        "__NLAST__": run_num * var_dict["npart"],
        "__WORKDIR__": workdir,
        "__BEAMFILE__": var_dict["beamfile"],
        "__COUPLING_PATH__": var_dict["couplingpath"],
        "__INPFILE__": "{}.inp".format(var_dict["studyname"]),
        "__SEED_ASFLOAT__": "{:10.1f}".format(run_num)
        }

    job_sub_dict = {
        "__RUN_NUM__": "{:05d}".format(run_num),
        "__NAME__": var_dict["studyname"],
        "__QUEUE__": var_dict["queue"],
        "__WORKDIR__": workdir,
        "__FLUKAPRO__": var_dict["flukapath"],
        "__FLUKAHP_PATH__": var_dict["flukahppath"],
        "__TRACKER_PATH__": var_dict["trackerpath"],
        "__SERVER_PATH__": var_dict["serverpath"],
        "__COUPLING_PATH__": var_dict["couplingpath"],
        "__POSTPROC__": postproc_resolved
        }

    # Render the templates by substituting all values

    # The prep dict has all possible values - use safe substitue to only
    # use the ones needed in the chosen template,
    prep_template_resolved = preproc_template.safe_substitute(prep_sub_dict)
    job_template_resolved = job_template.substitute(job_sub_dict)

    # Write the ready files
    with open(prep_file, "w") as f:
        f.write(prep_template_resolved)

    with open(job_file, "w") as f:
        f.write(job_template_resolved)

    # Set the permissions
    make_executable(prep_file)
    make_executable(job_file)


def make_super_jobs(rfirst, rlast, ncycles, var_dict={}, template_dict=load_all_templates()):
    # This is a really ugly function that butchers the template to cut off the header  and
    # breaks the logic flow of the program. It can be made much more elegant if a proper templating
    # engine like jinja2 is used to implement the logic inside the template itself.

    template = template_dict["coupling_superjob"]

    contents = template.template  # get it as a string
    aslines = contents.split("\n")
    shebang = aslines[0]
    header = "\n".join([line for line in aslines if line.startswith("#PBS")
                          or line.startswith("CURDIR")])
    body = "\n".join([line for line in aslines if not line.startswith("#")
                          and not line.startswith("CURDIR")])

    header_template = _Template(header)
    body_template = _Template(body)

    name = var_dict["studyname"]
    workdir = var_dict["workdir"]
    queue = var_dict["queue"]

    if rlast % ncycles:
        raise SystemExit("Number of jobs should be exactly divisible"
                         " by the number of jobs in a cycle.")

    condor_joblist = _path.join(workdir, "jobs.txt")
    superjob_count = rfirst / ncycles + 1 if ncycles > 1 else rfirst/ncycles
    subjobs = []
    for run_num in range(rfirst, rlast+1):
        # Note, no directories are created here, the iteration
        # just enforces the sequential order
        subs_dict = {
            "__NAME__": name,
            "__WORKDIR__": workdir,
            "__RUNDIR__": "run_{:05d}".format(run_num)
            }
        job = body_template.substitute(subs_dict)
        subjobs.append(job)
        if run_num % ncycles == 0:
            header_dict = {
                "__QUEUE__": queue,
                "__RFIRST__": (superjob_count-1)*ncycles + 1,
                "__RLAST__": run_num
            }
            head = header_template.substitute(header_dict)
            jobfilename = _path.join(workdir, "job_{:05d}".format(superjob_count))
            content = "{}\n{}\n{}".format(shebang, head, "".join(subjobs))
            with open(jobfilename, "w") as f:
                f.write(content)
            make_executable(jobfilename)

            with open(condor_joblist, "a") as f:
                f.write("{}\n".format(_path.basename(jobfilename)))
            
            subjobs = []
            superjob_count += 1


def copy_submit_config(destdir, queue,
                       source=_path.join(_path.dirname(__file__),
                                         "./input_templates/htcondorsub.tpl")):
    with open(source, "r") as f:
        contents = f.read()

    # Simple substitution, no need for template
    contents_wqueue = contents.replace("__QUEUE__", queue)

    with open(_path.join(destdir, "htcondor.sub"), "w") as f:
        f.write(contents_wqueue)


def submit(inputfile, njobs, ncycles, couplingpath,
           trackerpath="default", workdir="default",
           limifile="fort3.limi", additional=[],
           postprocfile=None, beamfile=None, queue="tomorrow", flukapath="default",
           local=False, verbose=True):

    # Check the input
    print("--> Initialising submission")
    print(("--> Date: {}".format(_datetime.today().strftime("%d-%m-%Y"))))

    # Check installation paths
    abs_couplingpath = _path.abspath(couplingpath)
    if not _path.isdir(abs_couplingpath):
        raise SystemExit("Path to the coupling repository does not exist {}".format(
            abs_couplingpath))
    
    if trackerpath == "default":
        tracker_path = _path.join(abs_couplingpath, "sixtrack/SixTrack")
    else:
        tracker_path = _os.path.abspath(trackerpath)

    if not _path.isfile(tracker_path):
        raise SystemExit("Tracker executable not found in {}, please build or symlink it there.\n"
                         "".format(_path.dirname(tracker_path)))

    server_path = _path.join(abs_couplingpath, "fluka/flukaserver")  # This is checked in the job

    if flukapath is "default":
        if "FLUKAPRO" in list(_os.environ.keys()):
            abs_flukapath = _os.environ["FLUKAPRO"]
        else:
            raise SystemExit("Could not find the FLUKAPRO enviromental variable.\n"
                             "Please provide the path to the Fluka installation as an argument.")
    else:
        abs_flukapath = _path.abspath(flukapath)
        if not _path.isdir(flukapath):
            raise SystemExit("Path to Fluka installation "
                             "does not exist {}".format(abs_couplingpath))

    condor_queues = ["espresso", "microcentury", "longlunch", "workday",
                     "tomorrow", "testmatch", "nextweek"]

    if queue not in condor_queues:
        raise SystemExit("Invalid queue: {},\n possible queues are {}".format(
            queue, ", ".join(condor_queues)))

    # Check directories and present input files
    abs_inputpath = _path.abspath(inputfile)  # Work in absolute paths
    study_dir = _path.dirname(abs_inputpath)
    study_name = _path.basename(abs_inputpath).replace(".inp", "")
    
    def _pj(filename, sourcedir=study_dir):  # append the absolute source path to the filename
        return _path.abspath(_path.join(sourcedir, filename))

    if not abs_inputpath.endswith(".inp"):
        inputfile = "{}.inp".format(abs_inputpath)
    else:
        inputfile = abs_inputpath

    workdirname = _pj(study_name) if not workdir or workdir == "default" else workdir
    distfile = "initial.dat" if beamfile is None else beamfile

    # Some of the names are assigned dynamically, but all files are checked
    required_files = [_pj("fort.2"), _pj("fort.3"), _pj("insertion.txt"),
                      inputfile, _pj(limifile), _pj(distfile)]

    required_files.extend([_pj(extra_file) for extra_file in additional])

    for f in required_files:
        if not _path.isfile(f):
            print(("Required files (some may be user-specified!):\n{}".format(
                ",\n".join([_os.path.basename(req) for req in required_files]))))
            raise SystemExit("Could not find required file {}".format(f))

    # Optional files that would be copied if present, but won't be required
    optional_files = [_pj("relcol.dat")]

    # Perform some checks if all files are present:
    # If initials.dat is used, check if it is long enough:
    npart_job = get_npart_fort3(_pj("fort.3"))
    if distfile.endswith("initial.dat"):
        nlines = count_lines(_pj(distfile))
        needed = njobs * npart_job
        if nlines < needed:
            raise SystemExit("Insufficient number of particles in intial.dat\n"
                             "Need {}x{}={}, got {}".format(njobs, npart_job, 
                                                            needed, nlines))

    if verbose:
        print(("--> Study name: {}".format(study_name)))
        print(("--> Coupling installation: {}".format(abs_couplingpath)))
        print(("--> Fluka installation: {}".format(abs_flukapath)))
        print(("--> Full path to tracker: {}".format(tracker_path)))
        print(("--> Full path to server: {}".format(server_path)))
        if beamfile:
            print("--> Beam generated by gpdist at execution")
        if limifile:
            print(("--> Using provided file {} as aperture defintion".format(limifile)))
        print(("--> Queue selected: {}".format(queue)))
        print(("--> Number of jobs: {}".format(njobs)))
        print(("--> Number of cycles: {}".format(ncycles)))

    if local and njobs > 10:
        valid = False
        while not valid:
            answer = eval(input("--> **You are about to to run {} jobs locally.\n"
                               "--> **Are you sure? (y/n) ".format(njobs)))
            if answer.lower().startswith("y"):
                valid = True
                print("--> Fine, here you go!")
            elif answer.lower().startswith("n"):
                print("--> Phew, that was a close one!")
                return

    # Prepare the top-level directory structure
    abs_workdir = _path.abspath(workdirname)
    clean_inputdir = _path.join(abs_workdir, "clean_input")
    emails_dir = _path.join(abs_workdir, "emails")

    if not _path.exists(clean_inputdir):
        if verbose:
            print(("--> Preparing input in: {}".format(abs_workdir)))
        _os.makedirs(clean_inputdir)  # This makes the workdir as well
        _os.makedirs(emails_dir)
        for f in required_files:
            if f == _pj(limifile):  # Rename the limi file
                _shutil.copy2(f, _path.join(clean_inputdir, "fort3.limi"))
            else:  # Copy the rest of the required files as they are
                _shutil.copy2(f, clean_inputdir)
        
        for of in optional_files:
            if _path.exists(of):
                _shutil.copy2(of, clean_inputdir)
    else:
        if verbose:
            print(("--> Directory already exists, extending the run: {}".format(abs_workdir)))

    # Determine the run numbers/seeds
    existing_runs = [d for d in _os.walk(abs_workdir).next()[1] if "run_" in d]
    if existing_runs:
        last_run = [int(d.replace("run_", "")) for d in existing_runs][-1]
    else:
        last_run = 0

    rfirst = last_run + 1
    rlast = rfirst + int(njobs) - 1

    if verbose:
        print(("\n  **Running seeds from {} to {}...\n".format(rfirst, rlast)))

    # Load the postprocessing block if any
    postproc = None
    if postprocfile:
        with open(_path.abspath(postprocfile), "r") as f:
            postproc = f.read()

    # Common variables for all runs
    common_var_dict = {
        "studyname": study_name,
        "queue": queue,
        "workdir": abs_workdir,
        "flukapath": abs_flukapath,
        "couplingpath": abs_couplingpath,
        "trackerpath": tracker_path,
        "serverpath": server_path,
        "flukahppath": _os.path.join(flukapath, "fluka/flukahp"),
        "npart": npart_job,
        "beamfile": _os.path.basename(beamfile),
        "postproc": postproc
        }

    # Prep over - show time!

    # Make all run directories and populate them with the job files
    # This is done in parallel so no order here
    # for run_num in range(rfirst, rlast + 1): #Single process version
    #     makeSingleRun(run_num, **run_var_dict)

    if verbose:
        print("--> Preparing job directories...")

    pool = _multiprocessing.Pool(processes=16)
    pool.map(_partial(make_single_run, var_dict=common_var_dict), list(range(rfirst, rlast + 1)))

    # Populate the superjob files with the jobs in order
    make_super_jobs(rfirst, rlast, ncycles, var_dict=common_var_dict)

    # Get the condor config file and set the queue
    copy_submit_config(abs_workdir, queue)

    # All ready - submit
    owd = _os.getcwd()  # save the current working directory to return to it later
    _os.chdir(abs_workdir)
    if not local:
        if verbose:
            print("--> Submitting jobs to HTCondor")

        batch_name = "{}__{}__{}".format(rfirst, rlast, _path.basename(abs_workdir))
        status = _call(["condor_submit", "-batch-name", batch_name, "htcondor.sub"])
        # status = 0 #for debugging
        if status != 0:
            _os.chdir(owd)
            raise SystemExit("Uh oh! Submission failed!")

    else:
        if verbose:
            print("--> Running jobs locally")
        with open("jobs.txt", "r") as f:
            for line in f:  # Execute as normal bash scripts
                if verbose:
                    print(("--> * Running {}".format(line.strip())))
                _Popen(["./{}".format(line.strip())])  # Use popen as no need to wait
                # for debugging
                # _call(["echo", "Hurr-durr, I am shell and I'm running {}".format(line.strip())])

    _os.unlink("jobs.txt")
    _os.chdir(owd)
    print("--> Done!")
