import os as _os
import glob as _glob
import gzip as _gzip
import numpy as _np


def _fatal(message):
    raise SystemExit(message)


def _eval_string_operator(operator, lhs, rhs):
    result = None
    if operator == "<":
        result = lhs < rhs
    elif operator == ">":
        result = lhs > rhs
    elif operator == "=":
        result = lhs == rhs
    else:
        _fatal("Operator not supported {}".format(operator))
    return result


class SixTrackDump(object):
    def __init__(self, study_directory, dump_filename, filterstring="", skiplines=0,
                 outputfilename="merged_dump_data.dat", imax=-1, outputdir=-1, debug=False):
        """
        Assumes a structure like:

        study_directory
          |
          --> runxxx
          |    |
          |    --> *<dumpfilename>*
          |
          --> runxxx
          |    |
          |    --> *<dumpfilename>*
         ...

        This script merges multiple trajectory dump files into one file. It implements
        stream reading with on-the fly filter conditions to help reducing unneccessary
        data processing. Files compressed with gzip are supported.
        """

        self.studydir = _os.path.abspath(study_directory)
        if imax < 0:
            self.imax = None
        else:
            self.imax = int(imax)

        if str(outputdir) == "-1":
            self.outputdir = self.studydir
        else:
            self.outputdir = _os.path.abspath(outputdir)

        self.dfilename = dump_filename
        self.outputfilename = outputfilename

        self.filterstring = filterstring
        self.skiplines = skiplines

        self.merged_data = []
        self.lines_read = 0
        self.lines_added = 0

        self.data_merged = _np.array([])

        self.debug = debug

        print(("Study directory: ", self.studydir))
        print(("Output directory: ", self.outputdir))
        print(("Output filename: ", self.outputfilename))
        print(("Looking for files with name containing: {}".format(self.dfilename)))

        self.load()
        self.write()

        print(("Data lines read: {}".format(self.lines_read)))
        print(("Data lines added: {}".format(self.lines_added)))

    def selective_load(self, filepath, filterstring, skiplines):  # TODO Move to DataLoader class
        """
        If the data is too much to handle in one go this allows streamed processing
        with filters on values.

        Use a semicolon-separated list of conditions of the form:

        @columnIndex <,>,= value

        eg.

        filterstr = '@1<10; @2>3; @5=1'
        """
        # Unpack into individual conditions and strip spaces
        conditions = filterstring.strip().split(";")
        conditions = [val.strip() for val in conditions if val]

        # Check always 1 and only 1 operator in every condition
        operators = frozenset(["<", ">", "="])
        operators_found = []
        for cnd in conditions:
            found = set(cnd) & operators
            operators_found.append(found)

        single_and_unique = [False]*len(conditions)
        for i, op in enumerate(operators_found):
            cnd = conditions[i]

            if len(op) == 1:
                if cnd.count(list(op)[0]) == 1:
                    single_and_unique[i] = True

        if not all(single_and_unique):
            _fatal("Invalid filter operator configuration {}".format(filterstring))

        data = []

        if filepath.endswith(".gz"):
            myopen = _gzip.open
        else:
            myopen = open

        with myopen(filepath) as filein:
            for i in range(skiplines):
                line = filein.readline()

            while line:
                line_data = line.split()
                add_line = True  # Add every line by default unless a condition fails

                for cnd in conditions:
                    operator = list(operators_found[conditions.index(cnd)])[0]
                    target, value = cnd.split(operator)

                    if target[0] == "@":
                        target_stripped = target[1:]
                    else:
                        _fatal("Invalid index formatting {}, use @{}".format(target, "index"))

                    try:
                        value_rendered = float(value)
                    except ValueError:
                        _fatal("Invalid fliter value: {}".format(value))

                    index = int(target_stripped)

                    # expression = "{} {} {}".format(index, operator, value_rendered)
                    if not _eval_string_operator(operator, float(line_data[index]), value_rendered):
                        add_line = False

                if add_line:
                    data.append(line_data)
                    self.lines_added += 1

                line = filein.readline()
                self.lines_read += 1

        return data

    def load(self):
        print("Loading data...")
        rundirs = [_os.path.join(self.studydir, each) for each in _os.listdir(self.studydir)
                   if each.startswith("run_")][:self.imax]

        # Just in case the data is extacted in a signle directory
        if not rundirs:
            print("No directories run_xxxx found, look for files in the study dir")
            dump_files = _glob.glob(_os.path.join(self.studydir, "*{}*".format(self.dfilename)))

        else:
            dump_files = [_glob.glob(_os.path.join(path, "*{}*".format(self.dfilename)))[0]
                          for path in rundirs
                          if _glob.glob(_os.path.join(path, "*{}*".format(self.dfilename)))]

        if not dump_files:
            raise SystemExit("No files found. Stop.")

        def _load_and_print_name(infile):
            if self.debug:
                print(infile)
            # loaded = _np.genfromtxt(infile)
            loaded = self.selective_load(infile, filterstring=self.filterstring,
                                         skiplines=self.skiplines)
            return loaded

        # data = [_np.genfromtxt(fn) for fn in dump_files] #load the data, 1 entry per touch
        data = [_load_and_print_name(fn) for fn in dump_files]  # load the data, 1 entry per touch

        # Merge the data from all files, sort on turn number and unpack into columns
        self.data_merged = list(zip(*sorted(_np.vstack(data), key=lambda tup: tup[1])))
        print((len(self.data_merged[0])))

    def write(self):
        outputfilename = _os.path.join(self.outputdir, self.outputfilename)
        header = " ID turn s[m] x[mm] xp[mrad] y[mm] yp[mrad] z[mm] dE/E[1] ktrack"
        # dmpfmt2 = ["% 2d", "% 4d", "% .8E", "% .8E", "% .8E", "% .8E",
        #            "% .8E", "% .8E", "% .8E", "% 4d"]

        dmpfmt3 = "%s"

        _np.savetxt(outputfilename, _np.column_stack(self.data_merged), delimiter="\t",
                    fmt=dmpfmt3, header=header, comments="#")

        print(("File written: ", outputfilename))
