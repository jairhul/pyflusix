import os.path as _path
import glob as _glob
import numpy as _np
import pandas as _pd

import pyflusix.machine_parameters as _mcp

"""
Author: Pascal Hermes
Changes by: Nuria Fuster Martinez, Andrey Abramov
"""


class LoadStudy(object):

    def __init__(self, studydir, inpdir=None, twissfile=None, machine=None, startS=0,
                 verbose=True, debug=False, beam=1):
        """
        A class to load and process the compressed output from a simulations run.
        This is intended to run after HiSixInputCompression (compress_orignals.py)
        has been executed. The loaded data includes aperture and collimator losses
        and the processing consits of syncronising S positions of collimators
        to MADX reference frame set by the TFS file and splitting aperture losses
        into cold and warm based on the type of machine used.
        """

        self.verbose = verbose  # Control the output from the analysis
        self.debug = debug
        if self.debug:
            self.verbose = True

        self.beam = beam

        self.s_start = startS
        self._fort2 = None  # Can optionally provide the fort.2 to work out the starting S

        supported_machines = ['lhc', 'hllhc', 'fcc', 'sps',
                              'ps']  # TODO: Rework this to use MachineParameters
        if not machine:
            msg = 'Please specify a machine. \n Supported machines are: {}'
            raise SystemExit(msg.format(', '.join(supported_machines)))

        if machine not in supported_machines:
            msg = 'Unknown machine, {}\n Supported machines: {}'
            raise SystemExit(msg.format(machine, ', '.join(supported_machines)))
        else:
            self.machine = machine

        try:
            self.s_start = float(startS)
        except ValueError:
            if _path.isfile(startS):
                self._fort2 = _path.abspath(startS)
            else:
                msg = ('Invalid startS argument: {}, please use a number '
                       'or a valid path to a fort.2 file')
                raise ValueError(msg.format(startS))

        self.dmachine = _mcp.MachineParameters()  # Make an empty instance for now to use the methods

        self._outdir = _path.join(_path.abspath(studydir), 'output')
        if inpdir:
            self._inpdir = _path.abspath(inpdir)
        else:
            self._inpdir = _path.join(_path.abspath(studydir), 'input')

        self._check_required_files()

        if twissfile:
            self._twissfile = _path.abspath(twissfile)
        else:
            if _glob.glob(_path.join(self._inpdir, '*.tfs')):
                self._twissfile = _glob.glob(_path.join(self._inpdir, '*.tfs'))[0]
            else:
                raise IOError("Tiwss file not found, please check the input")

        if self.verbose:
            print(("Outputdir: {}".format(self._outdir)))
            print(("Inputdir: {}".format(self._inpdir)))

        self._read()

    def _check_file_exits(self, filepath):
        if _path.isfile(filepath):
            if self.verbose:
                print(("Found file: {}".format(filepath)))
        else:
            raise IOError("File not found: {}".format(filepath))

    def _check_required_files(self):

        if not _path.isdir(self._outdir):
            msg = ("The directory {} does not exist, please check the input"
                   "\n and ensure you've compressed the study first using {}")
            raise IOError(msg.format(self._outdir, "compress_orginals.py"))

        if not _path.isdir(self._inpdir):
            msg = "The directory {} does not exist, please check the input"
            raise IOError(msg.format(self._inpdir))

        # check the existence of the relevant input files
        required_paths = [
            _path.join(self._outdir, "fort.208"),  # Collimator losses
            _path.join(self._outdir, "fort.999.comp"),  # Compressed aperture losses
            _path.join(self._inpdir, "collgaps.dat"),  # Collgaps file
        ]

        for path in required_paths:
            self._check_file_exits(path)

    def _read(self):

        # COLLIMATOR LOSSES
        # read the collimator losses

        closs = _pd.read_csv(self._outdir + '/' + 'fort.208',
                             delim_whitespace=True,
                             names=['CID', 'hits', 'elost'])

        if self.verbose:
            print(('Read Fort.208:', len(closs), 'lines'))

        # READ CORRECTION FILE FORT.66 and apply correction to the energy at the collimators
        corr = _pd.read_csv(self._outdir + "/" + 'fort.66',
                           delim_whitespace=True,
                           names=['CID', 'sumA', 'elost', 'elos_protons', 'elost_neutrons',
                                  'elost_others'])
        print(corr)
        if self.verbose:
            print(('Read Fort.66:', len(corr), 'lines'))

        corr_energy = _np.zeros(len(closs))
        for i in range(0, len(closs)):
            correction = 0
            coll_ID = closs.CID.values[i]
            closs_to_corr = closs[closs.CID == coll_ID]
            leakage = corr[corr.CID == coll_ID]
            if len(leakage) != 0:
                correction = closs_to_corr.elost.values - leakage.elost.values
                corr_energy[i] = correction
            else:
                corr_energy[i] = closs_to_corr.elost

        closs.elost = corr_energy

        # read the collgaps file
        names = ['CID', 'name', 'angle', 'betax', 'betay', 'halfgap', 'material',
                 'len', 'six', 'sigy', 'tilt1', 'tilt2', 'nsig']

        collg = _pd.read_csv(self._inpdir + '/' + 'collgaps.dat',
                             skiprows=1, delim_whitespace=True,
                             names=names)
        collg = collg[['CID', 'name', 'halfgap', 'nsig', 'len']]

        if self.verbose:
            print(('Read collgaps:', len(collg), 'lines'))

        # read the twiss file

        twiss_header, header_line_no =  _mcp.get_twiss_header(self._twissfile)
        twiss = _pd.read_csv(self._twissfile, skiprows=header_line_no + 1,
                             # Remove all non-data lines
                             delim_whitespace=True, names=twiss_header)

        twiss = twiss[['NAME', 'S']]
        twiss.columns = ['name', 's']

        if self.verbose:
            print(('Read twiss:', len(twiss), 'lines'))

        # merge the dataframes into one

        coll = _pd.merge(twiss, collg, how='right', left_on=['name'],
                         right_on=['name'])

        if self.debug:
            print('Coll after merge of twiss and collgaps')
            print(coll)

        coll = _pd.merge(closs, coll, how='right', left_on=['CID'],
                         right_on=['CID'])

        if self.debug:
            print('Coll after merge of fort.208 and coll')
            print(coll)

        self.coll = coll

        # APERTURE LOSSES

        aper = _pd.read_csv(self._outdir + '/' + 'fort.999.comp',
                            delim_whitespace=True, skiprows=1,
                            names=['s', 'elost', 'A', 'Z', 'temp'])

        if self.debug:
            print('Aperture from fort.999')
            print(aper)

        if self._fort2:
            # Look for the starting S position by finding the starting element in the fort.2 and
            # obtaining its S position in the Twiss file. This is done in the
            # Machine Parameters class
            self.dmachine.load_from_tfs_fort2(self._twissfile, self._fort2)
            self.s_start = self.dmachine.s_start

        if self.s_start > 0:
            if self.verbose:
                print('Adding an S offset to the S column fort.999 to match the S from TFS')

            totalS = max(twiss["s"])
            aper['s'] = (aper['s'] + self.s_start) % totalS

        if self.debug:
            print("Aperture after S offset")
            print(aper)

        # add a column indicating the temperature

        aper = aper.assign(temp=_pd.Series(['cold'] * len(aper)).values)

        # assign warm regions
        if self.machine == 'lhc':
            warmregions = LHC_WARM_REGIONS
        elif self.machine == 'hllhc':
            warmregions = HLLHC_WARM_REGIONS
        elif self.machine == 'fcc':
            warmregions = FCC_WARM_REGIONS
        elif self.machine == 'sps':
            warmregions = SPS_WARM_REGIONS
        elif self.machine == 'ps':
            warmregions = PS_WARM_REGIONS
        else:
            raise ValueError("Unrecognised machine: {}".format(self.machine))

        if self.beam not in [1,2,4,24]:
            raise ValueError("Beam must be one of 1, 2, 4, 24")

        for (i, f) in warmregions:
            if self.beam in [2, 24]:
                aper.loc[(max(twiss['s']) - aper['s'] > i) & (max(twiss['s']) - aper['s'] < f), 'temp'] = 'warm'
            else:
                aper.loc[(aper['s'] > i) & (aper['s'] < f), 'temp'] = 'warm'

        self.aper = aper

        if self.debug:
            print('Aperture adding a column with the temperature')
            print(aper)

        # create DataFrames with warm and cold losses

        self.cold = aper[aper.temp == 'cold']
        self.warm = aper[aper.temp == 'warm']

        if self.debug:
            print('DataFrame with cold losses')
            print('\n<<<==============================')
            print((self.cold))
            print('\n==============================>>>')

        if self.debug:
            print('DataFrame with warm losses')
            print('\n<<<==============================')
            print((self.warm))
            print('==============================>>>\n')

        # get normalisation
        self.norm = coll.elost.sum()

        if self.debug:
            print('Normalization')
            print((self.norm))


class Penetration(object):
    def __init__(self, outdir):
        self._impactsfile = _path.join(outdir, "fort.impacts")
        _info_file = _path.join(outdir, "machine_info.txt")

        self.machine = _mcp.MachineParameters()
        self.machine.load_from_infofile(_info_file)
        self.penetration = None

    def get_ppd(self, s0, s1, sf):
        p = _pd.read_csv(self._impactsfile, delim_whitespace=True,
                         names=['s', 'e', 'a', 'z', 'x', 'xp', 'y', 'yp', 'pid', 'genid', 'cid'])

        p.s = p.s.apply(self.machine.shift_S)

        p = p[(p.s > s0) & (p.s < s1)]

        ds = sf - p.s  # delta in length
        ppd = (ds * p.xp).abs()  # calculate the PPD

        p = p.assign(ds=ds, ppd=ppd)

        self.penetration = p

        # print 'Get penetration\n {}'.format(p)


# TODO: Move warm regions out to external file
SPS_WARM_REGIONS = [0, 1E9]  # All warm for the SPS and PS
PS_WARM_REGIONS = [0, 1E9]

FCC_WARM_REGIONS = _np.array([[4.38339376e+04, 4.50339376e+04],
                              [7.340721596e+04, 7.59198009e+04]])

LHC_WARM_REGIONS = _np.array([
    [0.00000000e+00, 2.25365000e+01],
    [5.48530000e+01, 1.52489000e+02],
    [1.72165500e+02, 1.92400000e+02],
    [1.99484700e+02, 2.24300000e+02],
    [3.09545428e+03, 3.15562858e+03],
    [3.16774008e+03, 3.18843308e+03],
    [3.21144458e+03, 3.26386758e+03],
    [3.30990008e+03, 3.35497408e+03],
    [3.40100558e+03, 3.45342858e+03],
    [3.47644008e+03, 3.49406558e+03],
    [3.50588528e+03, 3.56831858e+03],
    [6.40540880e+03, 6.45791380e+03],
    [6.46877850e+03, 6.85951380e+03],
    [6.87037850e+03, 6.92353380e+03],
    [9.73590702e+03, 9.82473052e+03],
    [9.83083202e+03, 9.86173052e+03],
    [9.87873202e+03, 9.93998552e+03],
    [9.95054802e+03, 1.00434620e+04],
    [1.00540245e+04, 1.01152780e+04],
    [1.01322795e+04, 1.01639705e+04],
    [1.01700720e+04, 1.02576030e+04],
    [1.31049892e+04, 1.31298045e+04],
    [1.31368892e+04, 1.31571237e+04],
    [1.31768002e+04, 1.32716472e+04],
    [1.33067527e+04, 1.33518257e+04],
    [1.33869312e+04, 1.34817782e+04],
    [1.35014547e+04, 1.35227845e+04],
    [1.35298692e+04, 1.35546845e+04],
    [1.63946378e+04, 1.64508713e+04],
    [1.64569728e+04, 1.64872713e+04],
    [1.64933728e+04, 1.68308713e+04],
    [1.68369728e+04, 1.68672713e+04],
    [1.68733728e+04, 1.69282948e+04],
    [1.97740644e+04, 2.02179087e+04],
    [2.30899797e+04, 2.31385770e+04],
    [2.31503967e+04, 2.31713755e+04],
    [2.31943870e+04, 2.32468100e+04],
    [2.32928425e+04, 2.33379155e+04],
    [2.33839480e+04, 2.34363710e+04],
    [2.34593825e+04, 2.34800825e+04],
    [2.34921940e+04, 2.35531160e+04],
    [2.64334879e+04, 2.64583032e+04],
    [2.64653879e+04, 2.64867177e+04],
    [2.65063942e+04, 2.66012412e+04],
    [2.66363467e+04, 2.66588832e+04],
])

HLLHC_WARM_REGIONS = _np.array([
    [0.00000000e+00, 2.25000000e+01],
    [8.31530000e+01, 1.36689000e+02],
    [1.82965500e+02, 2.01900000e+02],
    [2.10584700e+02, 2.24300000e+02],
    [3.09545428e+03, 3.15562858e+03],
    [3.16774008e+03, 3.18843308e+03],
    [3.21144458e+03, 3.26386758e+03],
    [3.30990008e+03, 3.35497408e+03],
    [3.40100558e+03, 3.45342858e+03],
    [3.47644008e+03, 3.49406558e+03],
    [3.50588528e+03, 3.56831858e+03],
    [6.40540880e+03, 6.45791380e+03],
    [6.46877850e+03, 6.85951380e+03],
    [6.87037850e+03, 6.92353380e+03],
    [9.73590702e+03, 9.82473052e+03],
    [9.83083202e+03, 9.86173052e+03],
    [9.87873202e+03, 9.93998552e+03],
    [9.95054802e+03, 1.00434620e+04],
    [1.00540245e+04, 1.01152780e+04],
    [1.01322795e+04, 1.01639705e+04],
    [1.01700720e+04, 1.02576030e+04],
    [1.31036000e+04, 1.31200300e+04],
    [1.31238892e+04, 1.31471237e+04],
    [1.31918002e+04, 1.32476472e+04],
    [1.33067940e+04, 1.33520892e+04],
    [1.34110312e+04, 1.34670082e+04],
    [1.35114547e+04, 1.35357845e+04],
    [1.35388592e+04, 1.35552845e+04],
    [1.63946378e+04, 1.64508713e+04],
    [1.64569728e+04, 1.64872713e+04],
    [1.64933728e+04, 1.68308713e+04],
    [1.68369728e+04, 1.68672713e+04],
    [1.68733728e+04, 1.69282948e+04],
    [1.97348504e+04, 1.97606997e+04],
    [1.97715644e+04, 2.02179087e+04],
    [2.02287734e+04, 2.02529744e+04],
    [2.30899797e+04, 2.31385770e+04],
    [2.31503967e+04, 2.31713755e+04],
    [2.31943870e+04, 2.32468100e+04],
    [2.32928425e+04, 2.33379155e+04],
    [2.33839480e+04, 2.34363710e+04],
    [2.34593825e+04, 2.34800825e+04],
    [2.34921940e+04, 2.35531160e+04],
    [2.64334879e+04, 2.64483032e+04],
    [2.64569832e+04, 2.64759232e+04],
    [2.65221932e+04, 2.65757332e+04],
    [2.66363832e+04, 2.66588832e+04],
])
