import copy as _copy
import warnings as _warnings

import pyflusix.data_loader as _cdl


class FlukaIsotopes(object):
    """
    Class that handles the loading and processing of fluka isotope information.
    This information is needed in order to connect the particle and parent id
    from SixTrack to a particle produced in Fluka. As all ids are counted fresh
    for every single run this class provides facillities to distinguish ids
    between different runs loaded by mangling the id and the run number
    into a unique id
    """
    def __init__(self):
        self.files_loaded = 0
        self.increment = int(1E6)  # Increment between files to make IDs unique
        self.isotopes = {}
        self.columns = []

    def __add__(self, other):
        result = _copy.deepcopy(self)

        # Need to offset the particle id to avoid clashes
        offset = (self.files_loaded + 1)*self.increment
        for pid in other.isotopes:
            data = other.isotopes[pid]
            data[0] = data[0] + offset
            result.isotopes[pid + offset] = data

        # The columns should always be the same
        result.columns = other.columns
        result.files_loaded = self.files_loaded + other.files_loaded

        return result

    def load(self, filepath, strict=True):
        """
        Loads the data from a Fluka isotpe file while enforcing unique particle ids between runs.

        Inputs:
          filepath (str) - path to the input file
          strict (bool)  - determines the behaiour when a file loading fails - if True it raises an
          exception, while if False it prints a warning and continues
        """
        try:
            self.columns = ["PID", "GENID", "COLLID", "A", "Z", "E"]
            flulog = _cdl.ColumnData(filepath, verbose=False)
            flulog.names = self.columns
        except:
            if not strict:
                _warnings.warn("Couldn't load file {}".format(filepath))
                return
            else:
                raise

        for row in flulog.iterrows():
            unique_partid = self.make_id_unique(row["PID"])
            unique_genid = self.make_id_unique(row["GENID"])  # Make sure no duplicate IDs

            self.isotopes[unique_partid] = [unique_genid, int(row["COLLID"]),
                                            int(row["A"]), int(row["Z"]), float(row["E"])]

        self.files_loaded += 1  # Keep track of number of files loaded

    def make_id_unique(self, pid):
        """
        Makes an id unique by adding the number of files already loaded times a
        constant increment which is larger than the maximum possible id

        Inputs:
          pid (int) - id  of a particle

        Returns:
          unique_pid (int) - the unique id
        """
        unique_pid = int(pid) + self.files_loaded*self.increment
        return unique_pid

    def decode_unique_id(self, uid):
        """
        Decodes a unique id of a particle into a run number and an id of the
        particle within that run. Can in order to track a particular particle to
        an individual run.

        Inputs:
          uid (int) - the unique id of a partice

        Returns:
          run number, particle id (tuple) - the run number and the particle id within that run
        """
        uid = int(uid)
        partid = uid % self.increment
        run_num = uid / self.increment

        return run_num, partid

    def write(self, outpath, append=False, reset=False):
        """
        Writes all the data loaded to a file. By using the 'append' and
        'reset' options stream writing can be achieved - accumulating data from
        multiple input files onto an output file witout holding it in memeory

        Inputs:
          outpath (str) - path where to save the file
          append (bool) - append to or overwite file found
          reset  (bool) - delete data from memory after writing, but keep
        """
        if append:
            mode = "a"
        else:
            mode = "w"

        with open(outpath, mode) as fileout:
            if not append:
                header_format = "# {:<12}{:<12}{:<8}{:<8}{:<8}{:<25}\n"
                fileout.write(header_format.format(*self.columns))
            for uid in self.isotopes:
                data_format = "{:12d}{:12d}{:6d}{:6d}{:6d}{:25.9f}\n"
                fileout.write(data_format.format(uid, *self.isotopes[uid]))

        if reset:  # Remove the loaded data from memory
            del self.isotopes
            self.isotopes = {}
