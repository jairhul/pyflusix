import matplotlib.pyplot as _plt

import pyflusix.data_loader as _cdl


class SixTrackOptics(object):
    def __init__(self, filename):
        """
        This class is intented to load, manipulate and plot optics from a SixTrack
        optics summary. The optics summary is extracted by running the grepOptics.sh
        script over a screen dump of a SixTrack run.
        """
        # TODO: Add more plotting options - dispersion, alpha etc.
        self.filename = filename
        self.optics = None
        self.load_optics()

    def load_optics(self):
        # Currently a redudnant layer, but additional functionality may be added
        # herein the future
        data = _cdl.ColumnData(self.filename, skip_header=2)

    def filter_optics(self, filterstring):
        """
        Use the ColumnData's awk-like filtering
        interface to select a range of the optics - e.g an S range
        """
        self.optics.filter_data(filterstring)

    def plot_beta(self, figure=None):
        if not figure:
            fig = _plt.figure(1)
        else:
            fig = figure

        _plt.plot(self.optics["S"], self.optics["BETX"], label=r"$\beta_{x}$")
        _plt.plot(self.optics["S"], self.optics["BETY"], label=r"$\beta_{y}$")

        _plt.xlabel(r"$S$ [m]")
        _plt.xlabel(r"$\beta$ [m]")
        _plt.legend(fontsize="medium")

        return fig
