import warnings as _warnings
import matplotlib as _mpl
import matplotlib.pyplot as _plt
import numpy as _np
import pandas as _pd

import pyflusix.machine_parameters as _mcp


class SixTrackAperture(object):
    """
    Class that loads, manipulates and makes various plots of a SixTrack aperture model.
    The aperture model is loaded from an aperture file emitted by a PRIN statement in the
    LIMI block of a SixTrack simulation.
    """
    def __init__(self, aperfilename, s_start=None, s_total=None, debug=False):
        """
        start_max_S are used if it is neccessary to wrap the aperture around a point in the machine.
        In general aperture wrapping is discouraged.
        """
        self.debug = debug

        # Load an instance of the MachineParameters class to handle S transforms
        # If none of the cases below  works then no shift is perfomed
        self.machine = _mcp.MachineParameters()

        if s_start is not None and s_total is not None:  # Both values are set
            self.machine.user_defined_S(s_start, s_total)
            if debug:
                msg = "Applying user-defined S shift: startS = {} m , totalS = {} m"
                print((msg.format(self.machine.s_start, self.machine.length)))

        elif s_start is not None or s_total is not None:  # Only one value is set
            msg = "Cannot have startS set, but not totalS or vice-verse. Please set both of none."
            raise ValueError(msg)

        self.apertures = _np.array([])
        self.load_aperture(aperfilename)

        self.aper_data_dict = self.get_xy_extents()

        self.collimator_data = _pd.DataFrame()

    def load_aperture(self, aperfilename):
        self.apertures = _np.genfromtxt(aperfilename, comments="#", dtype=None, encoding=None)
        self.apertures = sorted(self.apertures, key=lambda tup: tup[2])

    def load_collimator_info(self, collgaps, twissfile):
        # same procedure as in output.py
        twiss_header, header_line_no = _mcp.get_twiss_header(twissfile)
        twiss = _pd.read_csv(twissfile, skiprows=header_line_no + 1,  # Remove all non-data lines
                             delim_whitespace=True, names=twiss_header)

        twiss = twiss[['NAME', 'S']]
        twiss.columns = ['name', 's']

        names = ['CID', 'name', 'angle', 'betax', 'betay', 'halfgap', 'material',
                 'len', 'six', 'sigy', 'tilt1', 'tilt2', 'nsig']
        collg = _pd.read_csv(collgaps,
                             skiprows=1, delim_whitespace=True,
                             names=names)
        collg = collg[['CID', 'name', 'halfgap', 'nsig', 'len', 'angle']]

        self.collimator_data = _pd.merge(twiss, collg, how='right',
                                         left_on=['name'], right_on=['name'])

    def get_xy_extents(self):
        names = []
        S = []
        x_min = []
        x_max = []
        y_min = []
        y_max = []
        apertype = []
        symmetric = []

        for aper in self.apertures:
            names.append(aper[0])
            apertype.append(aper[1])

            S.append(self.machine.shift_S(aper[2]))

            # Takes care of offsets and beampipe rotation
            #                                            name aptype            s[m]       aper1[mm]       aper2[mm]  aper3[mm][rad]  aper4[mm][rad]  aper5[mm][rad]  aper6[mm][rad]  aper7[mm][rad]  aper8[mm][rad]      angle[rad]        xoff[mm]        yoff[mm]
            # New Sixtrack Format: #TODO: Interface format selection to user via an option
            ext = self.get_aperture_extents(aper[3], aper[4], aper[5], aper[6],
                                            aper[1], aper[9], aper[10], aper[11])

            # Old Sixtrack Format:
            # ext = get_aperture_extent(aper[3], aper[4], aper[5], aper[6],
            #                           aper[1], aper[7], aper[8], aper[9])

            x_min.append(ext[0])
            x_max.append(ext[1])
            y_min.append(ext[2])
            y_max.append(ext[3])

            issymmetric = False
            if ext[0] == -ext[1] and ext[2] == -ext[3]:
                issymmetric = True

            symmetric.append(issymmetric)

        # Make into an array to sort
        aper_data = _np.array([names, apertype, S, x_min, x_max, y_min, y_max, symmetric],
                              dtype=object)

        # If the S coodinate has been shifted we must sort the aperture again on S
        if self.machine.s_start:
            aper_data = aper_data[:, aper_data[2, :].argsort()]

        aperture_extent_dict = {"names": aper_data[0],
                                "apertype": aper_data[1],
                                "S": aper_data[2],
                                "x_min": aper_data[3],
                                "x_max": aper_data[4],
                                "y_min": aper_data[5],
                                "y_max": aper_data[6],
                                "symmetric": aper_data[7]}

        print(("Total number of apertures: {}".format(len(S))))
        print(("Found aperture types {}".format(", ".join(list(set(apertype))))))

        return aperture_extent_dict

    def get_aperture_extents(self, aper1, aper2, aper3, aper4,
                             aper_type, angle=0, xoffs=0, yoffs=0):
        """
        Get the maximum +ve and negative half extent in x and y for a
        given aperture model and (up to) four aperture parameters.

        returns xMin, xMax, yMin, yMax
        """

        # Obtain the extents from the geometry of the beampipe
        x = aper1
        y = aper2

        if aper_type == 'CR':
            x = aper1
            y = aper1
        if aper_type in ['RE', 'EL', 'OC']:
            x = aper1
            y = aper2
        elif aper_type == 'RL':
            x = min(aper1, aper3)
            y = min(aper2, aper4)
        elif aper_type == 'RT':
            x = aper3 + aper1
            y = aper2 + aper3

        elif aper_type == 'TR':
            # This is interpolated aperture between different aperture sizes and/or types
            # The aperture extent is then defined by the minimum of all possible apertures
            # given the parameters

            apertypes_to_check = ['CR', 'RE', 'EL', 'OC', 'RL', 'RT']
            x_apers = []
            y_apers = []

            if self.debug:
                print("TR aperture, check all types:")

            # Ignore offset and angle when comparing apertures
            # and only compare +ve half-extent in each plane

            for check_type in apertypes_to_check:
                _, x_check, _, y_check = self.get_aperture_extents(aper1, aper2, aper3, aper4,
                                                                   check_type)

                x_apers.append(x_check)
                y_apers.append(y_check)

            if self.debug:
                print(("Min in x from type {}".format(apertypes_to_check[x_apers.index(min(x_apers))])))
                print(("Min in y from type {}".format(apertypes_to_check[y_apers.index(min(y_apers))])))

            x = min(x_apers)
            y = min(y_apers)

        # Apply the rotation
        if angle > 0:
            msg = "Aperture rotation not impleneted yet.\n Rotation by {} not applied"
            _warnings.warn(msg.format(angle))

        # Apply the offset
        x_max = x - xoffs
        x_min = -x - xoffs

        y_max = y - yoffs
        y_min = -y - yoffs

        return x_min, x_max, y_min, y_max

    def plot_aperture_profile(self, plane="both", figure=None, outputfilename=None,
                              figuresize=(13, 5), linewidth=1):
        """
        The 'figure' argument must be a matplotlib.figure instance if it specified.
        This functionality is provided for cases when the aperture must be added
        to an existing plot.
        """
        if figure is None:
            fig = _plt.figure(1, figsize=figuresize)
        else:
            fig = figure

        data = self.aper_data_dict

        plot_hor = False
        plot_ver = False
        if str(plane) == "both":
            plot_hor = True
            plot_ver = True
        elif str(plane) == "horizontal":
            plot_hor = True
        elif str(plane) == "vertical":
            plot_ver = True
        else:
            msg = "Urecognised plane, {}, please specify 'horizontal', 'vertical' or 'both'."
            raise IOError(msg.format(plane))

        # Not strictly needed now, but will be needed when collimators are added
        legend_handles = []
        lw = linewidth  # shortcut
        if plot_hor:
            aperture_hor = _plt.plot(data["S"], data["x_max"], "b",
                                     label="horizontal aperture", linewidth=lw)

            _plt.plot(data["S"], data["x_min"], 'b', linewidth=lw)
            legend_handles.append(aperture_hor[0])
        if plot_ver:
            aperture_ver = _plt.plot(data["S"], data["y_max"], "g",
                                     label="vertical aperture", linewidth=lw)

            _plt.plot(data["S"], data["y_min"], 'g', linewidth=lw)
            legend_handles.append(aperture_ver[0])

        _plt.legend(handles=legend_handles)
        _plt.xlabel("S [m]")
        _plt.ylabel("Aperture Profile [mm]")

        if outputfilename:
            _plt.savefig(outputfilename, bbox_inches="tight")

        return fig

    def _plot_collimators(self, figure=None, plane="horizontal"):
        """
        Descriptive description
        """
        fig = figure
        data = self.aper_data_dict

        print((self.collimator_data['angle']))
        if plane == "horizontal":
            collimators = self.collimator_data.loc[self.collimator_data['angle'] < 1.3]
        else:
            collimators = self.collimator_data.loc[self.collimator_data['angle'] > 1.5]

        ax = _plt.gca()
        for index, coll in collimators.iterrows():
            # Make some rectangles for the TCLD collimator
            s_centre = coll["s"]
            sstart = s_centre - coll["len"]/2
            send = s_centre + coll["len"]/2
            hgap = coll["halfgap"]*1.e3

            w = send - sstart
            top_lim = _plt.ylim()[1]*2
            bot_lim = _plt.ylim()[0]*2
            h = top_lim - hgap
            upper_jaw = _mpl.patches.Rectangle((sstart, hgap), w, h,
                                               angle=0.0, color="k", zorder=10)
            lower_jaw = _mpl.patches.Rectangle((sstart, bot_lim), w, h,
                                               angle=0.0, color="k", zorder=10)

            ax.add_patch(upper_jaw)
            ax.add_patch(lower_jaw)

        # legend_handles = []  # Not strictly needed now, will be needed when collimators are added

        # _plt.legend(handles=legend_handles)
        # _plt.xlabel("S [m]")
        # _plt.ylabel("Aperture Profile [mm]")

        # _plt.show()
        return fig
