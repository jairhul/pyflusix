"""
Author: Pascal Hermes
Changes by: Nuria Fuster Martinez, Andrey Abramov
"""

from __future__ import print_function

import gzip as _gzip
import glob as _glob
import os as _os
import warnings as _warnings
import functools as _functools
import errno as _errno
import numpy as _np
import copy as _copy
import pickle as _cPickle

import pyflusix.fluka_isotopes as _fli
import pyflusix.machine_parameters as _mcp


'''
Author: Pascal Hermes
Changes by: Nuria Fuster Martinez, Andrey Abramov, Alessio Mereghetti
'''

def _progbar(progress):
    """
    Progressbar that turns a numerical fraction between 0 and 1 into
    updating visual bar on the screen
    """
    nmax = round(progress * 40, 1)
    nn = 0
    fs = ''
    while nn < nmax:
        fs = fs + '-'
        nn += 1
    while nn < 40:
        fs = fs + ' '
        nn += 1
    print('\r-->  [' + fs + '] (' + str(round(progress * 100, 1)) + '%)', end=' ')


def _read_file(filepath):
    """
    Would return a an apropriate file obejct depending on whether
    the file is gzipped or not. The file object is in read only
    mode
    """
    return _gzip.open(filepath) if filepath.endswith(".gz") else open(filepath, "r")


def _ensure_dir(path):
    """
    Makes a directory, if the directory exists does nothing
    """
    try:
        _os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == _errno.EEXIST and _os.path.isdir(path):
            pass
        else:
            raise


def _safeload(file_loader):
    """
    A simple decorator that wraps file loaders to handle corrupted or missing
    files without endless try-catch statements.
    It is the same as applying a try-catch around every file load.
    """

    @_functools.wraps(file_loader)
    def wrapper(*args, **kwargs):
        try:
            return file_loader(*args, **kwargs)
        except:
            _warnings.warn("Cannot load file: {}".format(args[1]))

    return wrapper


class HisixOutputCompression(object):
    """
    Class that handles the compression of hiSix ouput for lossmap generation
    """

    def __init__(self, study_dir=None, imax=-1, machine=None,
                 impacts=False, isotopes=False, verbose=True, **kwargs):

        """
        Arguments:
          study_dir (str) - path to directory containing run_xxxxx directories. Default is None
          imax (int)      - maximum number of run to be read. Default is all
          machine (str)   - type of machine. Default is None
          impacts (bool)  - add additional information about aperture losses
                            like transverse distribution, particle id etc. Default is False
          isotopes (bool) - inlcude processing of particle generation information. Default is False
          verbose (bool)  - print detailed information. Default is True

        Keyword Arguments:
          fort2file - path to fort.2 file if sourced from a non-standard location
          tfsfile   - path to TFS file if sourced from a non-standard location
        """

        self.collimator_leaking = {}
        self.collimator_losses = {}
        self.compressed_losses = {}
        self.aperture_losses = []
        self.aperture_impact = []

        self.runsread = 0  # number of runs read
        self.machine = machine
        self.machineparams = _mcp.MachineParameters()

        self.study_dir = study_dir
        self.verbose = verbose

        self.impact_distribution = impacts  # extract the impact distribution
        self.fluka_isotopes = isotopes  # merge the fluka isotope logs
        self.kwargs = kwargs

        # Initialise paths for output files
        self.correctionfile = ""
        self.collimatorfile = ""
        self.aperturlosfile = ""
        self.aperturcomfile = ""
        self.aperimpactfile = ""
        self.fluisotopefile = ""
        self.machineinfofile = ""

        self.fli = _fli.FlukaIsotopes()

        if study_dir is None:  # Allows having an empty instance for accumulation
            return

        self.study_dir = _os.path.abspath(study_dir)

        version = '0.2'
        if self.verbose:
            print("\n{}**** hiSixTrack post-processing ****\n {}Version {}\n\n".format(" " * 10,
                                                                                       " " * 15,
                                                                                       version))
            print("--> Compress the hiSixTrack output for loss map generation")

        if self.verbose:
            if self.impact_distribution:
                print('--> Including detailed impact distribution information.')
            if self.fluka_isotopes:
                print('--> Including Fluka isotope logs (includes unique particle id processing.)')

        aperturefile = 'aperture_losses.dat'
        collimatorfile = 'fort.208'
        flukaisotopefile = 'fluka_isotope.log'
        correctionfile = 'fort.66'

        if self.verbose:
            print('--> Using the following files:')
            print('--> Collimator losses:', collimatorfile)
            print('--> Aperture losses:  ', aperturefile)

        # Try to acquire machine info from auxiliary files
        self._get_machine_info()

        """
        Initialise a FlukaIsotopes instance to load the fluka_isotope.log files from
        the different run_xxxxx directories and also ensure the ids of all tracked particles
        and their originating parent particles are unique. For every individual run those
        ids are counted fresh and ids would be degendarate in the accumulated results.
        The ids are made unique by adding an offset of Nloaded*1E^6 to every id.

        For most purposes the mangled unique ids are useful as they are, but if need arises
        to determine from which invdividual run a particle came from, there is method in
        the FlukaIsotopes class called decodeUniqueID which takes in the id of a particle and
        returns a tuple of the run number and the id in this run. E.g

        run_number, part_id = fli.decodeUniqueID(unique_id)
        """

        # Initialise a path for the fluka isotopes write out file - write is streamed

        direct = _glob.glob(_os.path.join(self.study_dir, 'run*'))  # find directories to loop over
        ndirec = len(direct)  # number of directories to loop over

        if self.verbose:
            print('--> Number of runs    {}'.format(ndirec))
            print('--> Loading data:')

        ii = 0
        for (ii, rundir) in enumerate(direct):  # loop over directories
            # BREAK THE LOOP AT IMAX
            if self.runsread == imax:
                break

            # READ THE DATA
            for f in _os.listdir(rundir):
                fullpath = _os.path.join(rundir, f)
                if collimatorfile in f:
                    self.loadfile_collimator_loss(fullpath)
                if aperturefile in f:
                    self.loadfile_aperture_loss(fullpath)
                if flukaisotopefile in f and self.fluka_isotopes:
                    self.fli.load(fullpath, strict=False)
                if correctionfile in f:
                    self.loadfile_correction(fullpath)

            self.runsread += 1

            if self.verbose:
                _progbar(float(ii) / ndirec)

        if self.verbose:
            print()  # Opens a new line after the progressbar
            # print '--> Compressed runs:', self.runsread
            print('--> Total runs read: {}/{}'.format(ii + 1, ndirec))

        # SORT APERTURE LOSSES
        self.aperture_losses = _np.array(self.aperture_losses)

        if not self.aperture_losses.size:  # If there are no losses skip the compression
            return

        self.aperture_losses = self.aperture_losses[_np.argsort(self.aperture_losses[:, 0])]

        if self.verbose:
            print('--> Compressing data:')

        # Trivially parallel process, but requires shared resources for multiprocessing
        unique_s = _np.unique(self.aperture_losses[:, 0])

        compressed_losses = {}
        for s in unique_s:
            compressed_losses[s] = _np.array([0, 0, 0], dtype='float64')

        processed = 0
        ntotal = len(self.aperture_losses)  # For progress bar plotting only
        nstep = ntotal / 40 if ntotal > 40 else 1

        for loss in self.aperture_losses:
            compressed_losses[loss[0]] += _np.array(loss[1:])

            processed += 1
            if processed % nstep == 0:
                _progbar(float(processed) / ntotal)

        self.compressed_losses = compressed_losses

        # SORT APERTURE LOSSES WITH X,XP,Y,YP
        if self.impact_distribution:
            self.aperture_impact = _np.array(self.aperture_impact)
            self.aperture_impact = self.aperture_impact[_np.argsort(self.aperture_impact[:, 0])]
            self.aperture_impact = self.aperture_impact.tolist()

        if self.verbose:
            print()
            print("--> hiSixTrack compression run finished.")
            print('--> Total number of runs read:')
            print("-->  {} / {}".format(self.runsread, ndirec))

    def normalise(self):
        aper_loss = sum([self.compressed_losses[uspos][0] for uspos in self.compressed_losses])
        coll_loss = sum([self.collimator_losses[cid][2] for cid in self.compressed_losses])

        total_loss = aper_loss + coll_loss

        outfile = "{}_total_loss.txt".format(_os.path.basename(self.study_dir))
        outpath = _os.path.join(self.study_dir, outfile)
        with open(outpath, "w") as f:
            f.write("Total loss {} GeV".format(total_loss))

        for uspos in self.compressed_losses:
            loss = self.compressed_losses[uspos]
            loss[0] = loss[0] / float(total_loss)
            self.compressed_losses[uspos] = loss

        for cid in self.collimator_losses:
            closs = self.collimator_losses[cid]
            closs[2] = closs[2] / float(total_loss)
            self.compressed_losses[cid] = closs

    def __add__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError("Cannot add {} and {}".format(self.__class__, type(other)))

        result = _copy.deepcopy(self)

        # Merge collimator losses
        for cid in other.collimator_losses:
            A = other.collimator_losses[cid][1]
            E = other.collimator_losses[cid][2]
            if cid not in list(result.collimator_losses.keys()):
                result.collimator_losses[cid] = [cid, 0, 0]

            result.collimator_losses[cid][1] += A
            result.collimator_losses[cid][2] += E

        # Merging the correction
        for cid in other.collimator_leaking:
            A = other.collimator_leaking[cid][1]
            E = other.collimator_leaking[cid][2]
            if cid not in list(result.collimator_leaking.keys()):
                result.collimator_leaking[cid] = [cid, 0, 0]

            result.collimator_leaking[cid][1] += A
            result.collimator_leaking[cid][2] += E

        # Merging the compressed aperute losses
        for loss_s in other.compressed_losses:
            if loss_s not in list(result.compressed_losses.keys()):
                result.compressed_losses[loss_s] = other.compressed_losses[loss_s]
            else:
                result.compressed_losses[loss_s] += other.compressed_losses[loss_s]

        result.fli += other.fli  # This takes care of offsetting the ids internally
        # to avoid clashes

        # Need to take care of offseting the ids in the aperture impacts
        # to mach the merged fluka isotope data
        offset = (self.fli.files_loaded + 1) * self.fli.increment

        # Merging the impacts
        for impact in other.aperture_impact:
            impact[8] = impact[8] + offset  # offset the unique particle id
            result.aperture_impact.append(impact)

        # Obtain the rest of the state from the added machine

        if not self.machine:
            result.machine = other.machine
        if not self.machineparams:
            result.machineparams = other.machineparams
        if not self.kwargs:
            result.kwargs = other.kwargs

        # Obtain the rest of the state from the added machine
        result.impact_distribution = other.impact_distribution  # extract the impact distribution
        result.fluka_isotopes = other.fluka_isotopes  # merge the fluka isotope logs

        return result

    def _get_machine_info(self):
        # Try to calculate the S coordinate between SixTrack and MADX
        # By looking for a TFS file and a fort.2 file

        # Try to load machine parameters like length, startS etc.

        fort2file = None
        tfsfile = None

        if ( "fort2file" in list(self.kwargs.keys()) and self.kwargs["fort2file"] is not None ): # Check if it is supplied by the user
                fort2file = self.kwargs["fort2file"]
        else: # Check the common places to find a fort.2 file
            if _os.path.isfile(_os.path.join(self.study_dir, "clean_input/fort.2")):
                fort2file = _os.path.join(self.study_dir, "clean_input/fort.2")
            elif _os.path.isfile(_os.path.join(self.study_dir, "input/fort.2")):
                fort2file = _os.path.join(self.study_dir, "input/fort.2")
            elif _os.path.isfile(_os.path.join(self.study_dir, "fort.2")):
                fort2file = _os.path.join(self.study_dir, "fort.2")

        if ( "tfsfile" in list(self.kwargs.keys()) and self.kwargs["tfsfile"] is not None ):
            tfsfile = self.kwargs["tfsfile"]
        else:  # Check the common places to find a TFS file
            if _glob.glob(_os.path.join(self.study_dir, 'input/*tfs')):
                tfsfile = _glob.glob(_os.path.join(self.study_dir, 'input/*tfs'))[0]

        if self.verbose:
            if fort2file:
                msg = '--> Found fort.2 file: {}'.format(fort2file)
            else:
                msg = ("-> Cannot find fort.2 file, calculation of"
                       "S offset between SixTrack and MADX skipped!")
            print(msg)

            if tfsfile:
                msg = '--> Found TFS file: {}'.format(tfsfile)
            else:
                msg = ('--> Cannot find TFS file, calculation of S'
                       'offset between SixTrack and MADX skipped!')
            print(msg)

        if fort2file and tfsfile:
            if self.verbose:
                msg = ('--> Performing calculation of S offset between SixTrack and MADX,'
                       'results will be  written in {}')
                print(msg.format(_os.path.join(self.study_dir, "output/machine_info.txt")))

            self.machineparams.load_from_tfs_fort2(tfsfile, fort2file, machine=self.machine)

    @_safeload
    def load_pickle(self, filepath):
        """
        Loads a python binary pickle file with a serialised instance of the class.
        The loaded data is added to the data in the current instance allowing for accumulation.

        Arguments:
          filepath (str) - path to the pickle file
        """
        with open(filepath, "rb") as filein:
            data_in = _cPickle.load(filein)

        # Word around as using self = or self +=
        # doesn't seem to work
        total = self + data_in

        for key in total.__dict__:
            setattr(self, key, total.__dict__[key])

    @_safeload
    def loadfile_aperture_loss(self, filepath):
        with _read_file(filepath) as filein:
            for (j, line) in enumerate(filein):
                if j == -1:
                    break
                if j == 0:  # Header line
                    continue

                sline = line.split()

                (s, E) = (float(sline[4]), float(sline[12]))
                (A, Z) = (int(sline[15]), int(sline[16]))

                # if A == 1: #Get only ions
                #    continue

                self.aperture_losses.append([s, E, A, Z])

                if self.impact_distribution:
                    (x, xp) = (float(sline[8]), float(sline[9]))
                    (y, yp) = (float(sline[10]), float(sline[11]))
                    # The FlukaIsotpes instance (self.fli) keeps tack of runs loaded
                    # and is able to make the particle ids unique between runs.
                    # If the isotopes flag is not enabled or there is only one run
                    # loaded then the IDs are left as they are.
                    pid = self.fli.make_id_unique(int(sline[5]))

                    self.aperture_impact.append([s, E, A, Z, x, xp, y, yp, pid])

    @_safeload
    def loadfile_collimator_loss(self, filepath):
        with _read_file(filepath) as filein:
            for (j, line) in enumerate(filein):
                if j == -1:
                    break

                (cid, A, energy) = line.split()
                cid = int(cid)
                A = int(A)
                energy = float(energy)

                if cid not in list(self.collimator_losses.keys()):
                    self.collimator_losses[cid] = [cid, 0, 0.]

                self.collimator_losses[cid][1] += A
                self.collimator_losses[cid][2] += energy

    @_safeload
    def loadfile_correction(self, filepath):
        with _read_file(filepath) as filein:
            for (j, line) in enumerate(filein):
                if j == -1:
                    break
                if j == 0:
                    continue
                (pid, A, Z, cid, energy) = line.split()
                # The FlukaIsotpes instance (self.fli) keeps tack of runs loaded
                # and is able to make the particle ids unique between runs.
                # pid = self.fli.make_id_unique(pid)
                pid = int(pid)
                cid = int(cid)
                A = int(A)
                energy = float(energy)

                if cid not in list(self.collimator_leaking.keys()):  # generate entry if not existing
                    self.collimator_leaking[cid] = [cid, 0, 0, 0, 0, 0]

                if pid == 1:  # For saving the energy corresponding to protons
                    self.collimator_leaking[cid][3] += energy
                    self.collimator_leaking[cid][4] += 0
                    self.collimator_leaking[cid][5] += 0

                elif pid == 8:  # For saving the energy corresponding to neutrons
                    self.collimator_leaking[cid][3] += 0
                    self.collimator_leaking[cid][4] += energy
                    self.collimator_leaking[cid][5] += 0

                else:  # Energy from all particles not mentioned above
                    self.collimator_leaking[cid][3] += 0
                    self.collimator_leaking[cid][4] += 0
                    self.collimator_leaking[cid][5] += energy

                self.collimator_leaking[cid][1] += A  # add A/energy to dictionary
                self.collimator_leaking[cid][2] += energy

    @_safeload
    def prepare_output_paths(self, outdir="default"):
        if outdir == "default":
            patho = _os.path.join(self.study_dir, "output")
        else:
            patho = _os.path.abspath(outdir)
        _ensure_dir(patho)  # Make the directory if it doesn't exist

        # correction of collimator losses
        self.correctionfile = _os.path.join(patho, 'fort.66')
        # collimator losses
        self.collimatorfile = _os.path.join(patho, 'fort.208')
        # aperture losses
        self.aperturlosfile = _os.path.join(patho, 'fort.999')
        # compressed aperture losses
        self.aperturcomfile = _os.path.join(patho, 'fort.999.comp')
        # aperture losses with impact coordinates
        self.aperimpactfile = _os.path.join(patho, 'fort.impacts')
        # fluka isotope log with unique ids
        self.fluisotopefile = _os.path.join(patho, 'fluka_isotope_unique.log')
        # file with information about the machine
        self.machineinfofile = _os.path.join(patho, 'machine_info.txt')

        # REMOVE OLD OUTPUT
        for fname in [self.correctionfile, self.collimatorfile, self.aperturlosfile,
                      self.aperimpactfile, self.aperturcomfile, self.fluisotopefile,
                      self.machineinfofile]:
            try:
                _os.remove(fname)
            except OSError:
                pass

    def save_pickle(self, path="default"):
        """
        Serialise the current instance as a python binary pickle file.
        The instance can be reconstructed as is from the file using the
        load pickle method.

        This serialisation and the provided addition method makes it
        possible to efficiently combine many processed instances. For example
        every job in a batch submission can process its data and pickle it and
        all the pickled data can be merged later, which greatly speeds up
        the overall processing.

        Arguments:
          path (str) - path to save the picke file under. Default is "default"
                       which saves the file in <study_dir>/compressed_data.pkl
        """
        if path == "default":
            outpath = _os.path.join(self.study_dir, "compressed_data.pkl")
        else:
            outpath = _os.path.abspath(path)

        with open(outpath, "wb") as outfile:
            _cPickle.dump(self, outfile, 1)

    def write(self, outdir="default"):
        self.prepare_output_paths(outdir=outdir)

        # WRITE DATA TO OUTPUT FILES
        if self.machineparams:
            self.machineparams.write_machine_info(self.machineinfofile)

        if self.fluka_isotopes:
            self.fli.write(self.fluisotopefile)

        with open(self.correctionfile, 'w+') as f:
            for k in list(self.collimator_leaking.keys()):
                f.write(
                        '%2i %14i %14.8e %14.8e %14.8e %14.8e\n' % tuple(
                                self.collimator_leaking[k]))

        with open(self.collimatorfile, 'w+') as f:
            for k in list(self.collimator_losses.keys()):
                f.write('%2i %14i %14.8e \n' % tuple(self.collimator_losses[k]))

        with open(self.aperturlosfile, 'w+') as f:
            for l in self.aperture_losses:
                f.write('%7.1f %9.1f %5i %5i\n' % tuple(l))

        with open(self.aperturcomfile, 'w+') as f:
            f.write('# %3s %16s %9s %7s\n' % ('s [m]', 'E [GeV]', 'A', '(Z)'))
            for l in sorted(self.compressed_losses.keys()):
                loss = self.compressed_losses[l]
                f.write('%7.1f %16.1f %9i %9i\n' % (l, loss[0], loss[1], loss[2]))

        if self.impact_distribution:
            with open(self.aperimpactfile, 'w+') as f:
                for l in self.aperture_impact:
                    f.write('%7.1f %9.1f %5i %5i %14.9f %14.9f %14.9f %14.9f %12i \n'% tuple(l))

def main():
    usage = ('Processes the output from a sixtrack-fluka simulation'
             ' into a compressed format for analysis')
    parser = _optparse.OptionParser(usage)
    parser.add_option('--dir', action='store', dest="studydir", type="string",
                      default="", help="Path to the directory with run_xxxxx files")

    parser.add_option('--imax', action='store', dest="imax", type="int",
                      default=-1, help="Maximum number of runs to read")

    parser.add_option('--twissFile', action='store', dest="twissfile", type="string",
                      default=None, help="TFS file of the thin lattice with appropriate"
                      "columns for finding starting position of lattice in fort.2. (Optional)")

    parser.add_option('--machine', action='store', dest="machine", type="string",
                      default=None, help="Type of machine, default is to guess.")

    parser.add_option('-i', '--impacts', action='store_true',
                      default=False, help="Include detailed information about aperture losses")

    parser.add_option('-p', '--isotopes', action='store_true', default=False,
                      help="Inclde merging of isotopes and unueiq id processing for part ids")

    parser.add_option('-q', '--quiet', action='store_true',
                      default=False, help="Silence all printouts")

    options, args = parser.parse_args()

    #Sanity checks on input
    if not options.studydir:
        parser.print_help()
        parser.error("Please provide the path to study directory via the --study_dir argument")

    if args:
        print("ERROR when parsing, leftover arguments", args)
        parser.print_help()
        raise SystemExit

    verbose = not options.quiet

    start_time = _datetime.now()
    compression = HisixOutputCompression(options.studydir, options.imax, options.machine,
                                         options.impacts, options.isotopes, verbose, tfsfile=options.twissfile )
    compression.write()
    print("Time taken: {}".format(_datetime.now() - start_time))

if __name__ == "__main__":
    main()
