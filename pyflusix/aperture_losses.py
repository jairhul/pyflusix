import os as _os

import pyflusix.machine_parameters as _mcp


class ApertureLosses(object):
    """
    Class to handle the loading and the processing of aperture impacts.
    """
    def __init__(self, filename, s_min=None, s_max=None, s_start=None, s_total=None, verbose=True):
        self.filename = filename
        self.s_min = s_min
        self.s_max = s_max
        self.verbose = verbose

        self.impacts = []
        self.machine = _mcp.MachineParameters()

        _info_file = _os.path.join(_os.path.dirname(filename), "machine_info.txt")
        if verbose:
            print(("Loading aperture impacts from file {}".format(filename)))

        # Load an instance of the machine parameters class to handle S transforms
        # If none of the cases below  works then no shift is perfomed
        if s_start is not None and s_total is not None:  # Both values are set
            self.machine.user_defined_S(s_start, s_total)
            if verbose:
                msg = "Applying user-defined S shift: startS = {} m , totalS = {} m"
                print((msg.format(self.machine.s_start, self.machine.length)))

        elif s_start is not None or s_total is not None:  # Only one value is set
            msg = "Cannot have startS set, but not totalS or vice-verse. Please set both of none."
            raise ValueError(msg)

        elif _os.path.isfile(_info_file):  # No value is set
            self.machine.load_from_infofile(_info_file)
            if verbose:
                print(("Found machine info file: {}".format(_info_file)))
                msg = "Applying loaded S shift: startS = {} m , totalS = {} m"
                print((msg.format(self.machine.s_start, self.machine.length)))

        self.load()

        if self.verbose:
            print(("Impacts: ", len(self.impacts)))
            print(("Total E lost: {:.3E} GeV".format(sum([val[1] for val in self.impacts]))))

    def load(self):
        if self.s_min is None:
            self.s_min = 0
        if self.s_max is None:
            self.s_max = 1.e9
        if self.s_min < 0 or self.s_max < 0:
            msg = "The values for minum and maximum S of the selected region cannot be negative."
            raise ValueError(msg)
        if self.s_min > self.s_max:
            msg = "The maximum S of the selected region must be larger than the minumum S."
            raise ValueError(msg)

        with open(self.filename) as filein:
            line = filein.readline()  # The first line is a comment, skip
            line = filein.readline()

            if self.verbose:
                if self.s_min > 0 and self.s_max < 1.e9:
                    print(("Selecting region from S = {} m  to S = {}".format(self.s_min, self.s_max)))

            # Do the loading
            while line:
                sline = line.split()
                if self.s_min < self.machine.shift_S(sline[0]) < self.s_max:
                    impact = [float(val) for val in sline]
                    impact[0] = self.machine.shift_S(impact[0])
                    self.impacts.append(impact)

                line = filein.readline()
