import numpy as _np
import matplotlib.pyplot as _plt
from matplotlib.patches import Ellipse as _Ellipse

from matplotlib.ticker import MaxNLocator as  _MaxNLocator
from mpl_toolkits.axes_grid1 import make_axes_locatable as _make_axes_locatable
from matplotlib.colors import LogNorm as _LogNorm


def _eigsorted(cov):
    vals, vecs = _np.linalg.eigh(cov)
    order = vals.argsort()[::-1]
    return vals[order], vecs[:, order]


def _cov_mat_from_twiss(alpha, beta, emittance):
    gamma = (1+alpha**2)/beta
    c11 = beta*emittance
    c22 = gamma*emittance
    c12 = -alpha*emittance
    c21 = -alpha*emittance

    return _np.array([[c11, c12], [c21, c22]])


def draw_ellipse_from_twiss(alpha, beta, emittance, nsigma=1, centre_x=0, centre_y=0,
                            unitscale=1, color='b', label='', handles=[], ax=None):
    """
    Draws a phase space ellipse for a transverse coordinate plane from Twiss parameters
    with the radius specified in number of sigma. This is useful for visualsing beam core
    and halo ellipses anywhere in the machine.

    N.B. matplotlib.patches are used to draw the ellipse and those are not atomatically added
    to the legend. For this reason the function appends the drawn ellipse to and returns a
    list of legend handles that can be provided as an input or instantilised
    internally if no input is specified. The list of handles can be given directly to
    matplotlib.pyplot.legend() by specifying handles = myhandes as an argument.

    Inputs:
      alpha - Twiss alpha function   []
      beta  - Twiss beta function    [m]
      emittance - Physical emittance [m]
      nsigma    - Radius of the ellispe in terms of sigma [] (Optional)
      untiscale - Used to rescale the coodrinates before plotting (e.g m,rad -> mm, mrad) (Optional)
      color     - A valid matplotlib.pyplot color specifier (Optional)
      label     - Label to display in legend (Optional)
      handles   - An externally produced list of legend handles to append the
                  ellipse entry to (Optional)
      ax        - A matplotlib.axes._subplots.AxesSubplot instance to pace the plot on,
                  default the current axes (Optional)

    Returns:
      handles - List of legend handles containing holding the ellipse entry
    """
    cov = _cov_mat_from_twiss(alpha, beta, emittance)
    vals, vecs = _eigsorted(cov)
    theta = _np.degrees(_np.arctan2(*vecs[:, 0][::-1]))
    w, h = 2 * nsigma * _np.sqrt(vals)
    ell = _Ellipse(xy=(centre_x, centre_y),
                   width=w*unitscale, height=h*unitscale,
                   angle=theta, color=color, label=label)
    ell.set_facecolor('none')
    if ax is None:
        ax = _plt.gca()
    ax.add_artist(ell)
    handles.append(ell)

    return handles


def draw_histo_A(A, E=None, fig=None, **kwargs):
    """
    Draws a 1D histogram of atomic number A.
    If an energy array E is provided then weigth the entries by this.

    Inputs:
      A (array-like) - Array of A values
      E (array-like) - Array of E values for weighting (Optional)
      fig (matplotlib.figure.Figure) - Figure to draw on (Optional)

    Kwargs:
      fontize - size for the plot labels in points.
      outfile - the name of a file where to save th figure. Default is no save

    Returns:
      matplotlib.figure.Figure with the plot on it.
    """
    fontsizein = _plt.rcParams["font.size"]
    outfile = None
    if "fontsize" in kwargs:
        fontsizein = kwargs["fontsize"]
    if "outfile" in kwargs:
        outfile = kwargs["outfile"]
    if fig is None:
        f = _plt.figure()
    else:
        f = fig

    A = _np.asarray(A)
    if E is not None:
        weights = _np.asarray(E)/sum(E)  # Turn into a fraction
        vals, _, _ = _plt.hist(A,
                               bins=_np.linspace(0.5, int(max(A))+0.5, int(max(A)+1)),
                               weights=weights, log=True)

    else:
        vals, _, _ = _plt.hist(A,
                               bins=_np.linspace(0.5, int(max(A))+0.5, int(max(A)+1)),
                               log=True)

    ax = _plt.gca()
    ax.plot([], [], linestyle="None", label="Total Particles {}".format(len(A)))
    if E is not None:
        ax.plot([], [], linestyle="None", label="Total E {:.3E} GeV".format(sum(E)))
    ax.set_xlim(0, int(max(A)+1))
    ax.set_xlabel("A", fontsize=fontsizein)
    if E is not None:
        ax.set_ylabel("Energy Fraction", fontsize=fontsizein)
    else:
        ax.set_ylabel("Count", fontsize=fontsizein)
    f.subplots_adjust(right=0.85, wspace=0.4, hspace=0.4)
    leg = _plt.legend(loc=0, frameon=True, edgecolor="k", handlelength=0.05,
                      fontsize="medium", framealpha=1)

    if outfile:
        if not outfile.endswith(".pdf") and not outfile.endswith(".png"):
            outfile = "{}.pdf".format(outfile)
        _plt.savefig(outfile, bbox_inches="tight")

    return f


def draw_2d_histo_AZ(A, Z, E=None, fig=None, **kwargs):
    """
    Draws a 2D histogram of atomic number A and charge number Z.
    If an energy array E is provided then weigth the entries by this.

    Inputs:
      A (array-like) - Array of A values
      Z (array-like) - Array of Z values
      E (array-like) - Array of E values for weighting (Optional)
      fig (matplotlib.figure.Figure) - Figure to draw on (Optional)

    Kwargs:
      fontize - size for the plot labels in points.
      outfile - the name of a file where to save th figure. Default is no save

    Returns:
      matplotlib.figure.Figure with the plot on it.
    """

    fontsizein = _plt.rcParams["font.size"]
    outfile = None
    if "fontsize" in kwargs:
        fontsizein = kwargs["fontsize"]
    if "outfile" in kwargs:
        outfile = kwargs["outfile"]
    if fig is None:
        f = _plt.figure()
    else:
        f = fig

    A = _np.asarray(A)
    Z = _np.asarray(Z)
    if E is not None:
        weights = _np.asarray(E)/sum(E)
        vals = _plt.hist2d(A, Z, cmin=_np.log(1./max(E)), norm=_LogNorm(), weights=weights,
                           bins=[_np.linspace(0.5, int(max(A))+0.5, int(max(A)+1)),
                                 _np.linspace(0.5, int(max(Z))+0.5, int(max(Z)+1))])

    else:
        vals = _plt.hist2d(A, Z, cmin=1, norm=_LogNorm(),
                           bins=[_np.linspace(0.5, int(max(A))+0.5, int(max(A)+1)),
                                 _np.linspace(0.5, int(max(Z))+0.5, int(max(Z)+1))])

    ax = _plt.gca()
    ax.set_xlim(0, int(max(A)+1))
    ax.set_xlabel("A")  # , fontsize=fontsizein)
    ax.set_ylabel("Z")  # , fontsize=fontsizein)
    ax.set_aspect("equal")
    ax.yaxis.set_major_locator(_MaxNLocator(integer=True))
    ax.xaxis.set_major_locator(_MaxNLocator(integer=True))

    f.subplots_adjust(right=0.85, wspace=0.4, hspace=0.4)
    divider = _make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = f.colorbar(vals[3], cax=cax)
    # cbar.ax.set_yticklabels(cbar.ax.get_yticklabels(), fontsize=fontsizein)
    if E is not None:
        cbar.set_label("Energy Fraction")  # , fontsize=fontsizein)
    else:
        cbar.set_label("Count")  # , fontsize=fontsizein)
    ax.legend()

    if outfile:
        if not outfile.endswith(".pdf") and not outfile.endswith(".png"):
            outfile = "{}.pdf".format(outfile)
        _plt.savefig(outfile, bbox_inches="tight")

    return f


def plot_phase_space(x, y, xp, yp, dE, z, color=None, scatter=False, nbins=100, dunit="mm",
                     aunit="mrad", eunit="GeV", col_label=None, outputfilename=None,
                     showplot=True, **kwargs):
    """
    Plots phase space coordinates as 4 subplots - (x, y), (x, xp), (y, yp), (dE, Z).
    The plots can be either colormaps or scatter plots.
    """
    step = 1
    if "step" in kwargs:
        step = kwargs["step"]

    fontsizein = _plt.rcParams["font.size"]
    if "fontsize" in kwargs:
        fontsizein = kwargs["fontsize"]

    if not color:
        col = "b"
    else:
        if isinstance(color, str):
            col = color
        else:
            col = color[::step]

    f, axarr = _plt.subplots(2, 2, figsize=(12, 7))
    if scatter:
        axarr[1, 0].scatter(x[::step], y[::step], s=0.5, c=col)
    else:
        axarr[1, 0].hist2d(x[::step], y[::step], bins=nbins, cmin=1)
    axarr[1, 0].set_xlabel("x [{}]".format(dunit), fontsize=fontsizein)
    axarr[1, 0].set_ylabel("y [{}]".format(dunit), fontsize=fontsizein)
    axarr[1, 0].set_xlim(1.05*min(x[::step]), 1.05*max(x[::step]))
    axarr[1, 0].set_ylim(1.05*min(y[::step]), 1.05*max(y[::step]))

    if scatter:
        axarr[0, 0].scatter(x[::step], xp[::step], s=0.5, c=col)
    else:
        axarr[0, 0].hist2d(x[::step], xp[::step], bins=nbins, cmin=1)
    axarr[0, 0].set_xlabel("x [{}]".format(dunit), fontsize=fontsizein)
    axarr[0, 0].set_ylabel("xp [{}]".format(aunit), fontsize=fontsizein)
    axarr[0, 0].set_xlim(1.05*min(x[::step]), 1.05*max(x[::step]))
    axarr[0, 0].set_ylim(1.05*min(xp[::step]), 1.05*max(xp[::step]))

    if scatter:
        axarr[1, 1].scatter(y[::step], yp[::step], s=0.5, c=col)
    else:
        axarr[1, 1].hist2d(y[::step], yp[::step], bins=nbins, cmin=1)
    axarr[1, 1].set_xlabel("y [{}]".format(dunit), fontsize=fontsizein)
    axarr[1, 1].set_ylabel("yp [{}]".format(aunit), fontsize=fontsizein)
    axarr[1, 1].set_xlim(1.05*min(y[::step]), 1.05*max(y[::step]))
    axarr[1, 1].set_ylim(1.05*min(yp[::step]), 1.05*max(yp[::step]))

    if scatter:
        pt = axarr[0, 1].scatter(dE[::step], z[::step], s=0.5, c=col)
    else:
        pt = axarr[0, 1].hist2d(dE[::step], z[::step], bins=nbins, cmin=1)
    axarr[0, 1].set_xlabel("dE [{}]".format(eunit), fontsize=fontsizein)
    axarr[0, 1].set_ylabel("z [{}]".format("mm"), fontsize=fontsizein)
    axarr[0, 1].xaxis.get_major_formatter().set_powerlimits((0, 1))
    # axarr[0, 1].set_xlim(1.05*min(dE[::step]), 1.05*max(dE[::step]))
    # axarr[0, 1].set_ylim(1.05*min(z[::step]), 1.05*max(z[::step]))

    f.subplots_adjust(right=0.85, wspace=0.4, hspace=0.4)

    if not scatter:
        cbar_ax = f.add_axes([0.9, 0.1, 0.02, 0.78])
        cbar = f.colorbar(pt[3], cax=cbar_ax)
        cbar.set_label("Count", fontsize=fontsizein)
    else:
        _plt.tight_layout()

    if outputfilename:
        if not outputfilename.endswith(".pdf") and not outputfilename.endswith(".png"):
            outputfilename = "{}.pdf".format(outputfilename.replace(".dat", "").replace(".txt", ""))
        _plt.savefig(outputfilename, bbox_inches="tight")

    if showplot:
        _plt.show()

    return f
