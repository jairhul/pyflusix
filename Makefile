
install:
	pip install --editable . --user

uninstall:
	pip uninstall pyflusix

# Always install as developer for now
#install:
#	pip install . --user

#develop:
#	pip install --editable . --user

# Not uploaded on PyPI yet
# bumpversion is a python utility available via pip.  Make sure to add
# your pip user install location's bin directory to your PATH.
#bump-major:
#	bumpversion major setup.py setup.cfg

#bump-minor:
#	bumpversion minor setup.py setup.cfg

#bump-patch:
#	bumpversion patch setup.py setup.cfg

#pypi-upload:
#	python setup.py sdist bdist_wheel; \
#	twine upload --repository pypi dist/*
