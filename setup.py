from setuptools import setup, find_packages

setup(
    name="pyflusix",
    version="0.2.0",
    packages=find_packages(exclude=["docs", "tests", "bin"]),

    install_requires=["matplotlib",
                      "numpy",
                      "pandas",
                      "pymadx"],
    python_requires=">=2.7.*",

    author="LHC Collimation Project",
    author_email="andrey.abramov@cern.ch",
    description='Input preparation and analysis tools for SixTrack-FLUKA active coupling',
    #license='GPL3',
    url='https://andreyabramov.bitbucket.io/',
    keywords='sixtrck fluka accelerators',
)
