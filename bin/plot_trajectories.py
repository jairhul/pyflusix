#!/usr/bin/env python

import optparse as _optparse
import numpy as _np
import matplotlib.pyplot as _plt
import matplotlib as _mpl

import pyflusix.sixtrack_aperture as _sxtap


def load_dump_file(filename):
    """
    Load a dump file. Here expect only single / small files so
    no need to leverage the SixTrackDump class
    """
    data = _np.loadtxt(filename, comments="#", unpack=False)

    return data


def get_one_particle(data, partID):
    one_particle_data = [entry for entry in data if entry[0] == partID]

    # return one_particle_data
    return zip(*one_particle_data)


def plot_trajectory(datain, plot=0, label=None):
    s = datain[2]
    x = datain[3]
    xp = datain[4]
    y = datain[5]
    yp = datain[6]
    dE = datain[7]

    ms = _mpl.rcParams['lines.markersize']**2 / 5
    # make a different color for every line
    # define the colormap
    cmap = _plt.cm.Blues
    # extract all colors from the .jet map
    cmaplist = [cmap(i) for i in range(cmap.N)]

    # define the bins and normalize
    bounds = _np.linspace(100, len(s), endpoint=True)
    norm = _mpl.colors.BoundaryNorm(bounds, cmap.N)

    if plot == 0:
        # _plt.scatter(x, xp, label=label, c=s, cmap=cmap, s=ms)
        _plt.plot(s, x, label=label)
        _plt.xlabel("S (m)")
        _plt.ylabel("x (mm)")
        _plt.savefig("trajectoryX.pdf")

    elif plot == 1:
        _plt.plot(s, y, label=label)
        _plt.xlabel("S (m)")
        _plt.ylabel("y (mm)")
        _plt.savefig("trajectoryY.pdf")

    elif plot == 3:
        _plt.plot(s, dE, label=label)
        _plt.xlabel("S (m)")
        _plt.ylabel("dE/E (mm)")
        _plt.savefig("trajectorydE.pdf")


def main():
    usage = ''
    parser = _optparse.OptionParser(usage)
    parser.add_option('-f', '--filename', action='store', dest="filename", type="string",
                      default="", help="Name of dump file containing trajectories.")

    parser.add_option('-p', '--plane', action='store', dest="plane", type="string",
                      default="horizontal", help="Plane to make the plots in: horizontal "
                      "or vertical")

    parser.add_option('-s', '--sstart', action='store', dest="sstart", type="float",
                      default=0.0, help="Starting S position of region of interest.")

    parser.add_option('-e', '--send', action='store', dest="send", type="float",
                      default=0.0, help="Final S position of region of interest.")

    parser.add_option('-b', '--ymin', action='store', dest="miny", type="float",
                      default=0.0, help="Minimum y-axis extent of plot.")

    parser.add_option('-t', '--ymax', action='store', dest="maxy", type="float",
                      default=0.0, help="Maximum y-axis extent of plot.")

    parser.add_option('-i', '--imax', action='store', dest="imax", type="int",
                      default=-1, help="Maximum number of trajectories to plot from file.")

    parser.add_option('-a', '--aperfilename', action='store', dest="aperfilename", type="string",
                      default="", help="Name of aperture dump from SixTrack (Optional).")

    parser.add_option('-o', '--output', action='store', dest="outfile", type="string",
                      default="", help="Filepath to save the plot under.")

    parser.add_option('-v', '--verbose', action='store_true',
                      default=False, help="Print detailed information")

    options, args = parser.parse_args()

    if not options.filename:
        print "No filename specified. Stop."
        parser.print_help()
        return

    if args:
        print "ERROR when parsing, leftover arguments", args
        parser.print_help()
        raise SystemExit

    if options.outfile:
        outfile = options.outfile.replace(".pdf", "").replace(".png", "")
    else:
        outfile = options.filename.replace(".dat", "").replace(".gz", "").replace(".txt", "")

    filename = options.filename
    aperfile = options.aperfilename

    if options.plane == "horizontal":
        plot = 0
    elif options.plane == "vertical":
        plot = 1
    else:
        msg = "Unrecognised plane {}, please use 'horizontal' or 'vertical'."
        raise IOError(msg.format(options.plane))

    srange = (options.sstart, options.send)
    crange = (options.miny, options.maxy)

    fig = _plt.figure(1, facecolor="w", figsize=(12, 6))

    all_particles = load_dump_file(filename)

    trajectory_count = len(all_particles)
    if options.imax == -1 or options.imax > trajectory_count:
        stop = trajectory_count
    else:
        stop = options.imax

    for i in range(1, stop):
        one_particle = get_one_particle(all_particles, i)
        plot_trajectory(one_particle, label=None, plot=plot)

    # Plot the aperture as well
    if aperfile:
        aperture = _sxtap.SixTrackAperture(aperfile)
        fig = aperture.plot_aperture_profile(plane=options.plane, figure=fig)

    ax = fig.gca()
    ax.set_xlabel("S [m]")
    if plot == 0:
        ax.set_ylabel("X [mm]")
    elif plot == 1:
        ax.set_ylabel("Y [mm]")

    if srange[0] and srange[1]:
        if crange[0] and crange[1]:
            ax.set_ylim(crange)
        ax.set_xlim(srange)
        addstring = "-zoom"
    else:
        addstring = ""

    _plt.legend(fontsize="x-small")

    if plot == 0:
        _plt.savefig("{}-x{}.pdf".format(outfile, addstring), bbox_inches="tight")
    elif plot == 1:
        _plt.savefig("{}-y{}.pdf".format(outfile, addstring), bbox_inches="tight")

    _plt.show()


if __name__ == "__main__":
    main()
