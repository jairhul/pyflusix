#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import optparse as _optparse
from datetime import datetime as _datetime
from pyflusix.output_compression import HisixOutputCompression


def main():
    usage = ('Processes the output from a sixtrack-fluka simulation'
             ' into a compressed format for analysis')
    parser = _optparse.OptionParser(usage)
    parser.add_option('--dir', action='store', dest="studydir", type="string",
                      default="", help="Path to the directory with run_xxxxx files")

    parser.add_option('--imax', action='store', dest="imax", type="int",
                      default=-1, help="Maximum number of runs to read")

    parser.add_option('--machine', action='store', dest="machine", type="string",
                      default=None, help="Type of machine, default is to guess.")

    parser.add_option('-i', '--impacts', action='store_true',
                      default=False, help="Include detailed information about aperture losses")

    parser.add_option('-p', '--isotopes', action='store_true', default=False,
                      help="Inclde merging of isotopes and unique id processing for part ids")

    parser.add_option('-q', '--quiet', action='store_true',
                      default=False, help="Silence all printouts")

    options, args = parser.parse_args()

    # Sanity checks on input
    if not options.studydir:
        parser.print_help()
        parser.error("Please provide the path to study directory via the --study_dir argument")

    if args:
        print("ERROR when parsing, leftover arguments", args)
        parser.print_help()
        raise SystemExit

    verbose = not options.quiet

    start_time = _datetime.now()
    compression = HisixOutputCompression(options.studydir, options.imax, options.machine,
                                         options.impacts, options.isotopes, verbose)
    compression.write()
    print("Time taken: {}".format(_datetime.now() - start_time))


if __name__ == "__main__":
    main()
