#!/usr/bin/env python

from __future__ import print_function

import os as _os
import optparse as _optparse
import datetime as _datetime
import warnings as _warnings
import numpy as _np

from string import Template as _tpl

PROTON_MASS = 0.93827231

def load_beam_def_fort3(filepath, verbose=True):
    abs_filepath = _os.path.abspath(filepath)

    # Somewhat complex conditional structure, but all the data
    # can be found in a signle line-by-line read of the file
    beam_energy = None
    particle_info = None

    found_particledef = False
    found_init = False
    init_counter = 0

    with open(abs_filepath, "r+") as f:
        for line in f:
            if line.startswith("/"):  # Ignore commented out lines
                continue

            if "INIT" in line:
                found_init = True

            elif found_init and beam_energy is None:
                # The total energy is found 12 lines after INIT (apparently)
                if init_counter == 13:
                    beam_energy = line.strip().split()[0]
                else:
                    init_counter += 1

            elif "HION" in line:
                found_particledef = True

            # First elligible line after HION is the needed data
            elif particle_info is None and found_particledef:
                particle_info = line.strip().split()

    if beam_energy is None:
        raise SystemExit("Could not find the beam energy defintion in fort.3")

    if particle_info is None:
        particle_info = (1, 1, PROTON_MASS, 1, 0)  # If no explicit ion defintion use proton

    # Mass in MeV, Z, A
    # SixTrack versions >5 include an optional argument, the charge Q (default is Q=Z)
    Q = int(particle_info[3]) if len(particle_info) > 3 else int(particle_info[1])
    pdg_id = int(particle_info[4]) if len(particle_info) > 4 else 0
    particle_data = [int(particle_info[0]), int(particle_info[1]), float(particle_info[2]),
                     float(beam_energy) * 1.e-3, Q, pdg_id]

    # Newer SixTrack versions also have an optional 5th argument, the PDG id, which has no default and is ignored for now

    if verbose:
        if particle_data[2] != PROTON_MASS: # Check on the mass as electrons (A=0,Z=0) are ran through the ion interface
            print("--> From fort.3: ion beam, A= {} GeV, Z= {}, Mass= {:.17}, E= {} GeV, Q= {} e".format(*particle_data))
        else:
            print("--> From fort.3: proton beam, E= {} GeV".format(particle_data[3]))

    return particle_data


def check_parameter(tfs_dict, param):
    try:
        val = float(tfs_dict[param])
    except KeyError:
        msg = "{} not found in the TFS - setting to 0. Please check the output file"
        _warnings.warn(msg.format(param))
        val = 0.0
    return val


def load_optics_tfs(filepath, marker_name, verbose=True):
    abs_filepath = _os.path.abspath(filepath)

    header = None
    coll_optics = None
    with open(abs_filepath, "r+") as f:
        for line in f:
            if line.startswith("*"):
                header = line.strip().split()

            elif marker_name in line or marker_name.lower() in line or marker_name.upper() in line:
                coll_optics = line.strip().split()
                break #End the loop early - information found

    if coll_optics is None:
        msg = "Could not find info for marker {} in TFS optics"
        raise SystemExit(msg.format(marker_name))

    #pack the header and the data into a dictionary for easy access
    optics_dict = {}
    for i in range(len(coll_optics)):
        # Offset the header index by one to account for the leading *
        # Cast to floats later as someof the fields contain strings
        optics_dict[header[i+1]] = coll_optics[i]

    op = optics_dict #shortcut
    #s, betax, alphax, betay, alphay, , dx, dy, dpx, dpy, x, xp, y, yp
    reduced_coll_optics = [float(op["S"]), float(op["BETX"]), float(op["ALFX"]),
                           float(op["BETY"]), float(op["ALFY"]), float(op["DX"]),
                           float(op["DPX"]), float(op["DY"]), float(op["DPY"]),
                           check_parameter(op, "X"), check_parameter(op, "PX"),
                           check_parameter(op, "Y"), check_parameter(op, "PY")]
                           #In case the TFS is missing columns for the offset

    if verbose:
        print("--> Marker found in TFS: {}".format(marker_name))
        msg = "--> TFS optics at marker: S= {} m, BETX= {} m, ALFX= {}, BETY= {} m, ALFY= {}"
        print(msg.format(*reduced_coll_optics))

    return reduced_coll_optics


def initalise_options_dict():
    options_dict = {}
    #List of all the substitute keys in the loaded template
    keys = ["TEMPLATE_HEADER", "BEAM_DESCRIPTION", "BETX", "BETY", "ALFX", "ALFY",
            "DX", "DPX", "DY", "DPY", "ENX", "ENY", "MASS", "PC", "DELTAP", "BLEN",
            "IONBEAM", "A", "Z", "DISTX", "DISTY", "SIG_INNER_X", "SIG_OUTER_X", "SIG_INNER_Y",
            "SIG_OUTER_Y", "DISTL", "SIG_INNER_L", "SIG_OUTER_L", "PLANE",
            "GEOACC_LOWER", "GEOACC_UPPER", "XOFFS", "XPOFF", "YOFFS", "YPOFFS",
            "TDELAY", "PCOFFS", "SSHIFT"]

    # If a value is not explicity set, write out an EDITME tag to the output file as a placeholder
    for key in keys:
        options_dict[key] = "EDITME"

    return options_dict

def read_config_template(filepath=_os.path.join(_os.path.dirname(__file__),
                                                "../pyflusix/running/input_templates/"
                                                "config_gpdist_simple.tpl")):
    # The default is to look for the template in the input_templates directory in the
    # directory where the script is located
    abs_filepath = _os.path.abspath(filepath)

    with open(abs_filepath, "r+") as filein:
        template_contents = filein.read()

    template = _tpl(template_contents)

    return template


def write_config_file(outfilepath, options_dict):
    template = read_config_template()
    rendered_text = template.substitute(options_dict)

    abs_outfilepath = _os.path.abspath(outfilepath)
    with open(abs_outfilepath, "w") as of:
        of.write(rendered_text)


def main():

    usage = ("Automatically makes a simple gpdist configuration for a beam at a marker."
             " The resulting beam file must be checked and edited if needed.")
    parser = _optparse.OptionParser(usage)
    parser.add_option('--fort3', action='store', dest="f3file", type="string",
                      default="", help="Path to fort.3 file")

    parser.add_option('--tfsOptics', action='store', dest="tfsfile", type="string",
                      default="", help="Path to TFS file containing the opticw")

    parser.add_option('--marker', action='store', dest="markername",
                      type="string", default="", help="Name of marker where to make the beam")

    parser.add_option('--enorm', action='store', dest="enorm", type="float",
                      default=0.0, help="Normalised beam emittance [m]")

    parser.add_option('--blen', action='store', dest="blen", type="float",
                      default=0.0, help="Bunch length (4*sigma_l)")

    parser.add_option('--deltap', action='store', dest="dp", type="float",
                      default=0.0, help="Beam delta_p/p")

    parser.add_option('-q', '--quiet', action='store_true',
                      default=False, help="Silence detailed printouts")

    options, args = parser.parse_args()

    # Sanity checks on input
    if not options.f3file:
        parser.print_help()
        parser.error("Please provide a fort.3 file via the --fort.3 argument")

    elif not options.tfsfile:
        parser.print_help()
        parser.error("Please provide optical information file via the "
                     "--tfsOptics argument")

    elif not options.enorm:
        parser.print_help()
        parser.error("Please provide the normalised emittance via the --enorm argument")

        """
    elif not options.blen:
        parser.print_help()
        parser.error("Please provide the bunch length  via the --blen argument")

    elif not options.dp:
        parser.print_help()
        parser.error("Please provide the beam momentum spread  via the --deltap argument")
        """

    elif not options.markername:
        parser.error("Please provide the marker name via the --marker argument")

    if args:
        print("ERROR when parsing, leftover arguments", args)
        parser.print_help()
        raise SystemExit

    verbose = not options.quiet

    print("--> Preparing beam")

    if verbose:
        print("--> Loading data")

    beam_data = load_beam_def_fort3(options.f3file, verbose=verbose)

    #Optics fields: s, betax, alphax, betay, alphay, , dx, dy, dpx, dpy, x, xp, y, yp 
    marker_optics = load_optics_tfs(options.tfsfile, options.markername, verbose=verbose)

    #Populate options dict with available values and write out
    options_dict = initalise_options_dict()

    today = _datetime.datetime.today().strftime("%d-%m-%Y")
    thisfile = _os.path.basename(__file__)

    #Header and beam info
    header_message = (" Produced on {} by {}\n** Automatically generated input - "
                      "CHECK ALL PARAMETERS before running!")
    options_dict["TEMPLATE_HEADER"] = header_message.format(today, thisfile)

    msg = "Beam at marker {}"
    options_dict["BEAM_DESCRIPTION"] = msg.format(options.markername)

    #Optical parameters
    options_dict["BETX"] = marker_optics[1]
    options_dict["ALFX"] = marker_optics[2]
    options_dict["DX"] = marker_optics[5]
    options_dict["DPX"] = marker_optics[6]
    options_dict["BETY"] = marker_optics[3]
    options_dict["ALFY"] = marker_optics[4]
    options_dict["DY"] = marker_optics[7]
    options_dict["DPY"] = marker_optics[8]

    options_dict["ENX"] = options.enorm
    options_dict["ENY"] = options.enorm
    
    options_dict["A"] = beam_data[0]
    options_dict["Z"] = beam_data[1]
    options_dict["Q"] = str(beam_data[4])
    options_dict["MASS"] = repr(beam_data[2])
    options_dict["PDGID"] = str(beam_data[5])
    options_dict["PC"] = beam_data[3] #Assume ultrarelativistic and set PC=Etot
    options_dict["IONBEAM"] = "" if beam_data[2] != PROTON_MASS else "*" # Comment out ion defintion for protons
    options_dict["HASPDGID"] = "" if beam_data[5] != 0 else "*"

    options_dict["BLEN"] = options.blen
    options_dict["DELTAP"] = options.dp

    #Distribution type
    options_dict["DISTX"] = "GAUSS" # Very simple defaults, the user must edit
    options_dict["SIG_INNER_X"] = ""
    options_dict["SIG_OUTER_X"] = ""

    options_dict["DISTY"] = "GAUSS"
    options_dict["SIG_INNER_Y"] = ""
    options_dict["SIG_OUTER_Y"] = ""


    options_dict["DISTL"] = "PENCIL"
    options_dict["SIG_INNER_L"] = ""
    options_dict["SIG_OUTER_L"] = ""

    #Offset
    options_dict["XOFFS"] = marker_optics[9]
    options_dict["XPOFFS"] = marker_optics[10]
    options_dict["YOFFS"] = marker_optics[11]
    options_dict["YPOFFS"] = marker_optics[12]
    options_dict["TDELAY"] = 0.0 #Set explicitly to zero for now
    options_dict["PCOFFS"] = 0.0
    
    filepath = "CHECKME_config_gpdist.txt"
    write_config_file(filepath, options_dict)
    
    if verbose:
        print("--> Wrote file: {}".format(filepath))

    print("--> Done!")

if __name__ == "__main__":
    main()
