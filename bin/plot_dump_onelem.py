#!/usr/bin/env python

import optparse as _optparse
import matplotlib.pyplot as _plt

import pyflusix.data_loader as _cdl
import pyflusix.plotting_utilities as _plu


def main():
    usage = ''
    parser = _optparse.OptionParser(usage)
    parser.add_option('-f', '--filepath', action='store', dest="filename", type="string",
                      default="", help="Path to file with dump data to load.")

    parser.add_option('-o', '--output', action='store', dest="outfile", type="string",
                      default="", help="Filepath to save to plot under.")

    parser.add_option('-t', '--aformat', action='store', dest="aformat", type="string",
                      default="", help="[*Advanced*] Additional formatting operations.")

    parser.add_option('-s', '--showplot', action='store_true',
                      default=False, help="Display the plot interactively after saving.")

    parser.add_option('-v', '--verbose', action='store_true',
                      default=False, help="Print detailed information")

    options, args = parser.parse_args()

    if not options.filename:
        print "No target file. Stop."
        parser.print_help()
        return

    if args:
        print "ERROR when parsing, leftover arguments", args
        parser.print_help()
        raise SystemExit

    if options.outfile:
        outfile = "{}.png".format(options.outfile.replace(".pdf","").replace(".png", ""))
    else:
        outfile = "{}_dump.png".format(options.filename.replace(".dat",""))

    phase_space_plot = True
    histo_dE_plot = True

    data = _cdl.ColumnData(options.filename)
    if options.aformat:
        data.filter_data(options.aformat)

    data.sort_data("s[m]", reverse=False)

    if phase_space_plot:
        _plu.plot_phase_space(data["x[mm]"], data["y[mm]"], data["xp[mrad]"], data["yp[mrad]"],
                              data["dE/E[1]"], data["z[mm]"],
                              # color=data["dE/E[1]"], col_label="dE/E [1]",
                              outputfilename=outfile,
                              showplot=options.showplot)

    if histo_dE_plot:
        _plt.hist(data["dE/E[1]"], log=True)
        _plt.xlabel("dE/E [1]")
        _plt.ylabel("Count")
        _plt.savefig("dE_histo_{}".format(outfile.replace(".png", ".pdf")))
        _plt.show()


if __name__=="__main__":
    main()
