#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os as _os
import glob as _glob
import optparse as _optparse
import datetime as _datetime


def make_status_summary(studydir, outputfilename):
    """
    Check the completion status of individual runs in
    a larger study
    """

    run_statuses = []
    studypath = _os.path.abspath(studydir)

    print "Study directory {}".format(studypath)
    print "Scanning  data..."
    rundirs = [_os.path.join(studypath, each) for each in _os.listdir(studydir)
               if each.startswith("run_")]

    for rundir in rundirs:
        fort999_found = False
        fort999_iszipped = False
        fort208_found = False

        fort_999 = _glob.glob(_os.path.join(rundir, "*fort.999*"))
        if not fort_999:  # If the original format fails, try the new one
            fort_999 = _glob.glob(_os.path.join(rundir, "*aperture_losses*"))

        if fort_999:
            fort999_found = True
            if fort_999[0].endswith(".gz"):
                fort999_iszipped = True

        if _glob.glob(_os.path.join(rundir, "*fort.208*")):
            fort208_found = True

        all_good = fort999_found and fort999_iszipped and fort208_found

        run_statuses.append((_os.path.basename(rundir), fort999_found, fort999_iszipped,
                             fort208_found, all_good))

    ntotal = len(run_statuses)
    nsuccessful = len([rs for rs in run_statuses if rs[-1]])
    nfailed = ntotal-nsuccessful

    def _forprint(status_in):
        print_values = []
        for val in status_in:
            if isinstance(val, bool):
                print_values.append("Y" if val else "N")
            else:
                print_values.append(val.replace("run_", ""))

        return print_values

    with open(outputfilename, "w") as fileout:
        fileout.write("Job status summary {} \n".format(_datetime.datetime.today()))
        fileout.write("Study directory: {} \n".format(studypath))
        fileout.write("Total run checked: {} \n".format(ntotal))
        fileout.write("Successful: {}, Failed: {}\n".format(nsuccessful, nfailed))
        fileout.write("-"*64+"\n")
        colformat = "|{: ^11}| {: ^11} {: ^11} {: ^11} | {: ^11} |\n"
        fileout.write(colformat.format("Run no.", "fort999", "fort999_zip",
                                       "fort208", "successful"))
        fileout.write("-"*64+"\n")
        for status in run_statuses:
            fileout.write(colformat.format(*_forprint(status)))


def main():
    usage = ''
    parser = _optparse.OptionParser(usage)
    parser.add_option('-d', '--dir', action='store', dest="inputdir", type="string",
                      default="None", help="Path to study directory with run_xxxx "
                      "subdirectories containing the result files.")

    parser.add_option('-o', '--output', action='store', dest="outfile", type="string",
                      default="", help="Filepath to save the summary file under.")

    options, args = parser.parse_args()

    if not options.inputdir:
        print "No target directory. Stop."
        parser.print_help()
        return

    if args:
        print "ERROR when parsing, leftover arguments", args
        parser.print_help()
        raise SystemExit

    absinputdir = _os.path.abspath(options.inputdir)
    if options.outfile:
        outfile = "{}.txt".format(options.outfile.replace(".dat", "").replace(".txt", ""))
    else:
        outfile = _os.path.join(absinputdir, "job_status_summary.txt")

    make_status_summary(absinputdir, outfile)


if __name__ == "__main__":
    main()
