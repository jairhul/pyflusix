#!/usr/bin/env python

import optparse as _optparse
import numpy as _np
import matplotlib.pyplot as _plt

import pyflusix.aperture_losses as _apl
import pyflusix.sixtrack_aperture as _sxtap
import pyflusix.plotting_utilities as _plu
import matplotlib.ticker as _mtick


def main():
    usage = ''
    parser = _optparse.OptionParser(usage)
    parser.add_option('--filename', action='store', dest="filename", type="string",
                      default="fort.impacts", help="Path to file containing impacts."
                      "Default is fort.impacts.")

    parser.add_option('--plane', action='store', dest="plane", type="string",
                      default="horizontal", help="Plane to make the plots in: horizontal or"
                      "vertical. Default is horizontal")

    parser.add_option('--smin', action='store', dest="smin", type="float",
                      default=None, help="Starting S position of region of interest.")

    parser.add_option('--smax', action='store', dest="smax", type="float",
                      default=None, help="Final S position of region of interest.")

    parser.add_option('--sstart', action='store', dest="sstart", type="float",
                      default=None, help="Offset to be applied to all S position."
                      "Offset is applied before region selection.")

    parser.add_option('--stot', action='store', dest="stot", type="float",
                      default=None, help="Total S (length) of the machine."
                      "Must specify together with the --soffs option.")

    parser.add_option('--ymin', action='store', dest="ymin", type="float",
                      default=0.0, help="Minimum y-axis extent of plot.")

    parser.add_option('--ymax', action='store', dest="ymax", type="float",
                      default=0.0, help="Maximum y-axis extent of plot.")

    parser.add_option('--aperture', action='store', dest="aperfilename", type="string",
                      default="", help="Name of aperture dump from SixTrack (Optional).")

    parser.add_option('--output', action='store', dest="outfile", type="string",
                      default="", help="Filepath to save the plot under.")

    parser.add_option('-p', '--phplot', action='store_true',
                      default=False, help="Make a projected phase space plot of the losses.")

    parser.add_option('-z', '--azplot', action='store_true',
                      default=False, help="Make a 2D histrogram of A and Z of the losses.")

    parser.add_option('-a', '--waplot', action='store_true',
                      default=False, help="Make an energy-weighted histogram of A of the lossses.")

    parser.add_option('-s', '--splot', action='store_true',
                      default=False, help="Make an plot of the loss coordinates"
                      "agains S for the selected plane.")

    parser.add_option('-v', '--verbose', action='store_true',
                      default=False, help="Print detailed information")

    options, args = parser.parse_args()

    if not options.filename:
        parser.print_help()
        print "No filename specified. Stop."
        return

    if not any((options.phplot, options.azplot, options.waplot, options.splot)):
        parser.print_help()
        msg = ("\nNo plot option specified, use -spaz to enable all plots"
               "or refer to the help for individual options.")
        print msg

        return

    if args:
        print "ERROR when parsing, leftover arguments", args
        parser.print_help()
        raise SystemExit

    if options.outfile:
        outfile = options.outfile.replace(".pdf", "").replace(".png", "")
    else:
        outfile = options.filename.replace("fort.", "").replace(".gz", "").replace(".txt", "")

    filename = options.filename
    aperfile = options.aperfilename

    if options.plane not in ("horizontal", "vertical"):
        msg = "Unrecognised plane {}, please use 'horizontal' or 'vertical'."
        raise IOError(msg.format(options.plane))

    imp = _apl.ApertureLosses(filename, s_min=options.smin, s_max=options.smax,
                              s_start=options.sstart, s_total=options.stot)
    impacts = imp.impacts

    dscale = 1.e3 # Make meters into mm
    ascale = 1.e3
    S = _np.array([val[0] for val in impacts])
    E = _np.array([val[1] for val in impacts])
    A = _np.array([val[2] for val in impacts])
    Z = _np.array([val[3] for val in impacts])
    x = _np.array([val[4]*dscale for val in impacts])
    xp = _np.array([val[5]*ascale for val in impacts])
    y = _np.array([val[6]*dscale for val in impacts])
    yp = _np.array([val[7]*ascale for val in impacts])

    _plt.rcParams["figure.figsize"] = (12, 7)
    myfontsize = "medium"

    # Plotting the trace space in 4 subplots
    if options.phplot:
        step = 1  # TODO: This is not used, consider removing
        f, axarr = _plt.subplots(2, 2)
        axarr[1, 0].scatter(x[::step], y[::step], s=10, c="b")
        axarr[1, 0].set_xlabel("x [mm]", fontsize=myfontsize)
        axarr[1, 0].set_ylabel("y [mm]", fontsize=myfontsize)

        axarr[0, 0].scatter(x[::step], xp[::step], s=10, c="b")
        axarr[0, 0].set_xlabel("x [mm]", fontsize=myfontsize)
        axarr[0, 0].set_ylabel("xp [mrad]", fontsize=myfontsize)

        pt = axarr[1, 1].scatter(y[::step], yp[::step], s=10, c="b")
        axarr[1, 1].set_xlabel("y [mm]", fontsize=myfontsize)
        axarr[1, 1].set_ylabel("yp [mrad]", fontsize=myfontsize)

        # axarr[0, 1].scatter(xp[::step], yp[::step], s=5, c="b")#c=A[::step])
        # axarr[0, 1].scatter(A[::step], E[::step], s=5, c="b")
        axarr[0, 1].hist(E, weights=_np.full_like(E, 1./len(E)), log=True)
        axarr[0, 1].set_xlabel("E [GeV]", fontsize=myfontsize)
        axarr[0, 1].set_ylabel("Fraction", fontsize=myfontsize)
        axarr[0, 1].xaxis.set_major_formatter(_mtick.FormatStrFormatter('%.0e'))

        # f.subplots_adjust(right=0.85, wspace=0.4, hspace=0.4)
        # cbar_ax = f.add_axes([0.9, 0.1, 0.02, 0.78])
        # cbar = f.colorbar(pt, cax=cbar_ax)
        # cbar.set_label("A", fontsize=myfontsize)

        _plt.tight_layout()
        # Save as png as millons of points
        _plt.savefig("{}_imp_phasespace.png".format(outfile), bbox_inches="tight")

    # Ploting the 2D A-Z histogram
    if options.azplot:
        fig = _plt.figure(7)
        _plu.draw_2d_histo_AZ(A, Z, E=E, fig=fig, fontsize=20,
                              outfile="{}_AZ_histo.pdf".format(outfile))

    # Ploting the energy-weighted A histogram
    if options.waplot:
        fig = _plt.figure(8)
        _plu.draw_histo_A(A, E=E, fig=fig, fontsize=20,
                          outfile="{}_energetic_fractions.pdf".format(outfile))

    # Plotting longitudinal losses
    if options.splot:
        fig = _plt.figure(3, figsize=(12, 6))

        if aperfile:
            aper = _sxtap.SixTrackAperture(aperfile, s_start=imp.machine.s_start,
                                           s_total=imp.machine.length, debug=0)
            fig = aper.plot_aperture_profile(plane=options.plane, figure=fig, linewidth=3.5)

        ax = fig.gca()
        if options.plane == "horizontal":
            ax.scatter(S, x, label="aperture impacts", c="r", s=60, zorder=10)
            ax.set_ylabel("x [mm]")

        elif options.plane == "vertical":
            ax.scatter(S, y, label="aperture impacts", c="r", s=60, zorder=10)
            ax.set_ylabel("y [mm]")

        ax.set_xlabel("S [m]")

        xlim = ax.get_xlim()
        if options.smin is None:
            sstart = xlim[0]
        else:
            sstart = options.smin
        if options.smax is None:
            send = xlim[1]
        else:
            send = options.smax

        ax.set_xlim(sstart, send)

        ylim = ax.get_ylim()
        if options.ymin and options.ymax:
            ylim = (options.ymin, options.ymax)
        elif options.ymin:
            ylim[0] = options.ymin
        elif options.ymax:
            ylim[1] = options.ymax

        ax.set_ylim(ylim)
        ax.legend(fontsize="large")
        _plt.savefig("{}_longitudinal.png".format(outfile), bbox_inches="tight")

    _plt.show()


if __name__ == "__main__":
    main()
