#!/usr/bin/env python

import optparse as _optparse
import numpy as _np

import pyflusix.data_loader as _cdl
import pyflusix.plotting_utilities as _plu


def main():
    usage = ''
    parser = _optparse.OptionParser(usage)
    parser.add_option('-f', '--file', action='store', dest="filename", type="string",
                      default="", help="Path to file containing initial distribution, "
                      "normally called initials.dat.")

    parser.add_option('-p', '--plane', action='store', dest="plane", type="string",
                      default="horizontal", help="Plane to make the plots in: "
                      "horizontal or vertical")

    parser.add_option('-o', '--output', action='store', dest="outfile", type="string",
                      default="", help="Filepath to save the plot under.")

    parser.add_option('-v', '--verbose', action='store_true',
                      default=False, help="Print detailed information")

    options, args = parser.parse_args()

    if not options.filename:
        print "No filename specified. Stop."
        parser.print_help()
        return

    if args:
        print "ERROR when parsing, leftover arguments", args
        parser.print_help()
        raise SystemExit

    if options.outfile:
        outfile = options.outfile.replace(".pdf", "").replace(".png", "")
    else:
        outfile = "{}_initialdist".format(options.filename.replace(".dat", "").replace(".gz", "").replace(".txt", ""))

    filename = options.filename

    data = _cdl.ColumnData(filename)
    dscale = 1.e3
    ascale = 1.e3

    mean_E = _np.mean(data[12])
    dE = data[12] - mean_E

    _plu.plot_phase_space(data[3]*dscale, data[4]*ascale, data[6]*dscale, data[7]*ascale,
                          dE, data[5]*dscale, color=None, nbins=80, dunit="mm", aunit="mrad",
                          eunit="GeV", outputfilename="{}.pdf".format(outfile),
                          showplot=True)


if __name__ == "__main__":
    main()
