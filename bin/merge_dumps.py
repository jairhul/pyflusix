#!/usr/bin/env python

import optparse as _optparse

import pyflusix.sixtrack_dump as _sxd


def main():
    usage = ''
    parser = _optparse.OptionParser(usage)
    parser.add_option('-d', '--studydir', action='store', dest="inputdir", type="string",
                      default="None", help="Study directory with run_xxxx subdirectories "
                      "containing the dump files.")

    parser.add_option('-f', '--filename', action='store', dest="filename", type="string",
                      default="", help="Dump filename or string to match dump filenames with.")

    parser.add_option('-o', '--output', action='store', dest="outfile", type="string",
                      default="", help="Filepath to save the merged dump file under.")

    parser.add_option('-t', '--aformat', action='store', dest="aformat", type="string",
                      default="", help="[*Advanced*] Additional formatting operations.")

    parser.add_option('-i', '--imax', action='store', dest="imax", type="int",
                      default=-1, help="Maximum number of runs to load from the study directory.")

    parser.add_option('-v', '--verbose', action='store_true',
                      default=False, help="Print detailed information")

    options, args = parser.parse_args()

    if not options.inputdir:
        print "No target directory. Stop."
        parser.print_help()
        return

    if not options.filename:
        print "No filename specified. Stop."
        parser.print_help()
        return

    if args:
        print "ERROR when parsing, leftover arguments", args
        parser.print_help()
        raise SystemExit

    if options.outfile:
        namestub = options.outfile.replace(".dat", "").replace(".txt", "")
        outfile = "{}.dat".format(namestub)
    else:
        namestub = options.filename.replace(".dat", "").replace(".gz", "").replace(".txt", "")
        outfile = "merged_{}.dat".format(namestub)

    tch = _sxd.SixTrackDump(options.inputdir, dump_filename=options.filename,
                            filterstring=options.aformat, skiplines=5,
                            outputfilename=outfile, outputdir="", imax=options.imax)


if __name__ == "__main__":
    main()
