#!/usr/bin/env python

import optparse as _optparse
import numpy as _np

import pyflusix.data_loader as _cdl
import pyflusix.plotting_utilities as _plu


def main():
    usage = ''
    parser = _optparse.OptionParser(usage)
    parser.add_option('-f', '--file', action='store', dest="filename", type="string",
                      default="", help="Path to file with touches to load.")

    parser.add_option('-c', '--collid', action='store', dest="collid", type="int",
                      default=0, help="Id of the collimator where to make the plot.")

    parser.add_option('-p', '--plane', action='store', dest="plane", type="int",
                      default=0, help="Transverse plane, 0 - hor, 1 - ver. Default is 0")

    parser.add_option('-g', '--halfgap', action='store', dest="hgap", type="float",
                      default=0, help="Half-gap of the collimator in mm, only used to"
                      "calculate impact mean parameter. (Optional)")

    parser.add_option('-o', '--output', action='store', dest="outfile", type="string",
                      default="", help="Filepath to save to plot under."
                      "Default is current directory.")

    parser.add_option('-t', '--aformat', action='store', dest="aformat", type="string",
                      default="", help="[*Advanced*] Additional formatting"
                      "operations. (Optional)")

    parser.add_option('-s', '--showplot', action='store_true',
                      default=False, help="Display the plot interactively after saving."
                      "Default is False")

    parser.add_option('-v', '--verbose', action='store_true',
                      default=False, help="Print detailed information")

    options, args = parser.parse_args()

    if not options.filename:
        print "No target file. Stop."
        parser.print_help()
        return

    if not isinstance(options.plane, int) or options.plane > 1 or options.plane < 0:
        parser.print_help()
        parser.error("Invalid plane specifier. Please specify 0 or 1 for the plane.")

    if args:
        print "ERROR when parsing, leftover arguments", args
        parser.print_help()
        raise SystemExit

    if options.outfile:
        outfile = options.outfile
    else:
        namestub = options.filename.replace(".dat", "").replace(".txt", "")
        outfile = "{}_first_coll{}.png".format(namestub, options.collid)

    data = _cdl.ColumnData(options.filename)
    data.filter_data("@{}={}".format(0, options.collid))
    # data.sortOnColumn(2, reverse=False)
    if options.aformat:
        data.filter_data(options.aformat)

    _plu.plot_phase_space(data["X[mm]"], data["Y[mm]"], data["XP[mrad]"], data["YP[mrad]"],
                          data["Ekin[GeV]"], data["S[m]"], scatter=True,
                          outputfilename=outfile,
                          showplot=options.showplot)

    if options.hgap:
        if options.plane == 0:
            coord = "X"
        else:
            coord = "Y"

        dt = data["{}[mm]".format(coord)] # Get X or Y data via the data dict
        print "min {}: {}".format(coord, min(dt))
        print "max {}: {}".format(coord, max(dt))

        positive = [val for val in dt if val > 0]
        negative = [val for val in dt if val < 0]
        jaw_opening_pos = options.hgap
        jaw_opening_neg = -options.hgap

        msg = "Received jaw opening, positve {}: {} mm "
        print msg.format(coord, jaw_opening_pos)

        msg = "Received jaw opening, negative {}: {} mm"
        print msg.format(coord, jaw_opening_neg)

        msg = "Mean impact parameter, positive {}: {} mm"
        print msg.format(coord, _np.mean(positive) - jaw_opening_pos)

        msg = "mean impact parameter, negative {}: {} mm"
        print msg.format(coord, abs(_np.mean(negative) - jaw_opening_neg))

    """
    if histoS_plot:
        _plt.hist(data["S[m]"], log=True)
        _plt.xlabel("S [m]")
        _plt.ylabel("Count")
        _plt.savefig("spos_histo_col{}.pdf".format(colid))
        _plt.show()
    """

    print "Done!"


if __name__ == "__main__":
    main()
