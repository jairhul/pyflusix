#!/usr/bin/env python

import optparse as _optparse

import pyflusix.fluka_touches as _flt


def main():
    usage = ''
    parser = _optparse.OptionParser(usage)
    parser.add_option('-d', '--studydir', action='store', dest="inputdir", type="string",
                      default="None", help="Study directory with run_xxxx"
                      "subdirectories containing the touches.")

    parser.add_option('-o', '--outputdir', action='store', dest="outputdir", type="string",
                      default="None", help="Output directory where to store the"
                      "merged first touches.")

    parser.add_option('-i', '--imax', action='store', dest="imax", type="int", default=-1,
                      help="Maximum number of runs to load from the study directory.")

    parser.add_option('-v', '--verbose', action='store_true',
                      default=False, help="Print detailed information")

    options, args = parser.parse_args()

    if not options.inputdir:
        print "No target file. Stop."
        parser.print_help()
        return

    if args:
        print "ERROR when parsing, leftover arguments", args
        parser.print_help()
        raise SystemExit

    if options.inputdir == "None" or options.outputdir == "None":
        raise SystemExit("Please specify both an input and an output directory")

    touches = _flt.FlukaTouches(study_directory=options.inputdir, imax=options.imax,
                                outputdir=options.outputdir)

    print "Done!"


if __name__ == "__main__":
    main()
