#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import os as _os
import optparse as _optparse
import numpy as _np
import warnings as _warnings
import matplotlib.pyplot as _plt
import matplotlib.ticker as _tick

import pandas as _pd
import pyflusix.output as _hs
import pyflusix.machine_parameters as _mcp


def plot_lossmap(study_dir, machine=None, startS=None, minS=None, maxS=None,
                 quench_limit=None, lattice=None, reverse_lattice=False,
                 beam=1, twissfile=None, outfile="lossmap.pdf", show_plot=True,
                 verbose=True, debug=False):

    # If the machine argument is set to None, try to load a machine info file if it exists
    infofile = _os.path.join(_os.path.abspath(study_dir), "output/machine_info.txt")
    if not machine and _os.path.isfile(infofile):
        if verbose:
            print("Found machine info file: {}".format(infofile))
        machine_params = _mcp.MachineParameters()
        machine_params.load_from_infofile(infofile)
        cmachine = machine_params.length
        startS = machine_params.s_start
        machine = machine_params.machine

    # The warm regions are defined in the reference frame of beam1
    # so need to specify if beam2 or beam4 are used so transforms can be applied
    data = _hs.LoadStudy(study_dir, twissfile=twissfile, machine=machine,
                         startS=startS, verbose=verbose, debug=debug, beam=beam)

    clhc = 26658.8832  # TODO: Rewrite using the Machine Parameters class
    cfcc = 97749.3853421
    csps = 6900
    cps  = 628
    if machine == "lhc":
        cmachine = clhc
    elif machine == "hllhc":
        cmachine = clhc
    elif machine == "fcc":
        cmachine = cfcc
    elif machine == "sps":
        cmachine = csps
    elif machine == "ps":
        cmachine = cps
    else:
        raise ValueError("Urecognised machine: {}".format(machine))

    binwidth = 0.1
    # nbins = int(cmachine / binwidth)

    smin = 0
    smax = cmachine
    if minS is not None:
        try:
            smin = float(minS)
        except ValueError:
            raise ValueError("Smin must be a number")
        if smin < 0 or smin > cmachine:
            msg = "Smin must have a value between 0 and the circumference of the machine"
            raise ValueError(msg)

    if maxS is not None:
        try:
            smax = float(maxS)
        except ValueError:
            raise ValueError("Smin must be a number")
        if smax < smin or smax > cmachine:
            msg = "Smax must have a value between Smin and the circumference of the machine"
            raise ValueError(msg)

    if verbose:
        print("Plotting from S = {} m to S = {} m".format(smin, smax))

    xmin, xmax = smin, smax
    ymin, ymax = 1e-7, 1

    # emax = max(data.coll.elost)

    if beam == 24:
        data.coll.s = cmachine - data.coll.s
        data.warm.s = cmachine - data.warm.s
        data.cold.s = cmachine - data.cold.s
        reverse_lattice = True if not reverse_lattice else False

    if debug:
        print("data.cold.s")
        print(data.cold.s)

        print("data.cold.elost")
        print(data.cold.elost)

        print("data.coll.s")
        print(data.coll.s)

        print("data.coll.elost")
        print(data.coll.elost)

        print("data.warm.s")
        print(data.warm.s)

        print("data.warm.elost")
        print(data.warm.elost)

    coll_loss = data.coll['elost'].values
    coll_loss[_pd.isnull(coll_loss)] = 0.0  # Filter out NaNs if collimator has no energy deposition

    # Normalise to total energy lost instead of peak energy lost
    data.norm = sum(coll_loss) + sum(data.warm.elost) + sum(data.cold.elost)

    coll_loss = coll_loss / data.coll['len'].values  # normalise to collimator active length

    _plt.rcParams["figure.figsize"] = (24, 7)

    fig, ax1 = _plt.subplots(1, 1)

    # The collimator loss S is taken from the TFS file, while the warm/cold loss S
    # is taken from SixTrack. If the start point for the Sixtrack is not the same as
    # the TFS, a correction must be appled to warm/cold loss S

    # Empirically determinded scaling of plotted bin widths
    bwf = (300*((xmax-xmin)/clhc) - _np.log((xmax-xmin)/clhc))

    if data.cold['elost'].values.size:
        _plt.bar(data.cold['s'].values, data.cold['elost'].values/(binwidth*data.norm),
                 bwf*binwidth, label='Cold', color="blue", edgecolor='blue', zorder=7)

    if data.warm['elost'].values.size:
        _plt.bar(data.warm['s'].values, data.warm['elost'].values/(binwidth*data.norm),
                 bwf*binwidth, label='Warm', color="red", edgecolor='red', zorder=6)

    if coll_loss.size:
        _plt.bar(data.coll['s'].values.astype('float'), coll_loss/data.norm,
                 bwf*1/10., label='Collimator', color='black', edgecolor='black', zorder=8)

    if verbose:
        print("Plot bar widh: {} m".format(bwf))
        if coll_loss.size:
            print("Max losses:")
            max_coll_loss = max(coll_loss)
            max_coll_s = data.coll['s'].values.astype('float')[list(coll_loss).index(max_coll_loss)]
            print("Collimator {:.3E} GeV at S = {}".format(max_coll_loss, max_coll_s))
        if data.warm['elost'].values.size:
            max_warm_loss = max(data.warm.elost)
            max_warm_s = data.warm['s'].values.astype('float')[list(data.warm.elost).index(max_warm_loss)]
            print("Warm aperture: {:.3E} GeV at S = {}".format(max_warm_loss, max_warm_s))
        if data.cold['elost'].values.size:
            max_cold_loss = max(data.cold.elost)
            max_cold_s = data.cold['s'].values.astype('float')[list(data.cold.elost).index(max_cold_loss)]
            print("Cold aperture: {:.3E} GeV at S = {}".format(max_cold_loss, max_cold_s))

    # PLOT PROPERTIES
    if quench_limit:
        ax1.axhline(y=quench_limit, color="r", linestyle="--", label="quench limit")
    ax1.set_yscale('log', nonpositive='clip')
    ax1.set_xlim(xmin, xmax)
    ax1.set_ylim(ymin, ymax)
    ax1.set_xlabel(r's (m)')
    ax1.set_ylabel(r'$\eta$ (1/m)')
    ax1.yaxis.grid(False)
    ax1.yaxis.grid(b=True, which='major', zorder=0)
    ax1.yaxis.grid(b=True, which='minor', zorder=0)

    ax1.tick_params(axis='y', which='major', labelsize="x-small")
    ax1.tick_params(axis='y', which='minor', labelsize="x-small")

    # locmaj = _tick.LogLocator(numticks=8)
    # ax1.yaxis.set_major_locator(locmaj)

    # locmin = _tick.LogLocator(subs=(0.1,0.2,0.4,0.6,0.8,1,2,4,6,8,10))
    # ax1.yaxis.set_minor_locator(locmin)
    # ax1.yaxis.set_minor_formatter(_tick.NullFormatter())

    ax1.legend(loc=0, fontsize="small")

    if lattice and _os.path.isfile(lattice):
        found_pymadx = False
        try:
            from pymadx.Plot import AddMachineLatticeToFigure as _draw_lattice
            found_pymadx = True
        except ImportError:
            _warnings.warn("pymadx package not found, cannot draw the machine lattice diagram")
            _draw_lattice = None

        if found_pymadx:
            _draw_lattice(_plt.gcf(), lattice, reverse=reverse_lattice)
            _plt.xlim(xmin, xmax)

    if not outfile.endswith(".pdf") and not outfile.endswith(".png"):
        outfile = "{}.pdf".format(outfile)
    _plt.savefig(outfile)

    if show_plot:
        _plt.show()


def main():
    usage = ('Plots a lossmap from processed sixtrack-fluka coupling output.'
             ' By default the machine length and starting S position are taken from'
             ' the machine parameters file in studydir/output/machine_info.txt')
    parser = _optparse.OptionParser(usage)
    parser.add_option('--dir', action='store', dest="studydir", type="string",
                      default="", help="Path to the directory with run_xxxxx files")

    parser.add_option('--machine', action='store', dest="machine", type="string",
                      default="", help="Machine - lhc, hllhc, fcc. (Optional)")

    parser.add_option('--startS', action='store', dest="startS", type="string",
                      default="clean_input/fort.2",
                      help="Offset of SixTrack S coordinate from the TFS"
                      " - value in meters or path to fort.2 file. (Optional)")

    parser.add_option('--smin', action='store', dest="smin", type="float",
                      default=None, help="Lower S bound for plot (Optional)")

    parser.add_option('--smax', action='store', dest="smax", type="float",
                      default=None, help="Upper S bound for plot (Optional)")

    parser.add_option('--quench', action='store', dest="quench", type="float",
                      default=None, help="Quench limit in 1/m (Optional)")

    parser.add_option('--lattice', action='store', dest="lattice", type="string",
                      default=None, help="TFS file of the thick lattice with appropriate"
                      "columns for a  machine diagram. (Optional)")

    parser.add_option('--twissFile', action='store', dest="twissfile", type="string",
                      default=None, help="TFS file of the thin lattice with appropriate"
                      "columns for finding starting position of lattice in fort.2. (Optional)")

    parser.add_option('-r', '--reverse_lattice', action='store_true',
                      default=False, help="Reverse the plotting of the machine lattice"
                      " specified by the --lattice option (Optional)")

    parser.add_option('--beam', action='store', dest='beam', type='int',
                      default=1, help="The beam can be one of 1, 2, 4, 24."
                      " Beam 24 plots beam 2 as beam4 and automatically reverses the"
                      "lattice specified by --lattice (Optional)")

    parser.add_option('--outfile', action='store', dest="outfile", type="string",
                      default="lossmap.pdf", help="File to save the plot under")

    parser.add_option('--show', action='store_true',
                      default=True, help="Display the plot interactively."
                                         "Default is True (Optional)")

    parser.add_option('-q', '--quiet', action='store_true',
                      default=False, help="Silence all printouts (Optional)")

    parser.add_option('-x', '--debug', action='store_true',
                      default=False, help="Enable detailed pribtouts for debugging (Optional)")

    options, args = parser.parse_args()

    # Sanity checks on input
    if not options.studydir:
        parser.print_help()
        parser.error("Please provide the path to study directory via the --study_dir argument")

    if options.reverse_lattice and not options.lattice:
        parser.print_help()
        parser.error("The -r, --reverse_lattice flag requires the --latice option to be set.")

    if args:
        print("ERROR when parsing, leftover arguments", args)
        parser.print_help()
        raise SystemExit

    verbose = not options.quiet
    plot_lossmap(options.studydir, options.machine, options.startS, options.smin, options.smax,
                 options.quench, options.lattice, options.reverse_lattice, options.beam,
                 options.twissfile, options.outfile, options.show, verbose, options.debug)


if __name__ == "__main__":
    main()
