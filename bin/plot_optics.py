#!/usr/bin/env python

import os as _os
import optparse as _optparse
import matplotlib.pyplot as _plt

import pyflusix.sixtrack_optics as _sxo


def main():
    usage = ''
    parser = _optparse.OptionParser(usage)
    parser.add_option('-f', '--filepath', action='store', dest="filename", type="string",
                      default="", help="Path to file with touches to load.")

    parser.add_option('-o', '--output', action='store', dest="outfile", type="string",
                      default="", help="Filepath to save to plot under.")

    parser.add_option('-t', '--aformat', action='store', dest="aformat", type="string",
                      default="", help="[*Advanced*] Additional formatting operations.")

    parser.add_option('-s', '--showplot', action='store_true',
                      default=False, help="Display the plot interactively after saving.")

    parser.add_option('-v', '--verbose', action='store_true',
                      default=False, help="Print detailed information")

    options, args = parser.parse_args()

    if not options.filename:
        print "No target file. Stop."
        parser.print_help()
        return

    if args:
        print "ERROR when parsing, leftover arguments", args
        parser.print_help()
        raise SystemExit

    if options.outfile:
        outfile = "{}.pdf".format(options.outfile.replace(".pdf", "").replace(".png", ""))
    else:
        # Strip the extension, but preserve other dots in the name
        filename_base = ".".join(options.filename.split(".")[:-1])
        outfile = "{}_optics.pdf".format(filename_base)

    optics = _sxo.SixTrackOptics(options.filename)

    if options.aformat:
        optics.filter_optics(options.aformat)

    optics.plot_beta()

    # Manual hack is some comparison is needed TODO: Make a dedicated comparison method
    # op2 = SixTrackOptics("/Users/auser/adir/afile.dat")
    # op2.plotBeta()

    _plt.tight_layout()
    _plt.savefig(_os.path.join(outfile), bbox_inches="tight")

    _plt.show()

    print "Done!"


if __name__ == "__main__":
    main()
