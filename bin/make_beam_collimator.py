#!/usr/bin/env python

from __future__ import print_function

import os as _os
import optparse as _optparse
import datetime as _datetime
import warnings as _warnings
import numpy as _np

from string import Template as _tpl


PROTON_MASS_FLUKA = 0.93827231
PROTON_MASS_K2 = 0.938272088

def norm_to_physical_emittance(norm_emittance, mass, E):
    gamma = E/mass
    beta = _np.sqrt(1-(1/gamma)**2)

    return norm_emittance / beta / gamma


def get_coll_length_fort3(filepath, coll_name, verbose=True):
    abs_filepath = _os.path.abspath(filepath)

    coll_name = coll_name.lower() # use all lowercase

    coll_length = None

    in_fluka_block = False
    with open(abs_filepath, "r+") as f:
        for line in f:
            if line[0] in ["/", "!"]:  # Ignore commented out lines
                continue

            if "FLUKA" in line:
                in_fluka_block = True

            if in_fluka_block:
                if line.lower().startswith(coll_name):
                    coll_length = float(line.split()[3])

                elif "NEXT" in line:
                    break

    if coll_length is None:
        raise SystemExit("Could not find the length of collimator {} in fort.3".format(coll_name))

    if verbose:
        print("--> From fort.3: collimator {} has a length of {} m.\n".format(coll_name,  coll_length))

    return coll_length


def load_beam_def_fort3(filepath, verbose=True, pmass=PROTON_MASS_FLUKA):
    abs_filepath = _os.path.abspath(filepath)

    # Somewhat complex conditional structure, but all the data
    # can be found in a signle line-by-line read of the file
    beam_energy = None
    particle_info = None

    found_particledef = False
    found_init = False
    init_counter = 0

    with open(abs_filepath, "r+") as f:
        for line in f:
            if line[0] in ["/", "!"]:  # Ignore commented out lines
                continue

            if "INIT" in line:
                found_init = True

            elif found_init and beam_energy is None:
                # The total energy is found 12 lines after INIT (apparently)
                if init_counter == 13:
                    beam_energy = line.strip().split()[0]
                else:
                    init_counter += 1

            elif "HION" in line:
                found_particledef = True

            # First elligible line after HION is the needed data
            elif particle_info is None and found_particledef:
                particle_info = line.strip().split()

    if beam_energy is None:
        raise SystemExit("Could not find the beam energy defintion in fort.3")

    if particle_info is None:
        particle_info = (1, 1, pmass, 1, 0)  # If no explicit ion defintion use proton

    # Mass in MeV, Z, A
    # SixTrack versions >5 include an optional argument, the charge Q (default is Q=Z)
    Q = int(particle_info[3]) if len(particle_info) > 3 else int(particle_info[1])
    pdg_id = int(particle_info[4]) if len(particle_info) > 4 else 0

    particle_data = [int(particle_info[0]), int(particle_info[1]), float(particle_info[2]),
                     float(beam_energy) * 1.e-3, Q, pdg_id]

    # Newer SixTrack versions also have an optional 5th argument, the PDG id, which has no default and is ignored for now

    if verbose:
        if particle_data[2] != pmass: # Check on the mass as electrons (A=0,Z=0) are ran through the ion interface
            print("--> From fort.3: ion beam, A= {} GeV, Z= {}, Mass= {:.17}, E= {} GeV, Q= {} e".format(*particle_data))
        else:
            print("--> From fort.3: proton beam, E= {} GeV".format(particle_data[3]))

    return particle_data


def load_coll_info_collgaps(filepath, coll_name, verbose=True):
    abs_filepath = _os.path.abspath(filepath)

    coll_info = None
    with open(abs_filepath, "r+") as f:
        for line in f:
            if coll_name in line or coll_name.lower() in line or coll_name.upper() in line:
                coll_info = line.strip().split()

    if coll_info is None:
        raise SystemExit("Could not find info for collimator {} in collgaps".format(coll_name))

    c = coll_info  # shortcut
    # L, NSIGMA, rotation
    reduced_coll_info = [float(c[7]), float(c[12]), float(c[2])]

    if verbose:
        print("From collgaps: Ljaw= {} m, Nsgima= {}, anlge= {} rad".format(*reduced_coll_info))
        print("From collgaps: half-gap= {}".format(float(c[5])))

    return reduced_coll_info


def load_coll_info_colldb(filepath, coll_name, verbose=True):
    abs_filepath = _os.path.abspath(filepath)

    coll_info = []
    found = False
    with open(abs_filepath, "r+") as f:
        for line in f:
            if coll_name.lower() in line:  # start reading info when lowercase name is found
                found = True

            elif found:
                if line.startswith("#"):
                    break  # The termination character stops the read
                coll_info.append(line.strip())

    if not coll_info:
        raise SystemExit("Could not find info for collimator {} in CollDB".format(coll_name))

    c = coll_info  # shortcut
    # L, NSIGMA, rotation
    reduced_coll_info = [float(c[2]), float(c[0]), float(c[3])]

    if verbose:
        print("From CollDB: Ljaw= {} m, Nsgima= {}, anlge= {} rad".format(*reduced_coll_info))

    return reduced_coll_info


def load_coll_optics_six(filepath, coll_name, verbose=True):
    abs_filepath = _os.path.abspath(filepath)

    coll_optics = None
    with open(abs_filepath, "r+") as f:
        for line in f:
            if coll_name.lower() in line:
                coll_optics = line.strip().split()

    if coll_optics is None:
        msg = "Could not find info for collimator {} in SixTrack optics"
        raise SystemExit(msg.format(coll_name))

    c = coll_optics # shortcut
    # s, betax, alphax, betay, alphay, dx, dy, dpx, dpy, x, xp, y, yp
    reduced_coll_optics = [float(c[2]), float(c[3]), float(c[4]), float(c[10]), float(c[11]),
                           float(c[5]), float(c[6]), float(c[12]), float(c[13]),
                           float(c[8]), float(c[9]), float(c[15]), float(c[16])]

    if verbose:
        msg = "SixTrack optics at tank edge: S= {} m, BETX= {} m, ALFX= {}, BETY= {} m, ALFY= {}"
        print(msg.format(*reduced_coll_optics))

    return reduced_coll_optics


def check_parameter(tfs_dict, param):
    try:
        val = float(tfs_dict[param])
    except KeyError:
        msg = "{} not found in the TFS - setting to 0. Please check the output file"
        _warnings.warn(msg.format(param))
        val = 0.0
    return val


def load_coll_optics_tfs(filepath, coll_name, verbose=True):
    abs_filepath = _os.path.abspath(filepath)

    header = None
    coll_optics = None
    with open(abs_filepath, "r+") as f:
        for line in f:
            if line.startswith("*"):
                header =  line.replace("*", "").strip().split()

            elif coll_name in line or coll_name.lower() in line or coll_name.upper() in line:
                coll_optics_check = line.strip().split()
                name_index = header.index("NAME")
                if coll_optics_check[name_index].strip("'").strip('"').lower() == coll_name.lower(): # Check explicit name match TODO: make robust
                    coll_optics = coll_optics_check
                    break  # End the loop early - information found

    if coll_optics is None:
        msg = "Could not find info for collimator {} in MAD-X TFS optics"
        raise SystemExit(msg.format(coll_name))

    # pack the header and the data into a dictionary for easy access
    optics_dict = {}
    for i in range(len(coll_optics)):
        # Cast to floats later as some of the fields contain strings
        optics_dict[header[i]] = coll_optics[i]

    op = optics_dict  # shortcut
    # s, betax, alphax, betay, alphay, , dx, dy, dpx, dpy, x, xp, y, yp
    reduced_coll_optics = [float(op["S"]), float(op["BETX"]), float(op["ALFX"]),
                           float(op["BETY"]), float(op["ALFY"]), float(op["DX"]),
                           float(op["DPX"]), float(op["DY"]), float(op["DPY"]),
                           check_parameter(op, "X"), check_parameter(op, "PX"),
                           check_parameter(op, "Y"), check_parameter(op, "PY")]
                           # In case the TFS is missing columns for the offset

    if verbose:
        msg = "TFS optics at collimator centre: S= {} m, BETX= {} m, ALFX= {}, BETY= {} m, ALFY= {}"
        print(msg.format(*reduced_coll_optics))

    return reduced_coll_optics


def initalise_options_dict():
    options_dict = {}
    # List of all the substitute keys in the loaded template
    keys = ["TEMPLATE_HEADER", "BEAM_DESCRIPTION", "BETX", "BETY", "ALFX", "ALFY",
            "DX", "DPX", "DY", "DPY", "ENX", "ENY", "MASS", "PC", "DELTAP", "BLEN",
            "IONBEAM", "HASPDGID", "A", "Z", "Q", "PDGID", "DISTX", "DISTY", "SIG_INNER_X", "SIG_OUTER_X",
            "SIG_INNER_Y", "SIG_OUTER_Y", "DISTL", "SIG_INNER_L", "SIG_OUTER_L", "PLANE",
            "GEOACC_LOWER", "GEOACC_UPPER", "XOFFS", "XPOFF", "YOFFS", "YPOFFS",
            "TDELAY", "PCOFFS", "SSHIFT"]

    # If value is not explicity set, write out an EDITME tag to the output file as a placeholder
    for key in keys:
        options_dict[key] = "EDITME"

    return options_dict


def read_config_template(filepath=_os.path.join(_os.path.dirname(__file__),
                                                "../pyflusix/running/input_templates/"
                                                "config_ions.tpl")):
    # The default is to look for the template in the input_templates directory in the
    # directory where the script is located
    abs_filepath = _os.path.abspath(filepath)

    with open(abs_filepath, "r+") as filein:
        template_contents = filein.read()

    template = _tpl(template_contents)

    return template


def write_config_file(outfilepath, options_dict):
    template = read_config_template()
    rendered_text = template.substitute(options_dict)

    abs_outfilepath = _os.path.abspath(outfilepath)
    with open(abs_outfilepath, "w") as of:
        of.write(rendered_text)


def propagate_twiss_in_drift(beta, alpha, D, Dp, L):
    """
    Transforms twiss parameters in a drift.
    Positive sign of L means the end point is upstream of the starting point and a
    negative sign means the end point is downstream of the starting point
    """
    gamma = (1+alpha**2)/beta
    beta_new = beta + L**2*gamma - 2*L*alpha
    alpha_new = alpha - L*gamma
    D_new = D + Dp*L
    Dp_new = Dp

    return [beta_new, alpha_new, D_new, Dp_new]


def calculate_halo_parameters(emittance, beta0, alpha0, d0, dp0, L_coll, L_jaw, coll_opening,
                              impact_parameter, smear=1e-9, tfsoptics=False, verbose=True):
    """
    Calculates the halo sigma cuts for a beam starting at the edge of the collimator tank, such that
    at the edge of the active length of the collimator an impact parameter of a chosen magnitude
    and smear is achieved.

    Inputs:
      emittance    - Physical emittance of the beam
      beta0        - Twiss beta function at tank edge
      alpha0       - Twiss alpha function at tank edge
      L_coll       - Length of the collimator tank
      L_jaw        - Active length of the collimator
      coll_opening - Sigma opening of the collimator
      implact_parameter - in meters
      smear        - Smear of impact parameter in meters
      tfsoptics    - interpret the optics as being at the centre of the collimator
                     instead of at the tank start
      verbose      - Print detailed information

    N.B Dispersion is not considered for these calculatios
    """
    L_gap = (L_coll - L_jaw)/2.

    d_optics2jaw = 0  # Distance from the marker where the optics are defined to the jaw
    d_tank2jaw = 0  # Distance from the start of the tank to the jaw

    # If the beam if diverging we set the distribtion with the impact parameter on the outgoing
    # edge of the active length of the collimator
    diverging = alpha0 < 0

    if verbose:
        msg = "The beam is identified as {}, setting up halo parameters at the {} face."
        print(msg.format("DIVERGING" if diverging else "CONVERGING",
                         "OUTGOING" if diverging else "INCOMING"))
    if diverging:
        d_tank2jaw = L_gap + L_jaw
    else:
        d_tank2jaw = L_gap

    if tfsoptics:
        # TFS optics - defined at the centre of the collimator
        if diverging:
            d_optics2jaw = L_jaw/2.
        else:
            d_optics2jaw = -L_jaw/2.

        twiss_centre = [beta0, alpha0, d0, dp0]
        twiss_jaw = propagate_twiss_in_drift(beta0, alpha0, d0, dp0, d_optics2jaw)

    else:
        # SixTrack optics - defined at the start of the collimator tank
        # For sixtract the start point of the optics is the real start point of the beam
        d_optics2jaw = d_tank2jaw

        twiss_centre = propagate_twiss_in_drift(beta0, alpha0, d0, dp0, L_coll/2.)
        twiss_jaw = propagate_twiss_in_drift(beta0, alpha0, d0, dp0, d_optics2jaw)

    # The physical opening of the collimator is calculated using the sigma at the centre
    beta_centre = twiss_centre[0]
    sigma_centre = _np.sqrt(beta_centre*emittance)

    beta_jaw = twiss_jaw[0]
    sigma_jaw = _np.sqrt(beta_jaw*emittance)

    # The physical opening is set by the sigma opening at the centre
    coll_opening_phys = coll_opening*sigma_centre
    halo_cut_inner = (coll_opening_phys + impact_parameter - smear/2) # in meters
    halo_cut_outer = (coll_opening_phys + impact_parameter + smear/2)

    # Divide by the sigma to get halo limits in terms of Nsigma
    halo_nsigma_inner = halo_cut_inner/sigma_jaw
    halo_nsigma_outer = halo_cut_outer/sigma_jaw

    if verbose:
        print("Calculated half-gap: {} [m]".format(coll_opening*sigma_centre))

        print("Inner halo size at jaw: {} [sigma]".format(halo_nsigma_inner))
        print("Outer halo size at jaw: {} [sigma]".format(halo_nsigma_outer))

    parameters = (halo_nsigma_inner, halo_nsigma_outer, halo_cut_inner, halo_cut_outer,
                  d_tank2jaw, d_optics2jaw)
    return parameters


def main():

    usage = ("Automatically makes a gpdist configuration file for a pencil beam "
             "incident on a collimator with a chosen impact parameter")
    parser = _optparse.OptionParser(usage)
    parser.add_option('--fort2', action='store', dest="f2file", type="string",
                      default="", help="[DEPRECATED] Path to fort.2 file. Not used anymore.")

    parser.add_option('--fort3', action='store', dest="f3file", type="string",
                      default="", help="Path to fort.3 file")

    parser.add_option('--collGaps', action='store', dest="cgfile", type="string",
                      default="", help="Path to collGaps file [incompatible with --collDB]")

    parser.add_option('--collDB', action='store', dest="cdbfile", type="string",
                      default="", help="Path to collDB file [incompatible with --collGaps]")

    parser.add_option('--sixOptics', action='store', dest="stofile", type="string",
                      default="", help="Path to SixTrack optics digest "
                      "[incompatible with --tfsOptics]")

    parser.add_option('--tfsOptics', action='store', dest="tfsfile", type="string",
                      default="", help="Path to TFS file [incompatible with --sixOptics]")

    parser.add_option('--collName', action='store', dest="collname",
                      type="string", default="", help="Name of collimator where the beam starts")

    parser.add_option('--pmass', action='store', dest="pmass", type="float",
                      default=PROTON_MASS_FLUKA, help="Proton mass [GeV]")

    parser.add_option('--enorm', action='store', dest="enorm", type="float",
                      default=0.0, help="Normalised beam emittance [m]")

    parser.add_option('--blen', action='store', dest="blen", type="float",
                      default=None, help="Bunch length (4*sigma_l)")

    parser.add_option('--deltap', action='store', dest="dp", type="float",
                      default=None, help="Beam delta_p/p")

    parser.add_option('--imp', action='store', dest="imp", type="float",
                      default=None, help="Impact parameter [m]")

    parser.add_option('-q', '--quiet', action='store_true',
                      default=False, help="Silence detailed printouts")

    options, args = parser.parse_args()

    # Sanity checks on input
    if options.f2file:
        print("The fort.2 file provided via the --fort.2 argument is not required anymore.")

    elif not options.f3file:
        parser.print_help()
        parser.error("Please provide a fort.3 file via the --fort.3 argument")

    elif not options.stofile and not options.tfsfile:
        parser.print_help()
        parser.error("Please provide optical information file via the --sixOptics or"
                     "--tfsOptics arguments")

    elif not options.cgfile and not options.cdbfile:
        parser.print_help()
        parser.error("Please provide optical information file via the --collGaps or"
                     "--collDB arguments")

    elif options.stofile and options.tfsfile:
        parser.print_help()
        parser.error("The options --SixOptics and --TfsOptics are exclusive")

    elif options.cgfile and options.cdbfile:
        parser.print_help()
        parser.error("The options --collGaps and --collDB are exclusive")

    elif not options.enorm:
        parser.print_help()
        parser.error("Please provide the normalised emittance via the --enorm argument")

    elif not options.enorm:
        parser.print_help()
        parser.error("Please provide the normalised emittance via the --enorm argument")

    elif options.imp is None:
        parser.print_help()
        parser.error("Please provide the impact parameter via the --imp argument")

    elif options.blen is None:
        parser.print_help()
        parser.error("Please provide the bunch length  via the --blen argument")

    elif options.dp is None:
        parser.print_help()
        parser.error("Please provide the beam momentum spread  via the --deltap argument")

    elif not options.collname:
        parser.error("Please provide the collimator name via the --collName argument")

    if args:
        print("ERROR when parsing, leftover arguments", args)
        parser.print_help()
        raise SystemExit

    verbose = not options.quiet
    if verbose:
        print("--> Loading data")

    L_coll = get_coll_length_fort3(options.f3file, options.collname, verbose=verbose)
    beam_data = load_beam_def_fort3(options.f3file, verbose=verbose, pmass=options.pmass)

    emittance = norm_to_physical_emittance(options.enorm, beam_data[2], beam_data[3])

    if options.cgfile:
        coll_info = load_coll_info_collgaps(options.cgfile, options.collname,
                                            verbose=verbose)
    else:
        coll_info = load_coll_info_colldb(options.cdbfile, options.collname,
                                          verbose=verbose)

    if options.stofile:
        coll_optics = load_coll_optics_six(options.stofile, options.collname,
                                           verbose=verbose)
    else:
        coll_optics = load_coll_optics_tfs(options.tfsfile, options.collname,
                                           verbose=verbose)

    if verbose:
        print("--> Halo parameter calculation")

    angle = coll_info[-1]
    if angle == 0:
        plane = "H"
        # Allow some tolerance - no collimators with "nearly" 90 deg rotation
    elif abs(angle - _np.pi/2) < 1.e-5:
        plane = "V"
    else:
        raise SystemExit("Collimator {} not purely horizontal or vertical - "
                         "cannot automatically make halo".format(options.collname))

    if verbose:
        msg = "Collimator plane identified as {}"
        print(msg.format("HORIZONTAL" if plane == "H" else "VERICAL"))

    if plane == "H":
        op = [coll_optics[1], coll_optics[2], coll_optics[5], coll_optics[6]]
    elif plane == "V":
        op = [coll_optics[3], coll_optics[4], coll_optics[7], coll_optics[8]]

    tfsoptics = False
    if options.tfsfile and not options.stofile:
        tfsoptics = True

    # Inputs are: emittance, beta, alpha, length of collimator, length of jaw,
    # Nsigma opening, impact parameter
    # This method determines where to calculate the beam parameters based on
    # the beam divergence and the type of optics input used (SixTrack or TFS) and returns the
    # halo dimensions at the appropriate jaw and the drift distances needed to transform all
    # the starting optics
    parameters = calculate_halo_parameters(emittance, op[0], op[1], op[2], op[3], L_coll,
                                           coll_info[0], coll_info[1], options.imp,
                                           tfsoptics=tfsoptics, verbose=verbose)

    halo_nsigma_inner = parameters[0]
    halo_nsigma_outer = parameters[1]
    halo_cut_inner = parameters[2]
    halo_cut_outer = parameters[3]
    d_tank2jaw = parameters[4]
    d_optics2jaw = parameters[5]

    # Transform all the Twiss parameters and offsets from the optics marker to the position
    # of the jaw
    twiss_jaw_x = propagate_twiss_in_drift(coll_optics[1], coll_optics[2], coll_optics[5],
                                            coll_optics[6], d_optics2jaw)

    twiss_jaw_y = propagate_twiss_in_drift(coll_optics[3], coll_optics[4], coll_optics[7],
                                            coll_optics[8], d_optics2jaw)

    xoffs_jaw = coll_optics[9] - coll_optics[10]*d_optics2jaw
    xpoffs_jaw = coll_optics[10]
    yoffs_jaw = coll_optics[11] - coll_optics[12]*d_optics2jaw
    ypoffs_jaw = coll_optics[12]

    # Populate options dict with available values and write out
    options_dict = initalise_options_dict()

    today = _datetime.datetime.today().strftime("%d-%m-%Y")
    thisfile = _os.path.basename(__file__)

    # Header and beam info
    header_message = (" Produced on {} by {}\n** Automatically generated input - "
                      "CHECK ALL PARAMETERS before running!")
    options_dict["TEMPLATE_HEADER"] = header_message.format(today, thisfile)

    msg = "Beam at {} with {} um impact parameter"
    options_dict["BEAM_DESCRIPTION"] = msg.format(options.collname, options.imp*1.e6)

    # Optical parameters
    options_dict["BETX"] = twiss_jaw_x[0]
    options_dict["ALFX"] = twiss_jaw_x[1]
    options_dict["DX"] = twiss_jaw_x[2]
    options_dict["DPX"] = twiss_jaw_x[3]
    options_dict["BETY"] = twiss_jaw_y[0]
    options_dict["ALFY"] = twiss_jaw_y[1]
    options_dict["DY"] = twiss_jaw_y[2]
    options_dict["DPY"] = twiss_jaw_y[3]

    options_dict["ENX"] = options.enorm
    options_dict["ENY"] = options.enorm

    options_dict["A"] = str(beam_data[0])
    options_dict["Z"] = str(beam_data[1])
    options_dict["MASS"] = repr(beam_data[2])
    options_dict["Q"] = str(beam_data[4])
    options_dict["PDGID"] = str(beam_data[5])
    options_dict["PC"] = str(beam_data[3])   # Assume ultrarelativistic and set PC=Etot
    # Comment out ion defintion for protons
    options_dict["IONBEAM"] = "" if beam_data[2] != options.pmass else "*"
    options_dict["HASPDGID"] = "" if beam_data[5] != 0 else "*"

    options_dict["BLEN"] = options.blen
    options_dict["DELTAP"] = options.dp

    # Distribution type
    if plane == "H":
        options_dict["DISTX"] = "FLATC"
        options_dict["SIG_INNER_X"] = "{:.9}".format(halo_nsigma_inner)
        options_dict["SIG_OUTER_X"] = "{:.9}".format(halo_nsigma_outer)

        options_dict["DISTY"] = "GAUSS"
        options_dict["SIG_INNER_Y"] = "{:<7}".format(0)
        options_dict["SIG_OUTER_Y"] = "{:<7}".format(1)

    else:
        options_dict["DISTY"] = "FLATC"
        options_dict["SIG_INNER_Y"] = "{:.9}".format(halo_nsigma_inner)
        options_dict["SIG_OUTER_Y"] = "{:.9}".format(halo_nsigma_outer)

        options_dict["DISTX"] = "GAUSS"
        options_dict["SIG_INNER_X"] = "{:<7}".format(0)
        options_dict["SIG_OUTER_X"] = "{:<7}".format(1)

    options_dict["DISTL"] = "PENCIL"
    options_dict["SIG_INNER_L"] = ""
    options_dict["SIG_OUTER_L"] = ""

    # Offset
    options_dict["XOFFS"] = xoffs_jaw
    options_dict["XPOFFS"] = xpoffs_jaw
    options_dict["YOFFS"] = yoffs_jaw
    options_dict["YPOFFS"] = ypoffs_jaw
    options_dict["TDELAY"] = str(0.0)  # Set explicitly to zero for now
    options_dict["PCOFFS"] = str(0.0)

    # Acceptance cuts
    options_dict["PLANE"] = "{}".format(1 if plane == "H" else 3)  # 1 is horizonta, 3 is vertical
    options_dict["GEOACC_LOWER"] = "{:.8E}".format(halo_cut_inner)
    options_dict["GEOACC_UPPER"] = "{:.8E}".format(halo_cut_outer)

    # S shift
    # The distribution parameters are calculated for the jaw and are always backpropagated to
    # the start of the tank. For converging beams the backpropagation distance is the distance
    # from the jaw to the edge of the tank = (L-coll - L_jaw)/2
    options_dict["SSHIFT"] = str(-d_tank2jaw)

    # Write the file here
    write_config_file("CHECKME_gpdist_config.txt", options_dict)


if __name__ == "__main__":
    main()
