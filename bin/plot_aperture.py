#!/usr/bin/env python
from __future__ import print_function

import optparse as _optparse
import matplotlib.pyplot as _plt

import pyflusix.sixtrack_aperture as _sxtap


def main():
    usage = ''
    parser = _optparse.OptionParser(usage)
    parser.add_option('-f', '--filename', action='store', dest="filename", type="string",
                      default="", help="Name of aperture dump from SixTrack.")

    parser.add_option('-p', '--plane', action='store', dest="plane", type="string",
                      default="horizontal", help="Plane to make the plots in: "
                      "'horizontal', 'vertical' or 'both'")

    parser.add_option('--smin', action='store', dest="smin", type="float",
                      default=None, help="Starting S position of region of interest.")

    parser.add_option('--smax', action='store', dest="smax", type="float",
                      default=None, help="Final S position of region of interest.")

    parser.add_option('--sstart', action='store', dest="sstart", type="float",
                      default=None, help="Offset to be applied to all S position."
                      "Offset is applied before region selection.")

    parser.add_option('--stot', action='store', dest="stot", type="float",
                      default=None, help="Total S (length) of the machine."
                      "Must specify together with the --soffs option.")

    parser.add_option('-b', '--ymin', action='store', dest="ymin", type="float",
                      default=0.0, help="Minimum y-axis extent of plot.")

    parser.add_option('-t', '--ymax', action='store', dest="ymax", type="float",
                      default=0.0, help="Maximum y-axis extent of plot.")

    parser.add_option('-o', '--output', action='store', dest="outfile", type="string",
                      default="", help="Filepath to save the plot under.")

    parser.add_option('-v', '--verbose', action='store_true',
                      default=False, help="Print detailed information")

    options, args = parser.parse_args()

    if not options.filename:
        print("No filename specified. Stop.")
        parser.print_help()
        return

    if args:
        print("ERROR when parsing, leftover arguments", args)
        parser.print_help()
        raise SystemExit

    if options.outfile:
        outfile = options.outfile.replace(".pdf", "").replace(".png", "")
    else:
        outfile = options.filename.replace(".dat", "").replace(".txt", "")

    aperfile = options.filename

    # _plt.rcParams["figure.figsize"] = (12,7)
    # fig = _plt.figure(1, figsize=(12, 7))
    aperture = _sxtap.SixTrackAperture(aperfile, options.sstart, s_total=options.stot, debug=0)
    fig = aperture.plot_aperture_profile(plane=options.plane)

    ax = fig.gca()
    xlim = ax.get_xlim()

    if options.smin is not None:
        smin = options.smin
    else:
        smin = xlim[0]

    if options.smax is not None:
        smax = options.smax
    else:
        smax = xlim[1]

    ax.set_xlim(smin, smax)

    ylim = ax.get_ylim()
    if options.ymin and options.ymax:
        ylim = (options.ymin, options.ymax)
    elif options.ymin:
        ylim[0] = options.ymin
    elif options.ymax:
        ylim[1] = options.ymax

    ax.set_ylim(ylim)
    _plt.legend(fontsize="medium")
    _plt.savefig("{}_aperprofile.pdf".format(outfile), bbox_inches="tight")

    _plt.show()


if __name__ == "__main__":
    main()
