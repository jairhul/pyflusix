#!/usr/bin/env python
# -*- coding: utf-8 -*-

import optparse as _optparse
from datetime import datetime as _datetime

from pyflusix.running import submit


def main():
    usage = 'Create and submit a parallel Fluka-SixTrack coupling study to HTCondor'
    parser = _optparse.OptionParser(usage)
    parser.add_option('-i', '--inpfile', action='store', dest="inpfile", type="string", default="",
                      help="Name of the Fluka input file for the study")

    parser.add_option('-j', '--njobs', action='store', dest="njobs", type="int", default=1,
                      help="Number of individual jobs (run_xxxxx directories).")

    parser.add_option('-k', '--ncycles', action='store', dest="ncycles", type="int", default=1,
                      help="Number of jobs per superjobs (cycles) submitted to HTCondor.")

    parser.add_option('-c', '--coupling', action='store', dest="couplingpath", type="string",
                      default="", help="Path to the Fluka-SixTrack coupling setup.")

    parser.add_option('-t', '--tracker', action='store', dest="trackerpath", type="string",
                      default="default", help=("Path to the SixTrack execurable. "
                                               "Default is <couplingpath>/sixtrack/SixTrack"))

    parser.add_option('-f', '--fluka', action='store', dest="flukapath", type="string",
                      default="default",
                      help="Path to the FlukaPRO installation."
                           "Default is to read the FLUKAPRO environmental variable.")

    parser.add_option('-e', '--post', action='store', dest="postproc", type="string", default="",
                      help="Postprocessing script that runs after every job on the cluster.")

    parser.add_option('-b', '--beam', action='store', dest="beam", type="string", default=None,
                      help="Configuration file for gpdist if the input distribution is"
                           " dynamically gnerated. Default is to use intials.dat.")

    parser.add_option('-m', '--limi', action='store', dest="limi", type="string",
                      default="fort3.limi",
                      help="An aperture limi file to be used with this run. Default is fort3.limi."
                           " Note: the file thus specified will be copied to the run directories"
                           " as fort3.limi regardless of the filename.")

    parser.add_option('-a', '--additional', action='append', dest='additional', type="string",
                      default=[],
                      help="A whitespace-separated list of additional files for the run."
                           " Those files will be copied to the clean_input of the submssion "
                           "for use in job.")

    parser.add_option('-r', '--workdir', action='store', dest="workdir", type="string",
                      default="default",
                      help="Directory where to save the run_xxxx directories."
                           " Default is a dir of the same name as the inputfile adjacent to it.")

    parser.add_option('-q', '--queue', action='store', dest="queue", type="string",
                      default="tomorrow", help="Queue to put the jobs under on HTCondor")

    parser.add_option('-l', '--local', action='store_true',
                      default=False, help="Execute the jobs locally as shell scripts"
                                          " instead of submitting to the batch system")

    parser.add_option('-z', '--quiet', action='store_true',
                      default=False, help="Silence most printouts")

    options, args = parser.parse_args()

    # Sanity checks on input
    if not options.inpfile:
        parser.print_help()
        parser.error("Please provide the input file via the -i / --inpfile argument")

    if not options.couplingpath:
        parser.print_help()
        parser.error("Please provide the path to the coupling installation"
                     " via the -c / --coupling argument")

    if args:
        print "ERROR when parsing, leftover arguments", args
        parser.print_help()
        raise SystemExit

    verbose = not options.quiet
    start_time = _datetime.now()

    op = options  # shortcut
    submit(op.inpfile,
           op.njobs,
           op.ncycles,
           op.couplingpath,
           trackerpath=op.trackerpath,
           flukapath=op.flukapath,
           postprocfile=op.postproc,
           beamfile=op.beam,
           workdir=op.workdir,
           limifile=op.limi,
           additional=op.additional,
           queue=op.queue,
           local=op.local,
           verbose=verbose)

    if not op.quiet:
        print "--> Time taken: {}".format(_datetime.now() - start_time)
    return 0


if __name__ == "__main__":
    main()
